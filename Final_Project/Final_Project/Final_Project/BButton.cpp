#include "BButton.h"

BButton::BButton(string buttonTxt, sf::Vector2f position) {

	_button = sf::RectangleShape(sf::Vector2f(160,60));
	_buttonTexture.loadFromFile("pics_fonts/stone_button.png");
	_button.setTexture(&_buttonTexture);	
	
	_button.setOrigin(_button.getGlobalBounds().width/2, _button.getGlobalBounds().height/2);
	_button.setPosition(position);
	
	//loading our font from the file
	_buttonFont.loadFromFile("pics_fonts/gilk.ttf");
	setFont(position, buttonTxt);

}

void BButton::setFont(sf::Vector2f position, string buttonTxt){

	_buttonText.setFont(_buttonFont);                  //setting font and size 
	_buttonText.setString(buttonTxt);
	_buttonText.setCharacterSize(size_of_but_font);
	_buttonText.setColor(font_color);
	
	//now we set the origin point of a text to be a middle
	//of a rectangle that boundet it
	_buttonText.setOrigin(_buttonText.getGlobalBounds().width/2.f,
		_buttonText.getGlobalBounds().height/2.f+10.f ); 
	//setting a location of a text to be a middle of a button
	_buttonText.setPosition(position); 
}


void BButton::draw (sf::RenderWindow &win) {
	win.draw(_button);
	win.draw(_buttonText);
}