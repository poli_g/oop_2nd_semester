#pragma once
#include "Button.h"

class BButton : public Button {

public:
	
	BButton(string = "", sf::Vector2f = sf::Vector2f(0, 0));
	virtual ~BButton() {};

	virtual void draw (sf::RenderWindow &);
	void setFont(sf::Vector2f position, string buttonTxt);

};

