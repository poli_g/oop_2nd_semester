#pragma once
#include "Menu.h"

Menu::Menu(sf::Font font, string txt, type_menu type): _menuFont(font), _type(type){
		_txt.setFont(_menuFont); 
		_txt.setCharacterSize(size_of_menu_font);
		_txt.setString(txt);
		_txt.setOrigin(_txt.getGlobalBounds().width/2, _txt.getGlobalBounds().height/2);
		_clickSound.openFromFile("pics_fonts/press.wav");
	}

Menu::~Menu() {
	for(unsigned i=0; i<_options.size(); i++)
			delete _options[i].second;
}

void Menu::add(string name, Command* c) {
	_options.push_back(option(new BButton (name),c));
}

void Menu::activate(sf::RenderWindow &win, sf::FloatRect rect) {
	do {
		win.clear();
		draw(win, rect);
		win.display(); 
	} while (performAction(getOptionFromUser(win)));
}

void Menu::draw(sf::RenderWindow &win, sf::FloatRect rect) {
	
	if( _type == Hor_m){
		//if drawing horisontal menu
		drawTexture(win, rect);
		drawText(win, rect);
		for (unsigned int i = 0; i < _options.size(); i++) {
			//drawing a buttons inside rectangle
			_options[i].first->setPosition(sf::Vector2f(rect.left, rect.top) + 
				sf::Vector2f((i+1)*(rect.width/(_options.size()+1)), rect.height/2));
			_options[i].first->draw(win);
		}
	}

	else{ // vertical menu

		drawTexture(win, rect);
		for (unsigned int i = 0; i < _options.size(); i++) {
			//drawing a buttons inside rectangle
			_options[i].first->setPosition(sf::Vector2f(rect.left, rect.top) + 
				sf::Vector2f((rect.width/2),((i+1)*rect.height/(_options.size()+1))));
			_options[i].first->draw(win);
		}
	}
}
void Menu::drawTexture(sf::RenderWindow &win, sf::FloatRect rect) {
	
	//drawing texture
	sf::Texture texture;
	sf::RectangleShape textureRect(sf::Vector2f( rect.width, rect.height));

	texture.loadFromFile("pics_fonts/stone_frame.png");
	textureRect.setTexture(&texture);
	win.draw(textureRect);
}

void Menu::drawText(sf::RenderWindow &win, sf::FloatRect rect) {
	
	//drawing text
	_txt.setPosition(sf::Vector2f(rect.left, rect.top)+sf::Vector2f(rect.width/2, rect.height/7));
	//_txt.setColor(sf::Color::Yellow);
	win.draw(_txt);

}

int Menu::getOptionFromUser(sf::RenderWindow &window) {
	sf::Event event;
	while (window.pollEvent(event)){
	// closing window & exiting game
		if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && 
			event.key.code == sf::Keyboard::Escape)) {
				return _options.size() - 1;
		}
	
		if (event.type == sf::Event::MouseButtonPressed) {
			sf::Vector2f mouseClick(event.mouseButton.x, event.mouseButton.y);	// saving mouse click
			// checking if a vertex was clicked:
		
			for (unsigned int i = 0; i < _options.size(); i++) {
				if (_options[i].first != NULL)
					if (_options[i].first->getBounds().contains(mouseClick)) {
						_clickSound.play();
						return i;
					}
			}
		
		}
	}
	// check if any button was pressed, if not return null
	return 100;

}

bool Menu::performAction(int commandNum) {
	if(commandNum == 100)
		return true;

	if (commandNum == _options.size() - 1)	// if "return was clicked"
		return false;
	else{
		_options[commandNum].second->execute();
		return true;
	}
}