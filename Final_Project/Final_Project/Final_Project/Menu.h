#pragma once
#include "Configurations.h"
#include "Command.h"
#include "BButton.h"
#include "PButton.h"

using std::pair;

class Menu {

public:
	typedef pair<Button*,Command*> option;
	
	Menu(sf::Font font,string txt = "", type_menu type = Hor_m);
	~Menu();

	virtual void activate(sf::RenderWindow &win, sf::FloatRect rec);	// activates the menu
	virtual void add(string name, Command* c);		// adding an opption to the menu
	virtual void Menu::drawText(sf::RenderWindow &win, sf::FloatRect rect);
	virtual void Menu::drawTexture(sf::RenderWindow &win, sf::FloatRect rect);
	
protected:
	vector<option> _options;	// menu options
	virtual void draw(sf::RenderWindow &, sf::FloatRect);	// displays the menu
	int getOptionFromUser(sf::RenderWindow &win);	// checks which option was selected
	virtual bool performAction(int);	// executes the selected option, if one was selected
	type_menu _type;
	sf::Text _txt;
	sf::Font _menuFont;
	sf::Texture texture;
	sf::RectangleShape textureRect;
	sf::Music _clickSound;	
};
