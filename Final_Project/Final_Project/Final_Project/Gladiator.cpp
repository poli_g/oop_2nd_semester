#include "Gladiator.h"

Gladiator::Gladiator() {
	
	//setting texture
	_warTexture.loadFromFile("pics_fonts/gladiator.png");
	_rect.setTexture(&_warTexture);

	_hitPoints = gladiator_hitpoints;
	_armour = gladiator_armour;
	_maxDamage = gladiator_damage;
}

Gladiator::~Gladiator() {
}
