#include "Controller.h"
#include "Quest.h"

long int Controller::num_of_steps = 0;

Controller::Controller(sf::FloatRect mapArea): _mapArea(mapArea), _side_menu (side_bar_size),
	_background(menu_size),_lvlNum(0){

	initLevels();
	initWeaponsButtons();
	initGraphicsAndSounds();
	initTexts();
}

Controller::~Controller() {
}


void Controller::initWeaponsButtons() {
	
	_weapon_buttons.push_back(new AButton(Left, sf::Vector2f(200,300)));
	_weapon_buttons.push_back(new AButton(Right, sf::Vector2f(600,300)));
	_weapon_buttons.push_back(new BButton("Choose", middle+sf::Vector2f(0, 200)));

}

void Controller::initGraphicsAndSounds() {
	initSideBar();
	// download fonts and sounds 
	_gameFont.loadFromFile("pics_fonts/PARCHM.TTF");
	_kickSound.openFromFile("pics_fonts/kick.wav");
	_clickSound.openFromFile("pics_fonts/press.wav");
}

void Controller::runGame() {
	sf::RenderWindow gameWin(sf::VideoMode(gameWinWidth, gameWinHeight), "Heroes - The remake");
	
	//Chest *chest = new Chest();

	introMenu(gameWin);		// select map
	playerMenu(gameWin);	// select main player
	initCharacters();		// create monsters specific to level and place characters on map
	gameWin.setVerticalSyncEnabled(true);
	gameWin.setJoystickThreshold(100);

	while (gameWin.isOpen()){

		gameWin.clear();
		drawSideBar(gameWin);
		drawGame(gameWin);
		gameWin.display();
		
		sf::Event event;
		while (gameWin.pollEvent(event))
			handleEvents(event, gameWin);
			
		if (checkQuests()) {
			//displayVictoryMsg(gameWin);
			introMenu(gameWin);
			prepareNextLevel();
		}
	}
}

void Controller::prepareNextLevel() {
	initCharacters();
}

bool Controller::checkQuests() {
	for (unsigned int i = 0; i < _levels[_lvlNum]->_quests.size(); i++) {
		if (!_levels[_lvlNum]->_quests[i]->checkQuestCompletion(this))
			return false;
	}
	return true;
}

int Controller::getGold() {
	int totalGold = 0;
	for (unsigned int i = 0; i < _group.size(); i++) {
		totalGold += _group[i]->getGold();
	}
	return totalGold;
}

void Controller::handleEvents(const sf::Event& event, sf::RenderWindow &win) {
	// closing window
	if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && 
		event.key.code == sf::Keyboard::Escape)) {
		win.close();
	}

	// all keyboard events:
	direction dir;	// movement direction
	sf::Vector2f newPlayerLoc;	// new player location after move

	if (event.type == sf::Event::KeyPressed) {
		if (event.key.code == sf::Keyboard::Up)
			dir = UP_D;
		if (event.key.code == sf::Keyboard::Down)
			dir = DOWN_D;
		if (event.key.code == sf::Keyboard::Left)
			dir = LEFT_D;
		if (event.key.code == sf::Keyboard::Right)
			dir = RIGHT_D;

		newPlayerLoc = _group[0]->calculateMoveLocation(dir);
		countSteps();
		double steps = getSteps();

		if (checkLocation(newPlayerLoc)){
			_group[0]->setSpriteDirection(dir, steps);
			_group[0]->setLocation(sf::Vector2i(newPlayerLoc));
		}

		for(int i = 0; i < _monsters.size(); i++)
			_monsters[i]->setSpriteDirection(dir, steps);
	}

	// all mouse events:
	else if (event.type == sf::Event::MouseButtonPressed) {
		sf::Vector2f mouseClick = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);

		// checking if player inventory was opened
		for (unsigned int i = 0; i < _hero_buttons.size(); i++) {
			if (_hero_buttons[i]->getBounds().contains(mouseClick)) {
				 chooseWeaponMenu(win, _group[i]);
				}		
			}

		// checking if a monster was clicked
		for (unsigned int i = 0; i < _monsters.size(); i++)
			if (_monsters[i]->getBounds().contains(mouseClick))
				if (_group[0]->calculateDistance(_monsters[i]->getPosition()) <= 100)
					fight(i, win);

		// checking if a chest was clicked 
		for (unsigned int i = 0; i < _levels[_lvlNum]->_mapObjects.size(); i++) {
			if (_levels[_lvlNum]->_mapObjects[i]->getBounds().contains(mouseClick.x, mouseClick.y)) {
				_group[0]->collideWithObject(_levels[_lvlNum]->_mapObjects[i], win);
			}
		} 
	}

}

void Controller::fight (unsigned int monsterNum, sf::RenderWindow &win) {
	unsigned int j = 0;
	while (j != _group.size() && war(win, _monsters[monsterNum], _group[j]) == Lost_s) {
		// if we're moving up a player that means the previous player lost
		std::vector<Player*>::iterator it = _group.begin();
		if (j == _group.size())
			break;

		for (; it != _group.end(); it++) {
			if (*it == _group[j]) {
				_group.erase(it);
				break;
			}
		}
		
		// checking if we won or lost: (if j >= group.size() then we lost)
		if (j >= _group.size())	{
			printGoodbyeMsg(win);
			win.close();
		}
	}
	// if we're here that means we won
	std::vector<Monster*>::iterator itMonster = _monsters.begin();
	for (; itMonster != _monsters.end(); itMonster++) {
		if (*itMonster == _monsters[monsterNum]) {
			_monsters.erase(itMonster);
			break;
		}
	}
}
void Controller::printGoodbyeMsg(sf::RenderWindow &win){
	Menu goodBye(_gameFont, "Sorry! You lost this game..");
	goodBye.add("Finish", NULL);
	goodBye.activate(win, sf::FloatRect(0, 0, win.getSize().x, win.getSize().y));
}

void Controller::setText(sf::Text* text, sf::Vector2f position, unsigned int char_size, string txt, sf::Color color){

	//setting font and size 
	text->setFont(_gameFont);                 
	text->setString(txt);
	text->setCharacterSize(char_size);
	text->setColor(color);
	
	//now we set the origin point of a text to be a middle
	text->setOrigin(text->getGlobalBounds().width/2.f, text->getGlobalBounds().height/2.f+10.f ); 
	
	//setting a location of a text to be a middle of a button
	text->setPosition(position); 
}

void Controller::initTexts(){

	//creating texts
	for(int i = 0; i < 4; i++){
		_game_texts.push_back(new sf::Text);
	}

	//setting them
	setText(_game_texts[0], sf::Vector2f(400,10), size_of_menu_font - 20, "fight !", sf::Color(255,90,40));
	setText(_game_texts[1], sf::Vector2f(400,80), size_of_menu_font - 20, "Choose your weapon:", sf::Color(255,90,40));
	setText(_game_texts[2], pos_score, 90, "gold", sf::Color::Black);
}
void Controller::setAndDrawScore(sf::RenderWindow & win){

	//insert a life points into the stream
	std::stringstream sstr;
	sstr << "gold: " << countGold() ;
	_game_texts[2]->setString(sstr.str());
	//now we set the origin point of a text to be a middle
	_game_texts[2]->setOrigin(_game_texts[2]->getGlobalBounds().width/2.f, _game_texts[2]->getGlobalBounds().height/2.f+10.f ); 
	win.draw (*_game_texts[2]);
}

// counts gold of all players
unsigned int Controller::countGold(){
	
	unsigned int gold = 0;
	for (unsigned int i = 0; i < _group.size(); i++)
		gold += _group[i]->getGold();

	return gold;
}

void Controller::initSideBar() {
	_side_texture.loadFromFile("pics_fonts/side_texture.png");
	_side_menu.setTexture(&_side_texture);
	_side_menu.setPosition (position_side_bar); // setting coordinates of a sidebar
	_texture.loadFromFile("pics_fonts/stone_frame_menu.png");
	_background.setTexture(&_texture);
}

bool Controller::checkLocation(sf::Vector2f loc) {
	for (unsigned int i = 0; i < _levels[_lvlNum]->_mapObjects.size(); i++) {
		if (_levels[_lvlNum]->_mapObjects[i]->getBounds().contains(sf::Vector2i(loc)))
			return false;
	}
	return true;
}

void Controller::initCharacters() {
	// placing players:
	for (unsigned int i = 0; i < _group.size(); i++) {
		_group[i]->setLocation(sf::Vector2i(_levels[_lvlNum]->_playersLocs[i].x, _levels[_lvlNum]->_playersLocs[i].y));
	}

	// creating and placing monsters:
	for (unsigned int i = 0; i < _levels[_lvlNum]->_monstersLocs.size(); i++) {
		Monster *monster = new Ork;
		monster->setLocation(sf::Vector2i(_levels[_lvlNum]->_monstersLocs[i].x, _levels[_lvlNum]->_monstersLocs[i].y));
		_monsters.push_back(monster);
	}
}

void Controller::drawTextAndBackgroundMenu(sf::RenderWindow & win, int index){
	win.draw(*_game_texts[index]);
	win.draw(_background);
	drawChooseMenuButtons(win);
}

void Controller::chooseWeaponMenu(sf::RenderWindow & win, Player *player) {
	
	sf::Event event1;
	bool keep_choosing = true;

	while(keep_choosing){
		while (win.pollEvent(event1)){
	
			if (event1.type == sf::Event::MouseButtonPressed) {
					sf::Vector2f mouseClick(event1.mouseButton.x, event1.mouseButton.y);	// saving mouse click
				
				if (_weapon_buttons[0]->getBounds().contains(mouseClick)){ 
						_clickSound.play();
						if (player->getCurrWeaponToDisplay() >= 1){
							player->setCurrWeaponToDisplay(player->getCurrWeaponToDisplay() - 1);
						}
					} // end of if contains
				
				else if(_weapon_buttons[1]->getBounds().contains(mouseClick)){ 
					_clickSound.play();
					if (player->getCurrWeaponToDisplay() < player->getNumOfWeapons() - 1){
						player->setCurrWeaponToDisplay(player->getCurrWeaponToDisplay() + 1);
					}		
				}
				//if choose button was clicked
				else if(_weapon_buttons[2]->getBounds().contains(mouseClick)){ 
					_clickSound.play();
					keep_choosing = false;
				} 
			} // end of if contains
		} // end of if event
	win.clear();
	drawSideBar(win);
	drawTextAndBackgroundMenu(win,1);
	player->drawWeaponSprite(player->getCurrWeaponToDisplay(),win);
	win.display();
	}
}

void Controller::drawChooseMenuButtons(sf::RenderWindow &gameWin){
	//drawing a buttons
	for (unsigned int i = 0; i < _weapon_buttons.size(); i++)
		_weapon_buttons[i]->draw(gameWin);
}

void Controller::drawSideBar(sf::RenderWindow &gameWin){
	drawButtons(gameWin);
	setAndDrawScore(gameWin);
}

void Controller::drawButtons(sf::RenderWindow &gameWin){
	
	gameWin.draw(_side_menu);
	if(_hero_buttons.size() > 0)
		_hero_buttons[0]->setPosition(pos_first_face);
	if(_hero_buttons.size() > 1 )
		_hero_buttons[1]->setPosition(pos_second_face);
	if(_hero_buttons.size() > 2)
		_hero_buttons[2]->setPosition(pos_third_face);

	//drawing a buttons
	for (unsigned int i = 0; i < _hero_buttons.size() && i < 3; i++)
		_hero_buttons[i]->draw(gameWin);

	drawEmptyFrames(gameWin);
}

void Controller::drawEmptyFrames(sf::RenderWindow &gameWin){
	
	//creating two empty places for the future players
	PButton no_face0(No_player, pos_first_face);
	PButton no_face1(No_player, pos_second_face);
	PButton no_face2(No_player, pos_third_face);

	// drawing it
	switch(_hero_buttons.size()){
		case 0:
			no_face0.draw(gameWin);
			no_face2.draw(gameWin);
			no_face1.draw(gameWin);
			break;
		case 1:
			no_face2.draw(gameWin);
			no_face1.draw(gameWin);
			break;
		case 2:
			no_face2.draw(gameWin);
			break;
	}
}	

// 0 - player lost; 1 - player win;
wor_status Controller::war(sf::RenderWindow & win,  Monster * ork, Player* player){

	sf::Event event1;
	bool keep_fighting = true;
	while (keep_fighting){
		while (win.pollEvent(event1)){
	
			if (event1.type == sf::Event::MouseButtonPressed) {
				sf::Vector2f mouseClick(event1.mouseButton.x, event1.mouseButton.y);	// saving mouse click
					if (ork->getWarBounds().contains(mouseClick)){ 
						_kickSound.play();

						// if ork have been killed creating window with win message
						if (!(ork->decreaseLife((player->getHitPoints()+player->getWeaponPower())))){
							Menu winMenu(_gameFont, "You Win!");
							winMenu.add("Continue", NULL);
							winMenu.activate(win, sf::FloatRect(0, 0, win.getSize().x, win.getSize().y));
							keep_fighting = false;
							return Win_s;
						}
					
						// if player have been killed creating window with loose message
						if (!player->decreaseLife(ork->getHitPoints())){
							Menu lostMenu(_gameFont, "You lost!");
							lostMenu.add("Continue", NULL);
							lostMenu.activate(win, sf::FloatRect(0, 0, win.getSize().x, win.getSize().y));
							deletePlayerFace();
							keep_fighting = false;
							return Lost_s;
						}
					} // end of if contains
				} // end of if event
			}
		win.clear();
		drawSideBar(win);
		win.draw(_background);
		win.draw(*_game_texts[0]);
		// draw characters
		ork->drawWarStatus(win);
		player->drawWarStatus(win);
		win.display();
	} // end of while keep fighting
}

void Controller::introMenu(sf::RenderWindow &win) {
	Menu introMenu(_gameFont, "Select world to play:");
	introMenu.add("Tutorial", new selectTutorial(this));
	introMenu.add("Tularean Forest", new selectForestLvl(this));
	introMenu.add("Golden Mysts", new selectFortLvl(this));
	introMenu.add("Continue", NULL);
	
	introMenu.activate(win, sf::FloatRect(0, 0, win.getSize().x, win.getSize().y));
}

void Controller::playerMenu(sf::RenderWindow &win) {
	Menu introMenu(_gameFont, "Select your player:");
	introMenu.add("Warrior", new selectWorrier(this));
	introMenu.add("Gladiator", new selectGladiator(this));
	introMenu.add("Thief", new selectThief(this));
	introMenu.add("Continue", NULL);

	introMenu.activate(win, sf::FloatRect(0, 0, win.getSize().x, win.getSize().y));
}

void Controller::addWarrior(){
	_hero_buttons.push_back(new PButton(Worrier_h));
	Player *player = new Warrior;
	_group.push_back(player);
}

void Controller::addGladiator(){
	_hero_buttons.push_back(new PButton(Gladiator_h));
	Player *player = new Gladiator;
	_group.push_back(player);
}

void Controller::addThief(){
	_hero_buttons.push_back(new PButton(Thief_h));
	Player *player = new Thief;
	_group.push_back(player);
}

void Controller::deletePlayerFace(){
	if(_hero_buttons.size() > 0){
		_hero_buttons.erase (_hero_buttons.begin());
	}
}

void Controller::initLevels() {
	Level *level1 = new Level;
	level1->LoadFromFile("level1.tmx");
	Level *level2 = new Level;
	level2->LoadFromFile("level2.tmx");
	Level *level3 = new Level;
	level3->LoadFromFile("level4.tmx");

	_levels.push_back(level1);
	_levels.push_back(level2);
	_levels.push_back(level3);

	for (unsigned int i = 0; i < _levels.size(); i++) {
		_levels[i]->SetDrawingBounds(_mapArea);
	}
}

void Controller::drawGame(sf::RenderWindow &window) {
	// drawing map:
	_levels[_lvlNum]->Draw(window);
	// drawing side bar
	drawSideBar(window);
	// drawing characters
	drawCharacters(window);
}

void Controller::drawCharacters(sf::RenderWindow &win) {
	for (unsigned int i = 0; i < _group.size(); i++)
		_group[i]->draw(win);
	for (unsigned int i = 0; i < _monsters.size(); i++)
		_monsters[i]->draw(win);
}