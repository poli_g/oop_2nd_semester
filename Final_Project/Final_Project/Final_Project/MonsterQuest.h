#pragma once
#include "quest.h"

class MonsterQuest : public Quest {
public:
	MonsterQuest();
	~MonsterQuest();

	bool checkQuestCompletion(Controller *);
};

