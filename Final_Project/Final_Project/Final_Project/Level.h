/*********************************************************************
Quinn Schwab
16/08/2010
 
SFML Tiled Map Loader
 
The zlib license has been used to make this software fully compatible
with SFML. See http://www.sfml-dev.org/license.php
 
This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.
 
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:
 
1. The origin of this software must not be misrepresented;
   you must not claim that you wrote the original software.
   If you use this software in a product, an acknowledgment
   in the product documentation would be appreciated but
   is not required.
 
2. Altered source versions must be plainly marked as such,
   and must not be misrepresented as being the original software.
 
3. This notice may not be removed or altered from any
   source distribution.
*********************************************************************/
 
#pragma once

#include <map>
#include "Configurations.h"
#include "tinyxml.h"
#include <fstream>
#include "Object.h"
 
using std::vector;
using std::string;

const sf::Color transparentDefine(255, 0, 255);

class Quest;

struct Layer{
    int opacity;
    std::vector <sf::Sprite> tiles;
};
 
class Level {
    public:
		Level();
		virtual ~Level() {};
        //Loads the map. Returns false if it fails.
        bool LoadFromFile(string filename);
        //Moves the map. Although I would recommend using a view instead in most cases.
        void Move(int xStep, int yStep);
        //Set the area to draw
		void SetDrawingBounds(sf::FloatRect bounds) {_drawingBounds = bounds;};
        //Draws the map to the provided window.
        void Draw(sf::RenderWindow &window);
 
    private:
		// VARIABLES:

        // these variables represent various map properties:
        int _widthInTiles, _heightInTiles, _tileWidth, _tileHeight;
        // used to offset the tile number.
        int _firstTileID;
        // the bounds in which to draw the map (can be part of screen)
        sf::Rect<float> _drawingBounds;
        // The tileset image.
        sf::Image _tilesetImage;
        // The tileset texture.
        sf::Texture _tilesetTexture;
		//container of subrects (to divide the tilesheet image up)
		vector <sf::IntRect> _subRects;
        // This stores all objects on the map, from trees to chests
        vector <Object*> _mapObjects;
        // This stores each layer of sprites/tiles so they can be drawn in order.
        vector <Layer> _layers;
		vector<sf::Vector2f> _playersLocs;
		vector<sf::Vector2f> _monstersLocs;
		vector<Quest*> _quests;

		// FUNCTIONS:
		//Set up misc map properties
		void initMapProperties(TiXmlElement *);	
		// initializes everything to use tileset:
		void initTileSetProperties(TiXmlElement *);
		// inits subRects vector:
		void fillSubRects (int, int);
		// creates all the objects of the map & inits the player/monster locs
		void readObjects(TiXmlElement *);
		void createAndPlaceObject(TiXmlElement *, string);

		friend class Controller;
};
 
