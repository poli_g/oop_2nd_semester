#pragma once
#include "Configurations.h"
#include "Player.h"
#include "PButton.h"
#include "BButton.h"
#include "Menu.h"

class Terrain;
class Gold;
class Chest;
class Weapons;

class Object {
public:
	Object(sf::IntRect bounds = sf::IntRect()): _bounds(bounds) {};
	 
	virtual ~Object();

	static Object* create(const string &, sf::IntRect);	// creation function
	sf::IntRect getBounds () {return _bounds;};
	sf::Sprite getSprite() {return _objSprite;};
	virtual void draw(sf::RenderWindow & win);
	virtual void collideWithPlayer(Player *, sf::RenderWindow &) = 0;

protected:
	sf::IntRect _bounds;	// the bounds of the object on the map
	sf::Texture _objTexture;	// texture of the object
	sf::Sprite _objSprite;	//sprite of the object
	sf::Font _gameFont;
};

class Gold : public Object {
public:
	Gold(sf::IntRect bounds = sf::IntRect()): Object(bounds) {
		sf::Clock randClock;
		srand(randClock.getElapsedTime().asMicroseconds());
		_amount = rand() % 20;
		_amount *= 10;

		_objTexture.loadFromFile("pics_fonts/gold.png");
		_objSprite.setTexture(_objTexture);
		_objSprite.setOrigin(_objSprite.getGlobalBounds().width/2, _objSprite.getGlobalBounds().height/2);
		_objSprite.setPosition(middle);
	};

	int getAmount() {return _amount;};

	void collideWithPlayer(Player *player, sf::RenderWindow &win) {player->addGold(getAmount());	  _amount = 0;};
private:
	int _amount;
};

class Chest : public Object {
public:
	Chest(sf::IntRect bounds = sf::IntRect());

	void collideWithPlayer(Player *player, sf::RenderWindow &win) {openChest(player, win);};

	void openChest(Player*, sf::RenderWindow &);
private:
	vector<Object*> _riches;
	vector<Button*> _weapon_buttons; // contains buttons for the choose weapon menu
	sf::Music _clickSound;
	sf::Text _chestText;
	sf::RectangleShape _chestBackground;
	sf::Texture _ChestTexture;

	void initWeaponsButtons();
	void Chest::drawTextAndBackground(sf::RenderWindow &);
	void drawChooseMenuButtons(sf::RenderWindow &gameWin);
};

class Terrain :	public Object {
public:
	Terrain(sf::IntRect bounds): Object(bounds) {};

	void collideWithPlayer(Player *, sf::RenderWindow &) {};
};

class Weapons : public Object {
public:
	Weapons(unsigned int index);
	//virtual void draw(sf::RenderWindow &);
	Weapons::~Weapons(){}

	unsigned int getValue() { return _value;};
	unsigned int getPower() { return _power;};

	void collideWithPlayer(Player *, sf::RenderWindow &) {;};

protected:
	unsigned int _power;
	unsigned int _value;
};