#include "MonsterQuest.h"
#include "Controller.h"

MonsterQuest::MonsterQuest() {
}

MonsterQuest::~MonsterQuest() {
}

bool MonsterQuest::checkQuestCompletion(Controller *controller) {
	if (controller->getNumOfMonsters() == 0)
		return true;
	return false;
}