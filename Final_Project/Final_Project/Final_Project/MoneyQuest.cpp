#include "MoneyQuest.h"
#include "Controller.h"

MoneyQuest::MoneyQuest() {
	sf::Clock randClock;
	srand(randClock.getElapsedTime().asMicroseconds());
	_targetGold = rand() % 500 + 500;
}

MoneyQuest::~MoneyQuest() {
}

bool MoneyQuest::checkQuestCompletion(Controller *controller) {
	if (controller->getGold() >= _targetGold)
		return true;
	return false;
}