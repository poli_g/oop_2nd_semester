#pragma once
#include "character.h"

class Object;
class Weapons;
class Monster;

class Player : public Character {
	 
public:
	Player();
	virtual ~Player();
	sf::Vector2f calculateMoveLocation(direction);

	void selectWeapon(Weapons*);
	void sellWeapon(Weapons*);
	sf::Sprite getWeponSprite(int);
	void drawWeaponSprite(int, sf::RenderWindow &);
	int getCurrWeaponToDisplay() {return _currWeaponToDisplay;};
	void setCurrWeaponToDisplay(int i) {_currWeaponToDisplay = i;};
	int getNumOfWeapons();
	Weapons *getCurrWeapon();
	unsigned getWeaponPower();
	unsigned getGold() {return _gold;};
	void setSpriteDirection(direction dir, long int steps);
	void collideWithObject(Object*, sf::RenderWindow &);
	void addGold(int amount) {_gold += amount;};
	float calculateDistance(sf::Vector2f);

protected:
	vector<Weapons*> _weapons;
	Weapons *_cur_weap;
	int _currWeaponToDisplay;
	int _gold;
};