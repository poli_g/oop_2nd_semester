#pragma once
#include "Object.h"

class Command {
public:
	virtual void execute() = 0;		// execute simple command
};
