#pragma once
#include "command.h"

class selectFortLvl : public Command {
	
public:
	selectFortLvl (Controller * obj): _operationObj(obj) {};
	virtual void execute () {_operationObj->loadLevel(forestLvl);};

protected:
	Controller *_operationObj;
};
