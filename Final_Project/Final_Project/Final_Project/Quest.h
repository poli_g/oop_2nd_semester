#pragma once
#include <SFML/Graphics.hpp>

class Controller;

class Quest
{
public:
	Quest();
	~Quest();

	virtual bool checkQuestCompletion(Controller *) = 0;
protected:
};

