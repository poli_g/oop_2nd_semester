#include "browsingMenu.h"

void browsingMenu::draw(sf::RenderWindow &win, sf::FloatRect area) {
	_options[1].first->draw(win);
	_options[2].first->draw(win);
}

void browsingMenu::activate(sf::RenderWindow &win, sf::FloatRect rect) {
	do {
		win.clear();
		draw(win, rect);
		performAction(0);
		win.display(); 
	} while (performAction(getOptionFromUser(win)));
}

void browsingMenu::add(AButton* but, Command* c) {
	_options.push_back(option(but, c));
}