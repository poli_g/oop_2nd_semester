******************************************************************************************
            
                              R E A D M E


*******************************************************************************************


1)	OOP2 : Final Project

2)	By:	Polina Granot ID:	307696070
		Shur Svetlana ID:	318032745


3)	In this exersize we implemented a Might and Magic game, using grafic library SFML.


4)	Added files:

	Controller.h/cpp		-	This class contains the game graph and it's actually controlls all game. It's responsible of reading from
								file .tmx, creating characters, drawing, interaction between players and more.. 
								
	Level.h/cpp				-	Class responsibble of reading from tmx file a nd translating it to objects of the game.
	Menu.h/cpp				-   contains vector of pair<button*, Command*> that represents button and a action it have to do.
	Object.h/cpp			-   Base class of Gold, Terrain (all kind of obsticals), Chest (it contains vector of <Object*>
								and it's responsible to open chest menu), Weapons (there are 12 different kind of weapons,
								we randomly desided what power and what value will be to each weapon).
	Button.h/cpp			-   Base class of PButton (Picture Button) and AButton (Arrow Button)
	
	Character.h/cpp			-   Base class of Player and Monster
	Player.h/cpp			-   Base class of Worrier, Gladiator and a Thief. The difference between players there power.
	Monster.h/cpp			-   Base class of Ork
	Quest.h/cpp				-	Base class of MoneyQuest (mission of the player)
	MoneyQuest.h/cpp		-	Class contains function that cheks if mission of the player compleated.

	tyni*** .h/cpp			-	Files translating a .tmx files to the game objects.

6) Algorithms:				-	we used menu design patern and a factory design patern in this project, in the 
								implementation of menu. Also we used double dispatch for interaction between characters.
	Configurations.h		-   includes "#includes" and consts.

	level1/2/3.tmx			-   text file contains data (neighbors list of a graph) of three different levels.
								Two graphs for each level. 
	folder pics_fonts       -	all textures, fonts and a sound we used in this game
	
	
5) Data Structures:			-	mostly we used vector from standart library. Vector of <players> ,vector <Monsers>,
								vector of Butons, vector of sf::Text, vector of weapons of the player and more. We desided
								to use vector from standart library becouse it's comfortable to use and store an objects of the game,
								to erase and to run on it with vector::iterator.

7) Known Bugs:				-	no bugs were detected

8) Additional Comments:		-	1) we got late permition from Michal
								2) the moovement of the characters is not managed by the clock, character mooves only if
									you pressing on keyboard buttons, like in original game
								3) to open a chest or to start fighting with a monster you have to be close enouth 
								   and to click on it , also if you want to take a gold (gold, monster or a chest)
							