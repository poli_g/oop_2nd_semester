#pragma once

#include "Configurations.h"
#include "Character.h"
#include "Object.h"
#include "Level.h"
#include "browsingMenu.h"
#include "Ork.h"
#include "Thief.h"
#include "Gladiator.h"
#include "Warrior.h"

class Controller {

	static long int num_of_steps;

public:
	Controller(sf::FloatRect mapArea = sf::FloatRect(0, 0, 800, 600));
	~Controller();

	void runGame();
	void loadLevel(int lvlNum) {_lvlNum = lvlNum;};
	void addWarrior();
	void addGladiator();
	void addThief();
	static void countSteps() {num_of_steps++;};
	static long int getSteps() {return num_of_steps;};
	int getGold();
	int getNumOfMonsters() {return int(_monsters.size());};

private:
	// DATA MEMBERS
	vector<Monster*> _monsters;	     // monsters
	vector<Player*> _group;		     // players
	vector<Level*> _levels;		     // all levels of the game
	vector<PButton*> _hero_buttons;
	vector<Button*> _weapon_buttons; // contains buttons for the choose weapon menu
	vector<sf::Text*> _game_texts;

	sf::RectangleShape _side_menu;
	sf::RectangleShape _background;
	sf::Texture _side_texture;
	sf::Texture _texture;
	sf::Music _kickSound;
	sf::Music _clickSound;
	
	sf::Font _gameFont;
	int _lvlNum;		// num of curr level in vector
	sf::FloatRect _mapArea;
	
	// FUNCTIONS
	void initLevels();
	void initCharacters();
	void initGraphicsAndSounds();
	void initSideBar();	
	void initTexts();
	void initWeaponsButtons();
	void introMenu(sf::RenderWindow &);
	//open a war window 
	void fight (unsigned int, sf::RenderWindow &);
	void deletePlayerFace();
	wor_status war(sf::RenderWindow &, Monster *, Player*);
	void playerMenu(sf::RenderWindow &);
	void setText(sf::Text*, sf::Vector2f, unsigned int, string, sf::Color);
	// opens a window for choosing weapon
	void chooseWeaponMenu(sf::RenderWindow & win, Player *player);
	void openChestMenu(sf::RenderWindow & win, Chest* chest);
	void handleEvents(const sf::Event& event, sf::RenderWindow &win);
	unsigned int countGold();
	bool checkLocation(sf::Vector2f);	// checks validity of location on map
	// draw functions
	void drawGame(sf::RenderWindow &);
	void drawCharacters(sf::RenderWindow &);
	// draws a hero faces on the game screen
	void drawButtons(sf::RenderWindow &gameWin);
	void drawTextAndBackgroundMenu(sf::RenderWindow & win, int index);
	void drawSideBar(sf::RenderWindow &gameWin);
	void setAndDrawScore(sf::RenderWindow & win);
	void drawChooseMenuButtons(sf::RenderWindow &gameWin);
	void drawEmptyFrames(sf::RenderWindow &gameWin);
	void printGoodbyeMsg(sf::RenderWindow &win);
	bool checkQuests();	// checks if all quests of curr map were completed
	void prepareNextLevel();
};

/**************** DERIVED CLASSES OF COMMAND *************************/

class selectTutorial : public Command {
public:
	selectTutorial(Controller *obj): _operationObj(obj) {};
	virtual void execute (){_operationObj->loadLevel(tutorialLvl);};

protected:
	Controller *_operationObj;

};

class selectFortLvl : public Command {
	
public:
	selectFortLvl (Controller * obj): _operationObj(obj) {};
	virtual void execute () {_operationObj->loadLevel(fortLvl);};

protected:
	Controller *_operationObj;
};

class selectForestLvl : public Command {
	
public:
	selectForestLvl (Controller * obj): _operationObj(obj) {};
	virtual void execute () {_operationObj->loadLevel(forestLvl);};

protected:
	Controller *_operationObj;
};
/*******************************************************************/

class selectWorrier : public Command {
	
public:
	selectWorrier (Controller * obj): _operationObj(obj) {};
	virtual void execute () {_operationObj->addWarrior();};

protected:
	Controller *_operationObj;
};

class selectGladiator : public Command {
	
public:
	selectGladiator (Controller * obj): _operationObj(obj) {};
	virtual void execute () {_operationObj->addGladiator();};

protected:
	Controller *_operationObj;
};

class selectThief : public Command {
	
public:
	selectThief (Controller * obj): _operationObj(obj) {};
	virtual void execute () {_operationObj->addThief();};

protected:
	Controller *_operationObj;
};

/*
//**************************************************************

class useItem : public Command {
public:
	useItem(Player* player): _operationObj(player) {};
	virtual void execute () {_operationObj->selectWeapon(_obj);};
protected:
	Player *_operationObj;
	Weapons *_obj;
};

class sellItem : public Command {
public:
	sellItem(Player *player): _operationObj(player) {};
	virtual void execute () {
		_operationObj->sellWeapon(_obj);
	};
protected:
	Player *_operationObj;
	Weapons *_obj;
};


//**********************************************************************

class displayInventoryItem : public Command {
public:
	displayInventoryItem(Player *player, sf::RenderWindow &win): _operationObj(player), _win(win) {};
	virtual void execute () {
		if (_operationObj->getNumOfWeapons() > 0) {
			sf::Sprite temp(_operationObj->getWeponSprite(_operationObj->getCurrWeaponToDisplay()));
			temp.setPosition(_win.getSize().x/2, _win.getSize().y/2);
			_win.draw(temp);
		}
	};
protected:
	Player *_operationObj;
	sf::RenderWindow &_win;
};

class nextWeapon : public Command {
public:
	nextWeapon(Player *player): _operationObj(player) {};
	virtual void execute () {
		if (_operationObj->getCurrWeaponToDisplay() == _operationObj->getNumOfWeapons() - 1)
			_operationObj->setCurrWeaponToDisplay(0);
		else
			_operationObj->setCurrWeaponToDisplay(_operationObj->getCurrWeaponToDisplay() + 1);
	};
protected:
	Player *_operationObj;
};

class prevWeapon : public Command {
public:
	prevWeapon(Player *player): _operationObj(player) {};
	virtual void execute () {
		if (_operationObj->getCurrWeaponToDisplay() == 0)
			_operationObj->setCurrWeaponToDisplay(_operationObj->getNumOfWeapons() - 1);
		else
			_operationObj->setCurrWeaponToDisplay(_operationObj->getCurrWeaponToDisplay() - 1);
	};
protected:
	Player *_operationObj;
};

*/