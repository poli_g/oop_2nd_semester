#pragma once
#include "button.h"

class PButton : public Button {

public:
	
	PButton(my_hero hero, sf::Vector2f = sf::Vector2f(0, 0));

	virtual void draw (sf::RenderWindow &);
	void setFont(sf::Vector2f position, string buttonTxt);

};

class AButton : public Button {

public:
	
	AButton(arrow_dir , sf::Vector2f = sf::Vector2f(0, 0));
	virtual void draw (sf::RenderWindow &);
	
};
