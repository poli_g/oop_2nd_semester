#pragma once
#include "Configurations.h"

class Button {

public:
	Button() {};
	virtual ~Button() {};

	virtual void setPosition(const sf::Vector2f &pos) {_button.setPosition(pos);
												_buttonText.setPosition(pos);};
	virtual sf::FloatRect getBounds(){return _button.getGlobalBounds();};
	sf::Font* getFont(){return &_buttonFont;};
	virtual void draw (sf::RenderWindow &) = 0;
	
protected:
	sf::RectangleShape _button;
	sf::Texture _buttonTexture;
	sf::Text _buttonText;
	sf::Font _buttonFont;
};

