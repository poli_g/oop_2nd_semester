#include "Warrior.h"

Warrior::Warrior() {
	
	//setting texture
	_warTexture.loadFromFile("pics_fonts/warrior.png");
	_rect.setTexture(&_warTexture);
	
	_hitPoints = warrior_hitpoints;
	_armour = warrior_armour;
	_maxDamage = warrior_damage;
}

Warrior::~Warrior() {
}
