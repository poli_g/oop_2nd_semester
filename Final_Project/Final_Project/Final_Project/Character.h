#pragma once
#include "Configurations.h"

class Character {
public:
	Character();
	virtual ~Character();
	virtual void drawWarStatus(sf::RenderWindow &);
	virtual void draw(sf::RenderWindow &);
	virtual bool decreaseLife (unsigned int points);
	virtual unsigned getHitPoints() { return _hitPoints;};
	sf::FloatRect getBounds () {return _sprite.getGlobalBounds();};
	sf::FloatRect getWarBounds () {return _rect.getGlobalBounds();};
	void setLocation (sf::Vector2i loc); 
	static int num_of_steps;

protected:
	unsigned int _life;
	unsigned int _armour;
	unsigned int _hitPoints;
	unsigned int _maxDamage;
	
	sf::Vector2i _position;
	sf::Texture _charTexture;
	sf::Texture _warTexture;
	sf::Sprite _sprite;
	sf::RectangleShape _rect;
	sf::Text _text;
	sf::Font _charFont;
};

