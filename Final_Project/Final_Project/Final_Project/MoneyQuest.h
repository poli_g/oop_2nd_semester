#pragma once
#include "quest.h"

class MoneyQuest : public Quest {
public:
	MoneyQuest();
	~MoneyQuest();

	bool checkQuestCompletion(Controller *);
private:
	int _targetGold;
};

