#pragma once
#include "character.h"
class Monster :	public Character {
public:
	Monster();
	virtual ~Monster();
	void setSpriteDirection(direction dir, long int steps);
	sf::Vector2f getPosition() {return _sprite.getPosition();};

private:
	unsigned int _level;
};

