#include "Object.h"

Object::~Object() {
}

Object* Object::create(const string &type, sf::IntRect bounds) {
	if ( type == terrainObj ) return new Terrain(bounds);
	if ( type == chestObj ) return new Chest(bounds);
	if ( type == goldObj ) return new Gold(bounds);
    return NULL;
}

void Object::draw(sf::RenderWindow & win){
	_objSprite.setPosition(middle);
	win.draw(_objSprite);
}

//*************************************************************

// creating a chest and different objects inside
Chest::Chest(sf::IntRect bounds): Object(bounds) , _chestBackground(sf::Vector2f(1000,600)){
	initWeaponsButtons();
	_clickSound.openFromFile("pics_fonts/press.wav");
	_gameFont.loadFromFile("pics_fonts/PARCHM.TTF");
	_ChestTexture.loadFromFile("pics_fonts/stone_frame_menu.png");
	_chestBackground.setTexture(&_ChestTexture);

	// setting chest text parameters:
	_chestText.setFont(_gameFont);
	_chestText.setString("Choose The object you wish to take");
	_chestText.setCharacterSize(size_of_menu_font - 30);
	//_chestText.setColor(sf::Color(255,90,40));
	
	//now we set the origin point of a text to be a middle
	_chestText.setOrigin(_chestText.getGlobalBounds().width/2.f, _chestText.getGlobalBounds().height/2.f ); 
	
	_chestText.setPosition(sf::Vector2f(450,60)); 

	sf::Clock randClock;
	srand(randClock.getElapsedTime().asMicroseconds());
	//creating random number of obhects from 2 to 6
	int num_of_obj = rand()%5;
	num_of_obj +=2 ;

	for(int i = 0; i < num_of_obj; i++){
		//randomly choosing witch kind of reaches will be inside a chest
		int index = rand()%15;
		//creating one of a twelve weapons
		if(index <= 12)
			_riches.push_back(new Weapons(index));
		//or creatig a gold
		else
			_riches.push_back(new Gold());
	}
} 

void Chest::initWeaponsButtons() {
	_weapon_buttons.push_back(new AButton(Left, sf::Vector2f(200,300)));
	_weapon_buttons.push_back(new AButton(Right, sf::Vector2f(600,300)));
	_weapon_buttons.push_back(new BButton("Choose", middle+sf::Vector2f(0, 200)));
}

void Chest::openChest(Player *player, sf::RenderWindow & win) {
	sf::Event event1;
	//iterator runs over vector of riches in the chest
	vector<Object*>::iterator it = _riches.begin();
	
	if(_riches.size() == 0){
		Menu emptyChestMenu(_gameFont, "This is an empty chest..");
		emptyChestMenu.add("Continue", NULL);
		emptyChestMenu.activate(win, sf::FloatRect(0, 0, (float)win.getSize().x, (float)win.getSize().y));
	}

	bool keep_choosing = true;
	while (keep_choosing && _riches.size() != 0){
	
		while (win.pollEvent(event1)){
			if (event1.type == sf::Event::MouseButtonPressed) {
					sf::Vector2f mouseClick((float)event1.mouseButton.x, (float)event1.mouseButton.y);	// saving mouse click
				
				if (_weapon_buttons[0]->getBounds().contains(mouseClick)){ 
						_clickSound.play();
						if (it != _riches.begin()){
							it--;
						}
				} // end of if contains
					
				else if(_weapon_buttons[1]->getBounds().contains(mouseClick)) { 
					_clickSound.play();
					if (it != _riches.end()){
							it++;
						}
				}
				else if(_weapon_buttons[2]->getBounds().contains(mouseClick)){ 
					_clickSound.play();
					player->collideWithObject(*it, win);

					// erase from vector of riches
					if( it!= _riches.end())
						it = _riches.erase(it);
				} 
				if (_riches.size() == 0){
						Menu leftRichesMenu(_gameFont, "You emptied the chest!");
						leftRichesMenu.add("Continue", NULL);
						leftRichesMenu.activate(win, sf::FloatRect(0, 0, (float)win.getSize().x, (float)win.getSize().y));
						keep_choosing = false;
				}
			} 
		} // end of while pollevent

	win.clear();
	// running by the loop 
	if( it == _riches.end())
		it = _riches.begin();
	if (it != _riches.end()){
		(*it)->draw(win);
		drawTextAndBackground(win);
	}
	win.display();
	} //end of while keep chosing

}

void Chest::drawTextAndBackground(sf::RenderWindow &win) {
	win.draw(_chestBackground);
	win.draw(_chestText);
	drawChooseMenuButtons(win);
}

void Chest::drawChooseMenuButtons(sf::RenderWindow &gameWin){
	//drawing a buttons
	for (unsigned int i = 0; i < _weapon_buttons.size(); i++)
		_weapon_buttons[i]->draw(gameWin);
}

//**********************************************************

Weapons::Weapons(unsigned int index){

	sf::Clock randClock;
	srand(randClock.getElapsedTime().asMicroseconds());
	
	_power = rand() % 10;
	_value = _power*10;
	switch (index){
		case 0:
			_objTexture.loadFromFile("pics_fonts/weapon1.png");
			break;
		case 1:
			_objTexture.loadFromFile("pics_fonts/weapon2.png");
			break;
		case 2:
			_objTexture.loadFromFile("pics_fonts/weapon3.png");
			break;
		case 3:
			_objTexture.loadFromFile("pics_fonts/weapon4.png");
			break;
		case 4:
			_objTexture.loadFromFile("pics_fonts/weapon5.png");
			break;
		case 5:
			_objTexture.loadFromFile("pics_fonts/weapon6.png");
			break;
		case 6:
			_objTexture.loadFromFile("pics_fonts/weapon7.png");
			break;
		case 7:
			_objTexture.loadFromFile("pics_fonts/weapon8.png");
			break;
		case 8:
			_objTexture.loadFromFile("pics_fonts/weapon9.png");
			break;
		case 9:
			_objTexture.loadFromFile("pics_fonts/weapon10.png");
			break;
		case 10:
			_objTexture.loadFromFile("pics_fonts/weapon11.png");
			break;
		case 11:
			_objTexture.loadFromFile("pics_fonts/weapon12.png");
			break;
		default:
			break;
		}
	_objSprite.setTexture(_objTexture);
	_objSprite.setOrigin(_objSprite.getGlobalBounds().width/2, _objSprite.getGlobalBounds().height/2);
}
