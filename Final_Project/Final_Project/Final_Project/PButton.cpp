#include "PButton.h"


PButton::PButton(my_hero hero, sf::Vector2f position) {

	_button = sf::RectangleShape(sf::Vector2f(150,170));
	switch (hero){
	case Worrier_h:
		_buttonTexture.loadFromFile("pics_fonts/warrior_face.png");
		break;
	case Thief_h:
		_buttonTexture.loadFromFile("pics_fonts/thief_face.png");
		break;
	case Gladiator_h:
		_buttonTexture.loadFromFile("pics_fonts/gladiator_face.png");
		break;
	case No_player:
		_buttonTexture.loadFromFile("pics_fonts/no_face.png");
		break;
	default:
		_buttonTexture.loadFromFile("pics_fonts/no_face.png");
		break;
	}

	_button.setTexture(&_buttonTexture);	
	
	_button.setOrigin(_button.getGlobalBounds().width/2, _button.getGlobalBounds().height/2);
	_button.setPosition(position);
	
}

void PButton::draw (sf::RenderWindow &win) {
	win.draw(_button);
}
/******************* CLASS ARROW BUTTON *******************/

AButton::AButton(arrow_dir dir, sf::Vector2f pos){

	_button = sf::RectangleShape(sf::Vector2f(150,180));
	switch (dir){
	case Left:
		_buttonTexture.loadFromFile("pics_fonts/left_arrow.png");
		break;
	case Right:
		_buttonTexture.loadFromFile("pics_fonts/right_arrow.png");
		break;
	default:
		_buttonTexture.loadFromFile("pics_fonts/right_arrow.png");
		break;
	}
	_button.setTexture(&_buttonTexture);	
	_button.setOrigin(_button.getGlobalBounds().width/2, _button.getGlobalBounds().height/2);
	_button.setPosition(pos);
}

void AButton::draw (sf::RenderWindow &win) {
	win.draw(_button);
}