#include "Player.h"
#include "Object.h"
#include "Monster.h"

Player::Player(): _cur_weap(NULL), _gold(100), _currWeaponToDisplay(0) {
	_charTexture.loadFromFile("pics_fonts/sprite_sheet_girl.png");
	_sprite.setTexture(_charTexture);
	_sprite.setTextureRect(sf::IntRect(0, 0, 25, 50));
	_sprite.setOrigin(_sprite.getGlobalBounds().width/2, _sprite.getGlobalBounds().height);
	
	_rect.setPosition(pos_player);

	// player gets weapons from the begining
	_weapons.push_back(new Weapons(2));
	_weapons.push_back(new Weapons(4));
	_weapons.push_back(new Weapons(6));
	_weapons.push_back(new Weapons(10));
	_weapons.push_back(new Weapons(11));
	_weapons.push_back(new Weapons(9));

	_cur_weap = _weapons[0];
}

Player::~Player() {
}

sf::Sprite Player::getWeponSprite(int index) {
	return _weapons[index]->getSprite();
}

// calculating a next location

sf::Vector2f Player::calculateMoveLocation(direction dir) {
	sf::Vector2f newLocation = _sprite.getPosition();
	switch (dir) {
		case UP_D:
			if (newLocation.y > 0)
				newLocation += sf::Vector2f(0, -10);
			break;
		case DOWN_D:
			if (newLocation.y < gameWinHeight)
				newLocation += sf::Vector2f(0, 10);
			break;
		case LEFT_D:
			if (newLocation.x > 0)
				newLocation += sf::Vector2f(-10, 0);
			break;
		case RIGHT_D:
			if (newLocation.x < gameWinWidth-200)
				newLocation += sf::Vector2f(10, 0);
			break;
	}
	return newLocation;
} 

void Player::collideWithObject(Object *obj, sf::RenderWindow & win) {
	if (calculateDistance(sf::Vector2f((float)obj->getBounds().left + obj->getBounds().width/2,(float) obj->getBounds().top + obj->getBounds().height/2)) <= 100.f)
		obj->collideWithPlayer(this, win);
}

void Player::drawWeaponSprite(int index, sf::RenderWindow &win) {
	_weapons[index]->draw(win);
}

int Player::getNumOfWeapons () {
	return _weapons.size();
}

Weapons *Player::getCurrWeapon() {
	return _cur_weap;
}

float Player::calculateDistance(sf::Vector2f location) {
	return sqrt(pow(location.x - this->_sprite.getPosition().x, 2.f) + pow(location.y - this->_sprite.getPosition().y, 2.f));
}

void Player::selectWeapon(Weapons* weapon) {
	if (_cur_weap != weapon) {
		if (_cur_weap != NULL) {
			_weapons.push_back(_cur_weap);
		}
		_cur_weap = weapon;	
	}
}

void Player::sellWeapon(Weapons* weapon) {
	for (std::vector<Weapons*>::iterator it = _weapons.begin() ; it != _weapons.end(); ++it) {
		if (*it == weapon)
			_gold += weapon->getValue();
	}
}

unsigned  Player::getWeaponPower() {

	if(_cur_weap)
		return _cur_weap->getPower();
	else
		return 0;
}

// setting direction and a case of a character sprite
void Player::setSpriteDirection(direction dir, long int steps){
	
	int index = (steps % 4);
	index *= 25;

	switch (dir){
		case UP_D:
			_sprite.setTextureRect(sf::IntRect(index,150, 25, 50));
			break;
		case DOWN_D:
			_sprite.setTextureRect(sf::IntRect(index, 0, 25, 50));
			break;
		case LEFT_D:
			_sprite.setTextureRect(sf::IntRect(index, 100, 25, 50));
			break;
		case RIGHT_D:
			_sprite.setTextureRect(sf::IntRect(index, 50, 25, 50));
			break;
		default:
			_sprite.setTextureRect(sf::IntRect(index, 150, 25, 50));
			break;
	}
}
