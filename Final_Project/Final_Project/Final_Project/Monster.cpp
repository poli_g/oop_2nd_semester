#include "Monster.h"

Monster::Monster() {
	sf::Clock randomSeedClock;
	srand(randomSeedClock.getElapsedTime().asMicroseconds());

	_level = rand() % 20 + 1;	// setting level of monster
	_hitPoints = rand() % _level + 1;
	_hitPoints *= 5;
	_armour = rand() % _level + 1;
	_maxDamage = rand() % _level + 1;

	_charTexture.loadFromFile("pics_fonts/sprite_sheet_ork.png");
	_sprite.setTexture(_charTexture);
	_sprite.setTextureRect(sf::IntRect(0, 0, 25, 50));
	_sprite.setOrigin(_sprite.getGlobalBounds().left + _sprite.getGlobalBounds().width/2, _sprite.getGlobalBounds().height);

	_warTexture.loadFromFile("pics_fonts/ork.png");
	_rect.setTexture(&_warTexture);
	_rect.setPosition(pos_ork);
}

Monster::~Monster() {
}
void Monster::setSpriteDirection(direction dir, long int steps){
	
	int index = (steps % 3);
	index *= 25;
	
	_sprite.setTextureRect(sf::IntRect(index,0, 25, 50));
}
