#pragma once
#include "command.h"

class selectForestLvl : public Command {
	
public:
	selectForestLvl (Controller * obj): _operationObj(obj) {};
	virtual void execute () {_operationObj->loadLevel(forestLvl);};

protected:
	Controller *_operationObj;
};
