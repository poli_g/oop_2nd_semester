#pragma once
#include "Controller.h"
#include "command.h"

class selectTutorial : public Command {
public:
	selectTutorial(Controller *obj): _operationObj(obj) {};
	virtual void execute (){_operationObj->loadLevel(tutorialLvl);};

protected:
	Controller *_operationObj;

};
