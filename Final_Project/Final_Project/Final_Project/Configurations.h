#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include <string>
#include <sstream>

enum type_menu {Hor_m, Ver_m};
enum my_hero {No_player, Worrier_h, Thief_h, Gladiator_h};
enum arrow_dir {Left, Right};
enum wor_status {Lost_s, Win_s};
enum direction {UP_D, DOWN_D, LEFT_D, RIGHT_D};

using std::vector;
using std::string;

const int offset = 50;

const unsigned int gameWinWidth = 1000;
const unsigned int gameWinHeight = 600;

const sf::Vector2f position_side_bar(800,0);
const sf::Vector2f side_bar_size (200,600);
const sf::Vector2f menu_size (800,600);
const sf::Vector2f middle (sf::Vector2f (menu_size.x/2, menu_size.y/2)); 

const sf::Vector2f pos_first_face(910,1*gameWinHeight/7);
const sf::Vector2f pos_second_face(910,3*gameWinHeight/7);
const sf::Vector2f pos_third_face(910,5*gameWinHeight/7);
const sf::Vector2f pos_score(910,6*gameWinHeight/7);

const sf::Vector2f pos_ork(2*800/7, 7*gameWinHeight/8);
const sf::Vector2f pos_player(5*800/7, 7*gameWinHeight/8);

const unsigned int thief_hitpoints = 10;
const unsigned int thief_armour = 0;
const unsigned int thief_damage = 5;
const unsigned int gladiator_hitpoints = 30;
const unsigned int gladiator_armour = 0;
const unsigned int gladiator_damage = 5;
const unsigned int warrior_hitpoints = 25;
const unsigned int warrior_armour = 8;
const unsigned int warrior_damage = 10;
const unsigned int ork_hitpoints = 15;

const string terrainObj("terrainObj");
const string chestObj("chestObj");
const string goldObj("goldObj");
const string weaponObj("weaponObj");
const string spellObj("spellObj");

const int tutorialLvl = 0;
const int forestLvl = 1;
const int fortLvl = 2;

const int size_of_but_font = 35;
const int size_of_menu_font = 150;

const sf::Color font_color = sf::Color(255,255,255,200);
