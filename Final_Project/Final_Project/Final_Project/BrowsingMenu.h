#pragma once
#include "menu.h"

class browsingMenu : public Menu {
public:
	browsingMenu(sf::Font font,string txt = "", type_menu type = Ver_m): Menu(font, txt, type) {};

	~browsingMenu() {};

	virtual void activate(sf::RenderWindow &, sf::FloatRect);
	virtual void add(AButton*, Command* c);		// adding an arrow button
protected:

	void draw(sf::RenderWindow &win, sf::FloatRect);
};
