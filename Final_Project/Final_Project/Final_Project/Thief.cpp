#include "Thief.h"

Thief::Thief() {
	
	//setting textures
	_warTexture.loadFromFile("pics_fonts/thief.png");
	_rect.setTexture(&_warTexture);
	
	_hitPoints = thief_hitpoints;
	_armour = thief_armour;
	_maxDamage = thief_damage;
}

Thief::~Thief() {
}
