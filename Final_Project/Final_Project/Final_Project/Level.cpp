#include "level.h"
#include "MoneyQuest.h"
#include "MonsterQuest.h"

Level::Level () {
	Quest *moneyQuest = new MoneyQuest;
	Quest *monsterQuest = new MonsterQuest;
	_quests.push_back(moneyQuest);
	_quests.push_back(monsterQuest);
}

bool Level::LoadFromFile(std::string filename) {
    TiXmlDocument levelFile(filename.c_str());
 
    levelFile.LoadFile();
 
    //Map element. This is the root element for the whole file.
    TiXmlElement *map = levelFile.FirstChildElement("map");
	initMapProperties(map);

	// tilesets:
	TiXmlElement *tilesetElement = map->FirstChildElement("tileset");
	initTileSetProperties(tilesetElement);    
 
    // Layers:
    TiXmlElement *layerElement = map->FirstChildElement("layer");
    while (layerElement)
    {
        Layer layer;
        if (layerElement->Attribute("opacity") != NULL) //check if opacity attribute exists
        {
            float opacity = strtod(layerElement->Attribute("opacity"), NULL);//convert the (string) opacity element to float
            layer.opacity = 255 * opacity;
        }
        else
        {
            layer.opacity = 255;//if the attribute doesnt exist, default to full opacity
        }
 
        //Tiles
        TiXmlElement *layerDataElement;
        layerDataElement = layerElement->FirstChildElement("data");
 
        TiXmlElement *tileElement;
        tileElement = layerDataElement->FirstChildElement("tile");
 
        int x = 0;
        int y = 0;
 
        while (tileElement)
        {
            int tileGID = atoi(tileElement->Attribute("gid"));
            int subRectToUse = tileGID - _firstTileID;//Work out the subrect ID to 'chop up' the tilesheet image.
            if (subRectToUse >= 0)//we only need to (and only can) create a sprite/tile if there is one to display
            {
                sf::Sprite sprite;//sprite for the tile
                sprite.setTexture(_tilesetTexture);
                sprite.setTextureRect(_subRects[subRectToUse]);
                sprite.setPosition(x * _tileWidth, y * _tileHeight);
 
                sprite.setColor(sf::Color(255, 255, 255, layer.opacity));//Set opacity of the tile.
 
                //add tile to layer
                layer.tiles.push_back(sprite);
            }
 
            tileElement = tileElement->NextSiblingElement("tile");
 
            //increment x, y
            x++;
			if (x >= _widthInTiles)//if x has "hit" the end (right) of the map, reset it to the start (left)
            {
                x = 0;
                y++;
				if (y >= _heightInTiles)
                {
                    y = 0;
                }
            }
        }
 
        _layers.push_back(layer);
        layerElement = layerElement->NextSiblingElement("layer");
    }
 
	readObjects(map);
    return true;
}

void Level::readObjects(TiXmlElement *map) {
	TiXmlElement *objectGroupElement;
	string objectType;

	//Check that there is atleast one object layer:
    if (map->FirstChildElement("objectgroup") != NULL)
    {
		// we entered an object layer:
        objectGroupElement = map->FirstChildElement("objectgroup");	
        while (objectGroupElement)//loop through object layers
        {
			// saving name of object in this layer:
			objectType = objectGroupElement->Attribute("name");	

            TiXmlElement *objectElement;
            objectElement = objectGroupElement->FirstChildElement("object");
			createAndPlaceObject(objectElement, objectType);
            objectGroupElement = objectGroupElement->NextSiblingElement("objectgroup");
        }
    }
}

void Level::createAndPlaceObject(TiXmlElement *objectElement, string objectType) {
	if (objectType == "playersLocations")
		while (objectElement) {
			_playersLocs.push_back(sf::Vector2f(atoi(objectElement->Attribute("x")), atoi(objectElement->Attribute("y"))));
		objectElement = objectElement->NextSiblingElement("object");
		}

	else if (objectType == "monstersLocations")
		while (objectElement) {
			_monstersLocs.push_back(sf::Vector2f(atoi(objectElement->Attribute("x")), atoi(objectElement->Attribute("y"))));
		objectElement = objectElement->NextSiblingElement("object");
		}

	else
	while (objectElement)	//loop through objects
        {
			// creating bounds rectangle of the object:
			sf::IntRect objectRect(atoi(objectElement->Attribute("x")), 
									atoi(objectElement->Attribute("y")),
									atoi(objectElement->Attribute("width")),
									atoi(objectElement->Attribute("height")));
			// creating the object and saving it in the objects vector:
			Object *newObj = Object::create(objectType, objectRect);
			_mapObjects.push_back(newObj);
                
			// jumping to next object in layer
            objectElement = objectElement->NextSiblingElement("object");	
        }
}

void Level::initMapProperties(TiXmlElement *map) {
	_widthInTiles = atoi(map->Attribute("width"));
	_heightInTiles = atoi(map->Attribute("height"));
	_tileWidth = atoi(map->Attribute("tilewidth"));
	_tileHeight = atoi(map->Attribute("tileheight"));
}

void Level::initTileSetProperties(TiXmlElement *tilesetElement) {
	_firstTileID = atoi(tilesetElement->Attribute("firstgid"));
	TiXmlElement *image = tilesetElement->FirstChildElement("image");
	string imagepath = image->Attribute("source");

	// loading file for tileset:
	if (!_tilesetImage.loadFromFile(imagepath)) {
		// throw exception?
	}

	_tilesetImage.createMaskFromColor(transparentDefine);
	_tilesetTexture.loadFromImage(_tilesetImage);
	_tilesetTexture.setSmooth(false);

	fillSubRects (_tilesetTexture.getSize().x / _tileWidth, 
					_tilesetTexture.getSize().y / _tileHeight);
	_subRects;
};
	
void Level::fillSubRects (int columns, int rows) {
	// tiles/subrects are counted from 0, left to right, top to bottom
	for (int y = 0; y < rows; y++) {
		for (int x = 0; x < columns; x++) {
			sf::IntRect rect(x * _tileWidth, y * _tileHeight, _tileWidth, _tileHeight);
			_subRects.push_back(rect);
		}
	}
}

void Level::Draw(sf::RenderWindow &window)
{
    for (unsigned int layer = 0; layer < _layers.size(); layer++)
    {
        for (unsigned int tile = 0; tile < _layers[layer].tiles.size(); tile++)
        {
            if (_drawingBounds.contains(_layers[layer].tiles[tile].getPosition().x, _layers[layer].tiles[tile].getPosition().y))
            {
                window.draw(_layers[layer].tiles[tile]);
            }
        }
    }
}