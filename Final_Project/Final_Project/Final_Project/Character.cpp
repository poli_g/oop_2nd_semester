#include "Character.h"

Character::Character(): _life(100), _rect (sf::Vector2f(300,400)){
	
	_rect.setOrigin(_rect.getGlobalBounds().width/2, _rect.getGlobalBounds().height);
	_charFont.loadFromFile("pics_fonts/gilk.ttf");
	_text.setFont(_charFont);                  //setting font and size 
	
	_text.setCharacterSize(size_of_but_font);
	_text.setColor(sf::Color(232,232,232,200));
	
	//now we set the origin point of a text to be a middle
	_text.setOrigin(_text.getGlobalBounds().width/2.f,
		_text.getGlobalBounds().height/2.f+10.f );
}

Character::~Character() {}

bool Character::decreaseLife (unsigned int points){

	if (_life < points){
		_life = 0;     //character dies
		return false;
	}
	else
		_life = _life - points;
}

void Character::drawWarStatus(sf::RenderWindow & win){

	//insert a life points into the stream
	std::stringstream sstr;
	sstr << "life: " <<_life;
	_text.setString(sstr.str());

	//drawing a character and a text
	win.draw (_rect);

	//setting coordinates of text
	_text.setPosition(_rect.getPosition()+sf::Vector2f(-30,0));
	win.draw (_text);
}

void Character::draw(sf::RenderWindow & win) {
	win.draw(_sprite);
}

void Character::setLocation (sf::Vector2i loc) {
	_sprite.setPosition((float)loc.x, (float)loc.y);
};