#include <SFML/Graphics.hpp>

int foo () {
	sf::Vertex vertices[3] =
	{
		sf::Vertex(sf::Vector2f(0, 0)),
		sf::Vertex(sf::Vector2f(50, 50)),
		sf::Vertex(sf::Vector2f(100, 100))
	};
	
	sf::RenderWindow window(sf::VideoMode(800, 600), "VertexArray Example");

	while(window.isOpen())
	{
		window.clear();
		window.draw(vertices, 2, sf::Lines);
		window.display();

		sf::Event event;
		window.pollEvent(event);
		if(event.type == sf::Event::Closed)
			window.close();
	}

	return 0;
}