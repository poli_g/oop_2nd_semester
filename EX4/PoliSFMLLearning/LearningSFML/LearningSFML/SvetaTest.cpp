#include <SFML/Graphics.hpp>
//#include <SFML/System/Clock.hpp>
#include <iostream>

int fain() {
	// objects and variables:
	sf::Event event;
	sf::RenderWindow window(sf::VideoMode(800, 600), "Angles");
	sf::CircleShape circle(50);
	sf::RectangleShape rec (sf::Vector2f(200,200));
	sf::RectangleShape rec1 (sf::Vector2f(200,200));
	sf::RectangleShape rec2 (sf::Vector2f(200,200));
	sf::RectangleShape srec (sf::Vector2f(80,80));


	//set position & color of circles

	circle.setOrigin(sf::Vector2f(25, 25));
	circle.setPosition(130,130);
	circle.setFillColor (sf::Color(111,6,130));
	circle.setOutlineThickness(8);
	circle.setOutlineColor(sf::Color (89,3,105, 95));

	//set position & color of rectangles

	rec.setOrigin(sf::Vector2f(100,100));
	rec.setPosition (150,150);
	rec.setFillColor(sf::Color (255, 239, 176));
	rec.setOutlineThickness(8);
	rec.setOutlineColor(sf::Color (255,239,105, 70));

	rec1.setOrigin(sf::Vector2f(100,100));
	rec1.setPosition (350,350);
	rec1.setFillColor(sf::Color (255, 239, 176));
	rec1.setOutlineThickness(8);
	rec1.setOutlineColor(sf::Color (255,239,105, 70));

	rec2.setOrigin(sf::Vector2f(100,100));
	rec2.setPosition (550,550);
	rec2.setFillColor(sf::Color (255, 239, 176));
	rec2.setOutlineThickness(8);
	rec2.setOutlineColor(sf::Color (255,239,105, 70));

	//set position & color of small rectangle

	srec.setOrigin(sf::Vector2f(40,40));
	srec.setPosition (550,550);
	srec.setFillColor(sf::Color (0, 205,43));
	srec.setOutlineThickness(8);
	srec.setOutlineColor(sf::Color (1,114,25, 95));

	//creating a text and a font

	sf::Font font;
	if (!font.loadFromFile("a BOSA NOVACps Bold.TTF"))
		return 1;
	sf::Text text (" Font test! :)", font);
	text.setPosition (350,50);

	sf::Font font1;
	if (!font1.loadFromFile("Dbbrnms.ttf"))
		return 1;
	sf::Text text1 (" Another font.. bigger", font1,40U);
	text1.setPosition (300,150);
	text1.scale(2.0f, 3.0f);
	text1.setColor(sf::Color(111,6,130));

		while (window.isOpen()){

		window.clear();
		window.draw (text);
		window.draw (text1);


		window.draw(rec);
		window.draw(rec1);
		window.draw(rec2);

		window.draw(circle);
		window.draw(srec);
		
		window.display();
		

        while (window.pollEvent(event)) {
			// close window:
			if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
				window.close();

		}
				

	}//end of while

	return 0;

}//end of main