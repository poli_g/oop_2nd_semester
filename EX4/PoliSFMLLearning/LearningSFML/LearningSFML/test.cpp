/*
////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>


////////////////////////////////////////////////////////////
/// Entry point of application
///
/// \return Application exit code
///
////////////////////////////////////////////////////////////
int foo()
{
	// creating circle,  according to his position we move the view
	sf::CircleShape player(10.f);
	player.setFillColor(sf::Color::Yellow);
	player.setOrigin(player.getGlobalBounds().width/2, player.getGlobalBounds().height/2);
	player.setPosition(player.getOrigin() + sf::Vector2f(400, 300));

    // Create a sprite for the background
	sf::Texture BackgroundImage;
	if (!BackgroundImage.loadFromFile("background.png"))
        return EXIT_FAILURE;
	sf::Sprite Background(BackgroundImage);

	// Create the main window
    sf::RenderWindow App(sf::VideoMode(800, 600), "SFML Views");
	App.setMouseCursorVisible(false);

    // Create another sprite for the cursor
	sf::Texture CursorImage;
    if (!CursorImage.loadFromFile("cursor.tga"))
        return EXIT_FAILURE;
    sf::Sprite Cursor(CursorImage);

    // Resize the background, so that it's much bigger than the window
	//Background.scale(1.5, 1.5);

    // Define a text for displaying some instructions
    //sf::String Text("Use the arrow keys to move the camera\nUse the + and - keys to zoom and unzoom");
    //Text.(10, 500);
    //Text.SetColor(sf::Color::White);

    // Create a view with the same size as the window, located somewhere in the center of the background
    sf::Vector2f Center(400, 300);
    sf::Vector2f HalfSize(200, 150);
	sf::View View(App.getDefaultView());
    //sf::View View(Center, HalfSize);

    // To start, put the cursor at the center
	Cursor.setOrigin(Cursor.getTextureRect().width, Cursor.getTextureRect().height);
	Cursor.setPosition(Center);

	App.setVerticalSyncEnabled(false);

    // Start game loop
    while (App.isOpen())
    {
        // Process events
        sf::Event Event;
		App.pollEvent(Event);
		
        // Close window : exit
		if (Event.type == sf::Event::Closed)
            App.close();

		float viewOffset = 0.5;
		//float playerOffset = 0.5;
		if (Event.type == sf::Event::KeyPressed) {
			/*
			if (Event.key.code == sf::Keyboard::Up)		player.move(0, -playerOffset);
			if (Event.key.code == sf::Keyboard::Down)	player.move(0, playerOffset);
			if (Event.key.code == sf::Keyboard::Left)	player.move(-playerOffset,  0);
			if (Event.key.code == sf::Keyboard::Right)	player.move( playerOffset,  0);
			*/

			/*
			// checking to see if view needs moving
			if (player.getPosition().x > View.getCenter().x/2)
				if (Event.key.code == sf::Keyboard::Right)	View.move( viewOffset,  0);
			
			
			// Move the view using arrow keys
			if (Event.key.code == sf::Keyboard::Up)		View.move(0, -viewOffset);
			if (Event.key.code == sf::Keyboard::Down)	View.move(0, viewOffset);
			if (Event.key.code == sf::Keyboard::Left)	View.move(-viewOffset,  0);
			if (Event.key.code == sf::Keyboard::Right)	View.move( viewOffset,  0);
			
		}
  
        // Set the view
        App.setView(View);

        // Clear screen
        App.clear();

        // Draw background
		App.draw(Background);

		/*
        // Draw cursor
		sf::Vector2f CursorPos = App.convertCoords(sf::Vector2i(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y));
		//sf::Vector2f CursorPos = sf::Vector2f(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y);
        Cursor.setPosition(CursorPos);
        App.draw(Cursor);
		

		App.draw(player);
        // Reset to the default view to draw the interface
        //App.setView(App.getDefaultView());

        // Draw some instructions
        //App.draw(Text);

        // Finally, display rendered frame on screen
        App.display();
    }

    return EXIT_SUCCESS;
}
*/

#include <SFML/Graphics.hpp>
#include <vector>
#include <math.h>

const unsigned int WIDTH = 400, HEIGHT = 400;
const float PI = 3.14159265;

using std::vector;

int main() {
	// objects and variables:
	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "Sprite test!");
	sf::CircleShape circle(50.f);
	circle.setOrigin(50, 50);
	circle.setPosition(WIDTH/2, HEIGHT/2);
	circle.setFillColor(sf::Color::Transparent);
	circle.setOutlineThickness(1.f);
	circle.setOutlineColor(sf::Color::Yellow);
	
	sf::Vertex line0[2] =
		{
			sf::Vertex(sf::Vector2f(WIDTH/2, HEIGHT/2)),
			sf::Vertex(sf::Vector2f((cos(0.f*PI/180)*25)+WIDTH/2, (sin(0.f*PI/180)*25)+HEIGHT/2))
		};
	sf::Vertex line60[2] =
		{
			sf::Vertex(sf::Vector2f(WIDTH/2, HEIGHT/2)),
			sf::Vertex(sf::Vector2f(cos(60.f*PI/180)*25+(WIDTH/2), -sin(60.f*PI/180)*25+(HEIGHT/2)))
		};
	sf::Vertex line120[2] =
		{
			sf::Vertex(sf::Vector2f(WIDTH/2, HEIGHT/2)),
			sf::Vertex(sf::Vector2f(cos(120.f*PI/180)*25+(WIDTH/2), -sin(120.f*PI/180)*25+(HEIGHT/2)))
		};
	sf::Vertex line180[2] =
		{
			sf::Vertex(sf::Vector2f(WIDTH/2, HEIGHT/2)),
			sf::Vertex(sf::Vector2f(cos(180.f*PI/180)*25+(WIDTH/2), -sin(180.f*PI/180)*25+(HEIGHT/2)))
		};
	sf::Vertex line240[2] =
		{
			sf::Vertex(sf::Vector2f(WIDTH/2, HEIGHT/2)),
			sf::Vertex(sf::Vector2f(cos(240.f*PI/180)*25+(WIDTH/2), -sin(240.f*PI/180)*25+(HEIGHT/2)))
		};
	sf::Vertex line300[2] =
		{
			sf::Vertex(sf::Vector2f(WIDTH/2, HEIGHT/2)),
			sf::Vertex(sf::Vector2f(cos(300.f*PI/180)*25+(WIDTH/2), -sin(300.f*PI/180)*25+(HEIGHT/2)))
		};
	
	while (window.isOpen()) {
		window.clear();
		window.draw(circle);
		window.draw(line0, 2, sf::Lines);
		window.draw(line60, 2, sf::Lines);
		window.draw(line120, 2, sf::Lines);
		window.draw(line180, 2, sf::Lines);
		window.draw(line240, 2, sf::Lines);
		window.draw(line300, 2, sf::Lines);
		window.display();
		sf::Event event;

        while (window.pollEvent(event)) {
			// close window:
			if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
				window.close();

		}
				
	}
	return 0;
}
