#include <SFML/Graphics.hpp>
#include <sstream>
#include <iosfwd>

template<class T>
std::ostream& operator<< (std::ostream& os, sf::Vector2<T> v)
{
	os << "(" << v.x << "," << v.y << ")";
	return os;
}

int someti(int argc, char** argv)
{
	// Get Desktop (screen) size.
	sf::VideoMode desktopMode = sf::VideoMode::getDesktopMode();

	// Create window which is half screen size.

	// Note how we use the members of the VideoMode we got above to create a new VideoMode.
	sf::RenderWindow window(sf::VideoMode(desktopMode.width/2, desktopMode.height/2), "RenderTexture Example");

	// Create rectangle which is half window size.

	// Note how we use window size to set rectangle size.

	// Note the use of u suffix (2u) to note that 2 is not int but unsigned int.

	// We must use that, as window.getSize() returns Vector2u, and the / operator
	// is defined only for unsigned scalar for this type.
	// Then, we create a Vector2f from it, as this is what rectangle c-tor wants.
	sf::RectangleShape rect(sf::Vector2f(window.getSize()/2u));

	// We want to place this rectangle in centered in the window, so we set the
	// origin of the rect to be its mid point, and then set the position (which
	// is now setting the center of the rect to the given point), to be the mid
	// of the window.

	// Note how we use the decimal point to make 2 a non-integer value, and f suffix
	// to make it fload, not double. We need that because getSize of shape returns
	// Vector2f, and now we need float scalar for the operator / to work on it.
	// Luckily, setOrigin also get Vector2f, so we don't need one more conversion
	// of the resulted vector
	rect.setOrigin(rect.getSize()/2.f);
	rect.setPosition(sf::Vector2f(window.getSize()/2u));

//	// Create texture to put other shapes on our rect.
//	// Again, we use rect size to set the texture size.
//	sf::RenderTexture texture;
//	texture.create(rect.getSize().x,rect.getSize().y);

	// Create circle, half size of the rectangle.
	sf::CircleShape circ(std::min(rect.getSize().x,rect.getSize().y)/4.f);
	// sf::Color turquoise(0, 255, 255);
	circ.setFillColor(sf::Color::Cyan);
	circ.setOrigin(circ.getRadius(), circ.getRadius());
	circ.setPosition(rect.getPosition());
//	circ.move(-rect.getSize().x/4, 0);

	// Put it on our texture
//	texture.draw(circ);

	// Create triangle same size as the circle
	sf::CircleShape triangle(circ.getRadius(), 3);
	triangle.setOrigin(triangle.getRadius(), triangle.getRadius());
	triangle.setPosition(circ.getPosition());

	// Load texture to pain on our triangle
	sf::Texture dottedTexture;
	dottedTexture.create(triangle.getLocalBounds().width, triangle.getLocalBounds().height);
	dottedTexture.loadFromFile("texture-design.png");
	dottedTexture.setRepeated(true);

	triangle.setTexture(&dottedTexture);
	//triangle.setFillColor(sf::Color::Magenta); // will have no effect
	triangle.setOutlineColor(sf::Color::Magenta);
	triangle.setOutlineThickness(5);

//	texture.draw(triangle);

//	// This is what actually update the texture with all the above drawing results
//	texture.display();

//	// Set the Texture of the texutre (not the RenderTexture it self) as the texture of the rect
//	rect.setTexture(&texture.getTexture());

	// Create rectangle by CircleShape
	sf::CircleShape r(rect.getSize().y/2.f, 4);
	r.setOrigin(r.getRadius(), r.getRadius());
	r.setPosition(rect.getPosition().x-rect.getSize().x/2, rect.getPosition().y);
	r.setFillColor(sf::Color::Yellow);

	sf::Font font(sf::Font::getDefaultFont());
	std::stringstream sstr;
	sstr << "Circ pos: " << circ.getPosition() << std::endl
		<< "Rect pos: " << rect.getPosition() << std::endl
		<< "Circ origin: " << circ.getOrigin() << std::endl
		<< "Rect origin: " << rect.getOrigin() << std::endl
		<< "Circ size: " << circ.getRadius() << std::endl
		<< "Rect size: " << rect.getSize() << std::endl;
	sf::Text txt(sstr.str(), font, 20);
	txt.setColor(sf::Color::Magenta);

	sf::Text mouseTxt("", font, 15);
	mouseTxt.setColor(sf::Color::Red);
	mouseTxt.setPosition(0, window.getSize().y-txt.getCharacterSize());

	while(window.isOpen())
	{
		window.clear();
		window.draw(rect);
		window.draw(circ);
		window.draw(triangle);
		window.draw(txt);
		window.draw(mouseTxt);
		window.draw(r);
		window.display();

		sf::Event event;
		window.waitEvent(event);
		switch(event.type)
		{
		case sf::Event::Closed:
			window.close();
			break;
		case sf::Event::MouseButtonPressed:
			std::stringstream sstr;
			sstr << sf::Vector2i(event.mouseButton.x, event.mouseButton.y);
			mouseTxt.setString(sstr.str());
			mouseTxt.setPosition(event.mouseButton.x, event.mouseButton.y);
			break;
		}
	}
	return 0;
}
