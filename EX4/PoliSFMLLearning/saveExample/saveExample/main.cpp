// needs to run on a console project

#pragma once
#include <iostream>
#include <fstream>
using namespace std;

enum SCORE {HIGH, LOW, BINGO};

// prints user menu:
int menu();
// returns if the number inputted by user is too high, too low, or the user guessed the num
SCORE play(int, int);
// loads game
void loadGame(int &, int &);
// saves game
void saveGame(int, int);

int main () {
	// variables for guessing game
	srand(NULL);
	int numToGuess = rand() % 100 + 1;
	int numOfAttempts = 0;
	int userGuess = 0;
	int menuChoice = menu();
	SCORE roundScore;

	while (menuChoice != 4) {
		switch (menuChoice) {
			case 1:
				cin >> userGuess;
				numOfAttempts++;
				roundScore = play(userGuess, numToGuess);
				if (roundScore == BINGO) {
					cout << "Congratulations! you guessed the number " << numToGuess << " in " << numOfAttempts << " attempts!" << endl;
					srand(NULL);
					numToGuess = rand() % 100;
					numOfAttempts = 0;
					userGuess = 0;
				}
				else if (roundScore == LOW)
					cout << "The number "<< userGuess << " is too low" << endl;
				else if (roundScore == HIGH)
					cout << "The number "<< userGuess << " is too high" << endl;
				break;
			case 2:
				loadGame(numToGuess, numOfAttempts);
				break;
			case 3:
				saveGame(numToGuess, numOfAttempts);
				break;
		}

		menuChoice = menu();
	}
	return 0;
}

int menu () {
	int userChoise;		// to save user choice
	
	cout << "Please select an action:" << endl;
	cout << "1 - select a number" << endl;
	cout << "2 - load saved game" << endl;
	cout << "3 - save game" << endl;
	cout << "4 - exit" << endl;

	cin >> userChoise;
	return userChoise;
}

SCORE play(int guess, int compNum) {
	if (guess == compNum)
		return BINGO;
	else if (guess < compNum)
		return LOW;
	else
		return HIGH;
}

void loadGame(int &numToGuess, int &numOfAttempts) {
	numToGuess = 0;
	numOfAttempts = 0;

	ifstream saveFile;
	saveFile.open("save.txt");
	char saveData;
	int coeff = 10;

	while (saveData = saveFile.get()) {
		if (saveData == '#')
			saveData = saveFile.get();
		numToGuess += coeff * saveData;
		coeff /= 10;
	}

	saveFile.close();
	cout << "Game loaded!" << endl << "Num of guesses so far: " << numOfAttempts << endl;
}

void saveGame (int numToGuess, int numOfAttempts) {
	ofstream saveFile;
	saveFile.open("save.txt");

	saveFile << '#' << numToGuess << '#' << numOfAttempts;

	saveFile.close();
	cout << "Game Saved!" << endl;
}