#pragma once
#include <cstdlib>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

using std::cerr;

const unsigned int SIZE_OF_FONT = 20;         //default size of font
const sf::Color ButtonColor (255, 222, 111);  //default color of button

class Button {

	public:
		
	//    C-TORS
	
	Button ();
	Button (const sf::Vector2f location, const std::string text); 

	//    FUNCTIONS
	
	//public function that draws a button, this function can recognize if 
	//it's pressed button or nomal 
	void drawButton (sf::RenderWindow&);
	//change the status of the button to be pressed
	void pressButton() {_isPressed = true;};
	//change the status of the button to be unpressed
	void unpressButton() {_isPressed = false;};
	//returns font
	sf::Font getFont() const {return _font;};
	//returns location of the button
	sf::Vector2f getLocation() const {return _location;};
	//this function returns bounds of a button 
	sf::RectangleShape getRect() const {return _rec;};
	
	// returns isPressed data member
	bool getIsPressed () const { return _isPressed; };
	//drawing prosees of pressing a button 
	void drawPressButton(sf::RenderWindow & win);
	

		
	private:
		
		//  data members
		
		sf::Vector2f _location;     //keeping location of the button
		sf::Text *_nameOfButton;    //the buttons name
		sf::Font _font;             //font we use (we using only one in this exersize)
		sf::Texture _texture;       //for the texture of green button
		sf::RectangleShape _rec;    //local bounds of button
		bool _isPressed;            //knows if button pressed

        //function that seting font to be our font
		void setFont();
	
};