******************************************************************************************
            
                              R E A D M E


*******************************************************************************************


1)	OOP EX4: Card Game

2)	By:	Polina Granot ID:	307696070
		Shur Svetlana ID:	318032745


3)	In this exersize we implemented a card game, called "Set" using grafic library SFML.


4)	Added files:


	Button.h/cpp		-  Class contain data members and functions
	Board.h/cpp		    -  this class contain the vector of 12 cards, 
				           there locations on the board. 
	Card.h/cpp		    -  base class for all card classes, include different operators
				           for compare a cards and virtual function of draw.  
	ColorChangingCard.h/cpp	 - for card wich changing a color of shapes on it
	JockerCard.h/cpp	 - includes texture for the joker card and draw function
	ShapeChangingCard.h/cpp  -  for card wich changing a shape on it
	GameController.h/cpp     -  controls the game, include functions that creates buttons, 
	                            checking set
	Set.cpp                  -  main
	forte.ttf                -  font we desided to use
	txtr1.png				 -  texture we using for stripes of shapes		
	button_up.png            -  texture we using for show pressed button
	button_down.png          -  texture we using for show unpressed button
	heard/2heard/3heard.png  -  texture representing one/two/three lives

	
5) Data Structures:		we used in some places std::vector from <vector> vector of cards* 
			         	and vector of buttons, this data structure
			        	is very easy to use and helps to contein a data.

6) Algorithms:		no algorithms was used. we used Inheritance and a polymorthism
		         	in this exersise. It help us to implement different kinds of cards.

7) Known Bugs:		no bugs were detected


8) Additional Comments:     	In addition to disappearing card, card that changing it's shape/color
				or number of shapes we required to make, we decided to make our game 
				to look and to act like real computer game. So we added score and lives:
				the game started with three lives and zero score. If player select wrong
				cards ("no set selected") so he lost one life. If player choosing right
				set - he is  getting 10 points for each set. 
				On the board you can see two buttons (they have two positions: up and down).					
				Button "Shuffel" player can use only if he has more then 50 poins, because
				to shuffle a cards will "cost" him 50 points. Button "Quit" opens another
				window and a player asked again if he wants to quit the game or he wants
				continue to play. 