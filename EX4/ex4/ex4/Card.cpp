#include "Card.h"

Card::Card(): _location(sf::Vector2f(0, 0)), _isSelected(false), 
			  _cardColor ((enum colors_t) (rand () % 3)), 
			  _cardShape ((enum shapes_t) (rand () % 3)), 
			  _cardTexture ((enum texture_t) (rand () % 3)), 
			  _numOfShapes ((enum number_t) ((rand () % 3) +1)) {

	_blankCard.setSize(blankCardSize);

	_stripeTexture.create(30, 30);
	_stripeTexture.loadFromFile ("txr1.png");
	_stripeTexture.setRepeated(true);
};

Card::Card (enum colors_t color, enum shapes_t shape, 
			enum texture_t texture, enum number_t number,
			sf::Vector2f location): _location(sf::Vector2f(location)), _isSelected(false), 
									_cardTexture(texture), _cardColor(color), _cardShape(shape),
									_numOfShapes(number) {
	_stripeTexture.create(30, 30);
	_stripeTexture.loadFromFile ("txr1.png");
	_stripeTexture.setRepeated(true);
};
	
Card::Card (const Card &other){
	*this = other;
}

Card Card::operator = (const Card &other) {
	_location = other._location;
	_cardColor = other._cardColor;
	_cardShape = other._cardShape;
	_numOfShapes = other._numOfShapes;
	_cardTexture = other._cardTexture;

	return *this;
}

bool Card::operator == (const Card &other) const {
	if (_cardColor == other._cardColor && _cardShape == other._cardShape
		&& _cardTexture == other._cardTexture && _numOfShapes == other._numOfShapes)
		return true;
	return false;
}

void Card::drawCard (const sf::Vector2f &cardLocation, sf::RenderWindow &window) { 
	
	_blankCard.setSize(blankCardSize);
	_blankCard.setOrigin(_blankCard.getSize() / 2.f);
	_blankCard.setFillColor(cardColor);
	_blankCard.setPosition(cardLocation);

	// drawing blank card to window:
	window.draw(_blankCard);

	// drawing shape on card:
	sf::CircleShape cardShape = setCardShape();
	cardShape.setOrigin(cardShape.getRadius(), cardShape.getRadius());
	
	// initializing setting of card's shape:
	// setting color:
	sf::Color cardColor;
	sf::Color outlineColor;
	setShapeColors (cardColor, outlineColor);
	
	// setting texture:
	switch (_cardTexture) {
		case FULL_TT:
			cardShape.setFillColor(cardColor);
			break;
		case EMPTY_TT:
			break;
		case STRIPES_TT:
			cardShape.setTexture(&_stripeTexture);
			cardShape.setFillColor(cardColor);
			break;
	}

	cardShape.setOutlineThickness(5.f);
	cardShape.setOutlineColor(outlineColor);

	// drawing the shapes to target:
	sf::CircleShape cardShape2 (cardShape);
	sf::CircleShape cardShape3 (cardShape);

	cardShape.setPosition(_blankCard.getPosition());
	cardShape2.setPosition(_blankCard.getPosition() - sf::Vector2f(0, 2*cardShape.getRadius() + 10));
	cardShape3.setPosition(_blankCard.getPosition() + sf::Vector2f(0, 2*cardShape.getRadius() + 10));
	
	switch (_numOfShapes) {
		case 0:
			break;
		case 1:
			window.draw(cardShape);
			break;
		case 2:
			window.draw(cardShape2);
			window.draw(cardShape3);
			break;
		case 3:
			window.draw(cardShape);
			window.draw(cardShape2);
			window.draw(cardShape3);
			break;
	}
}

sf::CircleShape Card::setCardShape () {
	sf::CircleShape cardShape(CARD_SHAPE_RADIUS);
	switch (_cardShape) {
		case CIRCLE_ST:
			break;
		case RECTANGLE_ST:
			cardShape.setPointCount(4);
			break;
		case TRIANGLE_ST:
			cardShape.setPointCount(3);
			break;
	}
	return cardShape;
}

void Card::setShapeColors (sf::Color &cardColor, sf::Color &outlineColor) {
	switch (_cardColor) {
		case RED_CT:
			cardColor = red;
			outlineColor = redTrans;
			break;
		case PURPLE_CT:
			cardColor =  purple;
			outlineColor = purpleTrans;
			break;
		case GREEN_CT:
			cardColor = green;
			outlineColor = greenTrans;
			break;
	}
}
