#pragma once
#include <cstdlib>
#include <vector>
#include "Card.h"
#include "JockerCard.h"
#include "ColorChangingCard.h"
#include "ShapeChangingCard.h"

using std::vector;
const unsigned int NUM_OF_CARDS = 12;
const unsigned int BOARD_WIDTH = 700;
const unsigned int BOARD_HEIGHT = 700;

const sf::Color TopColor(255,255,255,90);
const sf::Color BoardColor(sf::Color::Black);


class Board {
public:
	// default constructor
	Board ();

	// destructor
	~Board();

	// FUNCTIONS:

	// Draws the cards to the render target is receives as a parameter.
	void drawCardsOnBoard (sf::RenderWindow &winToDrawOn);

	// Shuffles all cards on board
	void shuffleCards (sf::RenderWindow & win);

	// returns _cardOnBoard data memeber
	vector<Card*> *getCardsOnBoard () { return &_cardsOnBoard; };

	// returns the card at position i of _cardsOnBoard
	Card *getCard (int i) { return _cardsOnBoard.at(i);};

	// sets the card at position i on _cardsOnBoard
	void setCard (Card *newCard, int i) { _cardsOnBoard.at(i) = newCard; };

	// returns line on top
	sf::RectangleShape getLineOnTop () { return _line_on_top; };

private:
	// DATA MEMBERS:

	vector<Card*> _cardsOnBoard;				// vector containing pointers to all cards currently on board
	vector<sf::Vector2f> _coordinatesOfCards;	// vector containing locations of cards on board
	sf::RectangleShape _line_on_top;				// line on top of board on which lives, score and buttons are shown

	// FUNCTIONS:

	// Sets the locations for each of the cards to be placed on board.
	void setCardLocations ();

	// randomly creates 12 cards and returns a vector containing pointers to all of them
	void randomCardsCreation ();


};
