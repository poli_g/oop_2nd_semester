#include "ColorChangingCard.h"

void ColorChangingCard::setShapeColors (sf::Color &cardColor, sf::Color &outlineColor) {
	// setting the color of the card
	switch (_cardColor) {
		case RED_CT:
			cardColor = red;
			outlineColor = redTrans;
			break;
		case PURPLE_CT:
			cardColor =  purple;
			outlineColor = purpleTrans;
			break;
		case GREEN_CT:
			cardColor = green;
			outlineColor = greenTrans;
			break;
	}
}

void ColorChangingCard::changeColor () {
	srand((unsigned int)time(NULL));
	enum colors_t newColor = (enum colors_t)(rand() % 3);
	while (newColor == _cardColor) {
		newColor = (enum colors_t)(rand() % 3);
	}
	_cardColor = newColor;
}
