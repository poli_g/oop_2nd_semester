#pragma once
#include <cstdlib>
#include <SFML\Graphics.hpp>
#include <vector>

using std::vector;

//CONSTS:
const sf::Color purple (111,6,130);				// purple color
const sf::Color purpleTrans (111,6,130, 90);	// purple color - transparent
const sf::Color red (180, 3, 3);				// red color
const sf::Color redTrans (180, 3, 3, 90);		// red color - transparent
const sf::Color green (10, 223, 90);			// green color
const sf::Color greenTrans (10, 223, 90, 90);	// green color - trnasparent
const sf::Color tableGreen (30, 200, 80);		// Table color
const sf::Color cardColor (227, 222, 205);		// color of the card itself

const sf::Vector2f blankCardSize(100, 160);		// size of blank card
const unsigned int NUM_OF_TRAITS = 4;			// number of traits a card can have (color, shape, etc)
const float CARD_SHAPE_RADIUS = 17.5;			// radius of shapes on card

// ENUMS:
enum colors_t {RED_CT, PURPLE_CT, GREEN_CT, JOCKER_CT};
enum shapes_t {CIRCLE_ST, RECTANGLE_ST, TRIANGLE_ST, JOCKER_ST};
enum texture_t {FULL_TT, EMPTY_TT, STRIPES_TT, JOCKER_TT};
enum number_t {ZERO_NT, ONE_NT, TWO_NT, THREE_NT, JOCKER_NT};

class Card {
public:
	// CONSTRUCTORS:

	// Default constructor, randomly selects traits of card (shape, texture, color & number of shapes)
	Card();
	// selective constructor, can create a specific card
	Card (enum colors_t color, enum shapes_t shapes, 
				enum texture_t texture, enum number_t number,
				sf::Vector2f location);
	// copy constructor
	Card (const Card &card);
	
	// FUNCTIONS:

	// draws a single card 
	virtual void drawCard (const sf::Vector2f &cardLocation, sf::RenderWindow &window);

	// returns the _isSelected data member
	bool getIsSelected () { return _isSelected; };
	
	// sets the isSelected data member
	void setIsSelected (bool val) { _isSelected = val; };
	
	// sets the _location data member
	void setLocation (sf::Vector2f location) { _location = location; };

	// returns the _blankCard data member
	sf::RectangleShape* getBlankCard () { return &_blankCard; };
	
	// changes the color of the card:
	virtual void changeColor () {;};

	// changes the shape of the card:
	virtual void changeShape () {;};

	// returns number of shapes
	enum number_t getNumOfShapes () { return _numOfShapes; };

	// returns card shape's color
	enum colors_t getCardColor () { return _cardColor; };

	// returns card's shape
	enum shapes_t getCardShape () { return _cardShape; };

	// returns card shape's texture
	enum texture_t getCardTexture () { return _cardTexture; };

	// OPERATORS:
	bool operator == (const Card &other) const; 
	bool operator != (const Card &other) const {return !(*this == other);}; 
	Card operator = (const Card &other);	

protected:
	// DATA MEMBERS:
	sf::Vector2<float> _location;	// location on card on screen
	sf::RectangleShape _blankCard;	// blank card
	enum colors_t _cardColor;		// color of shape drawn on card
	enum shapes_t _cardShape;		// shape drawn on card
	enum texture_t _cardTexture;	// texture of shape drawn on cardF
	enum number_t _numOfShapes;		// number of shapes drawn on card
	bool _isSelected;				// is the card selected by player?
	sf::Texture _stripeTexture;		// holds the striped texture of the cards shape

	// FUNCTIONS:
	// sets the shape to be drawn on the card
	virtual sf::CircleShape setCardShape ();

	// sets the color and outline of shape on card
	virtual void setShapeColors (sf::Color &cardColor, sf::Color &outlineColor);


};