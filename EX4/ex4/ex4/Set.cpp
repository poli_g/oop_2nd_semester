#pragma once
#include <cstdlib>
#include <iostream>
#include "Card.h"
#include "Board.h"
#include "Button.h"
#include "GameController.h"

int main () {
	GameController gameController;
	gameController.runGame();
	return 0;
}