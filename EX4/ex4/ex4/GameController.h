#pragma once
#include <cstdlib>
#include <vector>
#include "Button.h"
#include "Card.h"
#include "Board.h"
#include <sstream>
#include <iosfwd>

const unsigned int MENU_WIDTH = 600;
const unsigned int MENU_HEIGHT = 300;
const unsigned int SIZE_OF_SCORE = 30;

const sf::Vector2f livesSize(100, 30);

using std::vector;

class GameController {
public:
	// CONSTRUCTOR:
	// default constructor
	GameController ();
	
	// destructor
	~GameController ();

	// FUNCTIONS:

	// runs the game, manages the score, all the user triggered events and etc
	void runGame ();

private:
	// DATA MEMBERS:
	sf::RenderWindow _menuWindow;	// menu window that pops up when "menu" button is pressed
	vector<Button*> _gameButtons;	// buttons in game (menu, help, etc)
	Board _gameBoard;				// game's board containing all cards
	unsigned int _selectionCounter;	// counts how many cards are currently selected
	unsigned int _score;            // score of the game
	sf::Texture _oneLife;			// lives of player 
	sf::Texture _twoLives;			// lives of player 
	sf::Texture _threeLives;		// lives of player 
	sf::Text _textScore;            // the buttons name
	sf::Font _font;                 // font we use (we using only one in this exersize)
	bool _keepPlaying;				// so we know if we need to keep playing
	sf::Texture _frame;				// frame for all "windows" (menu, msgs etc)

	// FUNCTIONS

	// changes appearance between selected/unselected card (with/without outline)
	void toggleSelectionOutline (bool selected, Card* card);

	// clears selection from selected set of cards (wether the set was correct or not)
	void clearSelections();

	// handles events from user in function runGame()
	void handleEvents (sf::RenderWindow &window, const sf::Event &event);

	// reduces user score (happens when cards are shuffled)
	void reduceScore() { if (_score >= 10) _score -= 10; };

	// opens a msg window with the string given to it displayed as text
	void openMessageWindow (std::string);

	// executes all special cards' traits (change color or shape)
	void runSpecialCardTraits ();

	// Opens menu window
	void openQuitMenu ();

	// Checks if the selected cards are a "set" (according to the rules of the game)
	bool checkSet ();

	// creates the buttons for the game (menu, help, etc)
	void createGameButtons ();

	// draws the buttons of the game to the screen
	void drawButtons (sf::RenderWindow &window);                                 

	// handles the selected card, increases selected cards counter, changes appearance of card
	void handleCardSelection(int index);

	// Draws player's score on the render target it receives as a parameter.
	void drawScoreOnBoard (sf::RenderWindow &winToDrawOn);

	//this function increasing a score in 10 points 
	void increaseScore () { _score += 10 ; };

	// replaces the cards who's indexes were passed in the vector
	void replaceCards (const vector <unsigned int> &indexes);

	// reduces lives of player by one
	void reduceLives(sf::RectangleShape &lives);

	// asks user if he wants to play again, if not closes the game
	void closeGame(sf::RenderWindow &win);
};
