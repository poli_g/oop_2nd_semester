#include "JockerCard.h"

JockerCard::JockerCard() {
	_cardColor = JOCKER_CT;
	_cardShape = JOCKER_ST;
	_cardTexture = JOCKER_TT;
	_numOfShapes = JOCKER_NT;

	_jocker.create(200, 200);
	_jocker.loadFromFile("joker-card.jpg");
	_jocker.setRepeated(true);	
}


void JockerCard::drawCard (const sf::Vector2f &cardLocation, sf::RenderWindow &window) {
	// setting parameters for blank card:
	_blankCard.setSize(blankCardSize);
	_blankCard.setOrigin(_blankCard.getSize() / 2.f);
	_blankCard.setFillColor(cardColor);
	_blankCard.setPosition(cardLocation);

	_blankCard.setTexture(&_jocker);

	// drawing blank card to window:
	window.draw(_blankCard);
}