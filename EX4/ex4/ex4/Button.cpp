#include "Button.h"

/******************* C-TORS *********************************************/

Button::Button(): _rec(sf::RectangleShape(sf::Vector2f(150, 80))),
				  _location (sf::Vector2f(100,50)), _isPressed (false) {
					
	std::string str("Default");
	_nameOfButton = new sf::Text(str, _font, 30);
	_rec.setOrigin(_rec.getSize() / 2.f);
	_rec.setPosition(_location);
	setFont();

}

Button::Button(const sf::Vector2f location, const std::string text):
				_location (location), _isPressed (false) {

	_rec = sf::RectangleShape(sf::Vector2f(150, 80));
	_rec.setOrigin(_rec.getSize() / 2.f);
	_rec.setPosition(_location);

	_texture.loadFromFile ("button_up.png");
	
	//filling size of rectangle
	_texture.create(_rec.getLocalBounds().width,
	_rec.getLocalBounds().height);
	
	//loading font from file
	_font.loadFromFile("forte.ttf");
	_nameOfButton = new sf::Text(text, _font, 30);
	setFont();

}

/*********************** FUNCTIONS ****************************************/
void Button::setFont(){

	//loading our font from the file
	
	_nameOfButton->setFont(_font);                  //setting font and size 
	_nameOfButton->setCharacterSize(SIZE_OF_FONT);
	_nameOfButton->setColor(sf::Color(255,255,255));
	
	//now we set the origin point of a text to be a middle
	//of a rectangle that boundet it
	_nameOfButton->setOrigin(_nameOfButton->getLocalBounds().width/2.f,
		_nameOfButton->getLocalBounds().height/2.f ); 
	//setting a location of a text to be a middle of a button
	_nameOfButton->setPosition(_location); 

}
//drawing a prosess of press and unpressing the button  
void Button::drawPressButton(sf::RenderWindow & win){

	pressButton();
	drawButton(win);
	win.display();
	unpressButton();
	drawButton(win);
	win.display();
}

void Button::drawButton (sf::RenderWindow & window){

	//loadind texture of a button
	if (_isPressed){
		_texture.loadFromFile ("button_down.png");
	}
	else
		_texture.loadFromFile ("button_up.png");
	
	_rec.setTexture(&_texture);

	//draws a buton and a text on it
	window.draw (_rec);
	window.draw(*_nameOfButton);
	
}
