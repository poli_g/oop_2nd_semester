#include "ShapeChangingCard.h"

sf::CircleShape ShapeChangingCard::setCardShape () {
	sf::CircleShape cardShape(CARD_SHAPE_RADIUS);
	
	// setting cards shape
	switch (_cardShape) {
		case CIRCLE_ST:
			break;
		case RECTANGLE_ST:
			cardShape.setPointCount(4);
			break;
		case TRIANGLE_ST:
			cardShape.setPointCount(3);
			break;
	}
	return cardShape;
}

void ShapeChangingCard::changeShape () {
	srand((unsigned int)time(NULL));
	enum shapes_t newShape = (enum shapes_t)(rand() % 3);
	while (newShape == _cardShape) {
		newShape = (enum shapes_t)(rand() % 3);
	}
	_cardShape = newShape;
}