#include "GameController.h"

GameController::~GameController () {
	while (_gameButtons.size() > 0) {
		_gameButtons.erase(_gameButtons.begin());
	}
}

GameController::GameController () {
	_menuWindow.setTitle(std::string("Quit"));
	_menuWindow.setSize(sf::Vector2u(MENU_WIDTH, MENU_HEIGHT));
	_menuWindow.setVerticalSyncEnabled(true);
	_menuWindow.setVisible(false);
	 createGameButtons();               
	_selectionCounter = 0;
	_score = 0;
	_font.loadFromFile("forte.ttf");
	_textScore.setFont(_font);

	// loading lives:
	_oneLife.create((unsigned int)livesSize.x,(unsigned int) livesSize.y);
	_twoLives.create((unsigned int)livesSize.x,(unsigned int) livesSize.y);
	_threeLives.create((unsigned int)livesSize.x,(unsigned int) livesSize.y);
	_oneLife.loadFromFile("heart.png");
	_twoLives.loadFromFile("2heart.png");
	_threeLives.loadFromFile("3heart.png");

	// loading frame
	_frame.create(BOARD_HEIGHT, BOARD_WIDTH);
	_frame.loadFromFile("frame1.png");
}

// DESTRUCTOR

void GameController::drawScoreOnBoard (sf::RenderWindow &winToDrawOn) {

		//sting keeping the word "score" and a score of the game 
		std::stringstream sstr;
	    sstr << "Score: " <<_score <<  std::endl;

		sf::Text txt(sstr.str(), _font, SIZE_OF_SCORE);
		txt.setColor(sf::Color::Yellow);

		//seting origin for score
		txt.setOrigin(txt.getLocalBounds().width/2.f,
		txt.getLocalBounds().height/2.f ); 
		//setting a location of a text 
		txt.setPosition(sf::Vector2f(4 * (BOARD_WIDTH /5), 
				BOARD_HEIGHT /13)); 
		winToDrawOn.draw(txt);

}

void GameController::openQuitMenu () {
	openMessageWindow("Do You really want to quit?");
}

bool GameController::checkSet () {
	bool equalColor = false, diffColor = false, 
		 equalShape = false, diffShape = false,
		 equalTexture = false, diffTexture = false,
		 equalNum = false, diffNum = false;
	vector <unsigned int> set;	// to save indexes in _cardOnBoard of selected cards for set

	// initializing set vector:
	for (unsigned int j = 0; j < NUM_OF_CARDS; j++) {
		if (_gameBoard.getCard(j)->getIsSelected())
			set.push_back(j);
	}

	// setting to true relative booleans
	for (unsigned int i = 0; i < set.size() - 1; i++) {
		if (_gameBoard.getCard(set.at(i))->getCardColor() != JOCKER_CT && 
			_gameBoard.getCard(set.at(i + 1))->getCardColor() != JOCKER_CT) {
			if (_gameBoard.getCard(set.at(i))->getCardColor() == _gameBoard.getCard(set.at(i + 1))->getCardColor())
				equalColor = true;
			else 
				diffColor = true;
		}
		if (_gameBoard.getCard(set.at(i))->getCardShape() != JOCKER_ST &&
			_gameBoard.getCard(set.at(i + 1))->getCardShape() != JOCKER_ST) {
			if (_gameBoard.getCard(set.at(i))->getCardShape() == _gameBoard.getCard(set.at(i + 1))->getCardShape())
				equalShape = true;
			else 
				diffShape = true;
		}
		if (_gameBoard.getCard(set.at(i))->getNumOfShapes() != JOCKER_NT &&
			_gameBoard.getCard(set.at(i + 1))->getNumOfShapes() != JOCKER_NT) {
			if (_gameBoard.getCard(set.at(i))->getNumOfShapes() == _gameBoard.getCard(set.at(i + 1))->getNumOfShapes())
				equalNum = true;
			else 
				diffNum = true;
		}
		if (_gameBoard.getCard(set.at(i))->getCardTexture() != JOCKER_TT &&
			_gameBoard.getCard(set.at(i + 1))->getCardTexture() != JOCKER_TT) {
			if (_gameBoard.getCard(set.at(i))->getCardTexture() == _gameBoard.getCard(set.at(i + 1))->getCardTexture())
				equalTexture = true;
			else 
				diffTexture = true;
		}
	}

	// checking (according to booleans) if the cards are a set
	if (equalColor == true && diffColor == true ||
		equalNum == true && diffNum == true ||
		equalShape == true && diffShape == true ||
		equalTexture == true && diffTexture == true)
		return false;
	// if cards are a set we need to place new cards instead of them:
	replaceCards (set);
	return true;
}

void GameController::replaceCards (const vector <unsigned int> &indexes) {
	srand ((unsigned int)time (NULL));
	for (unsigned int i = 0; i < indexes.size(); i++) {
		unsigned int index = indexes.at(i);
		int num = rand () % 12 + 1;
		Card *newCard;
		switch (num) {
			case 1:
				newCard = new ColorChangingCard();
				break;
			case 2:
				newCard = new ShapeChangingCard();
				break;
			case 3:
				newCard = new JockerCard();
				break;
			default:
				newCard = new Card();
				break;
		}
		_gameBoard.getCardsOnBoard()->erase(_gameBoard.getCardsOnBoard()->begin() + index);
		_gameBoard.getCardsOnBoard()->insert(_gameBoard.getCardsOnBoard()->begin() + index, newCard);
	}
}


//creating buutons of the game
void GameController::createGameButtons () {

	//create a new menu button and insert into vector of buttons
	_gameButtons.push_back(new Button (sf::Vector2f (1 * (BOARD_WIDTH /5), 
				BOARD_HEIGHT /13), std::string ("Quit")));
	//create a Shuffle button and insert into vector of buttons
	_gameButtons.push_back(new Button (sf::Vector2f (2 * (BOARD_WIDTH /5), 
				BOARD_HEIGHT /13), std::string ("Shuffle!")));
	//create buttons for the menu window and insert into vector of buttons
	_gameButtons.push_back(new Button (sf::Vector2f (MENU_WIDTH/3, MENU_HEIGHT/2),
		std::string ("Continue")));
	_gameButtons.push_back(new Button (sf::Vector2f (2*MENU_WIDTH/3, MENU_HEIGHT/2),
		std::string ("Quit")));
}

//drawing buttons of the game
void GameController::drawButtons (sf::RenderWindow &window) {  
	
	_gameButtons[0]->drawButton(window);	// drawing buttons (menu, shuffle, etc)
	_gameButtons[1]->drawButton(window);	
}

//this function actualy runs the game
void GameController::runGame () {
	sf::RenderWindow window(sf::VideoMode(BOARD_WIDTH, BOARD_HEIGHT), "Set!");
	sf::RectangleShape livesRec (livesSize);		// rec containing lives of player
	livesRec.setPosition(sf::Vector2f((float)4 * (BOARD_WIDTH /7.5),(float) BOARD_HEIGHT /15));
	livesRec.setTexture(&_threeLives);

	// settings to reduce number of irrelevant events
	window.setJoystickThreshold(100000);
	window.setVerticalSyncEnabled(true);

	while (window.isOpen()) {
		if (_keepPlaying) {
			window.clear(BoardColor);				// clearing screen
			window.draw(_gameBoard.getLineOnTop()); // drawing light line on top for stats section of the game
			window.draw(livesRec);					// draws the rectangle with the lives texture
			_gameBoard.drawCardsOnBoard(window);	// drawing cards to the board
			drawButtons(window);                    //call to funstion that draws a buttons
			drawScoreOnBoard (window);				// drawing user score on the board
			window.display();						// displaying all objects to the screen
		
			// if a set was selected (a legal/not legal set, doesn't matter)
			if (_selectionCounter == 3) {
				if (!checkSet())
					reduceLives(livesRec);
				else 
					increaseScore();
				clearSelections();	
				runSpecialCardTraits ();
			}

			// if one of the buttons was pressed we wish to unpress it:
			if (_gameButtons[1]->getIsPressed())
				_gameButtons[1]->unpressButton();

			if (_gameButtons[0]->getIsPressed())
				_gameButtons[0]->unpressButton();
		
			sf::Event event;
			while (window.pollEvent(event))
				handleEvents(window, event);
		}
		else
			window.close();
	}
}

void GameController::handleEvents (sf::RenderWindow &window, const sf::Event &event) {
	// close window:
	if (event.type == sf::Event::Closed || 
		(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
		window.close();

	
	if (event.type == sf::Event::MouseButtonPressed) {  // user clicked mouse
		// we check this a lot so we defined it as a variable:
		sf::Vector2f mouseClick((float)sf::Mouse::getPosition(window).x,(float)sf::Mouse::getPosition(window).y);
		// cards were clicked:
		for (int i = 0; i < NUM_OF_CARDS; i++) {
			if (_gameBoard.getCard(i)->getBlankCard()->getGlobalBounds().contains(mouseClick))
				handleCardSelection(i);
		}

		//if button "Shuffle" is pressed
		if(_gameButtons[1]->getRect().getGlobalBounds().contains(mouseClick)){
			if (_score >= 50) {		// user has enough score to buy a shuffle
				_gameButtons[1]->drawPressButton(window);
				_gameBoard.shuffleCards(window);
				_selectionCounter = 0;
				reduceScore();
			}
			else{
			_gameButtons[1]->drawPressButton(window);
			}
			
		}//end of if
		
		if(_gameButtons[0]->getRect().getGlobalBounds().contains(mouseClick)){
			_gameButtons[0]->drawPressButton(window);
			openQuitMenu();
			
		}//if
	}//end of if(event.type == sf::Event::MouseButtonPressed)
}

void GameController::openMessageWindow (std::string msg) {
	sf::RenderWindow win(sf::VideoMode(MENU_WIDTH, MENU_HEIGHT, 32),"Don't leave this fantastic game!!!");
	sf::Text message(msg);
	message.setFont(_font);
	message.setPosition(130,50);
	
	while (win.isOpen()) {
		win.clear();
		
		_gameButtons[2]->drawButton(win);	// drawing buttons (quit, continue)
		_gameButtons[3]->drawButton(win);	

		win.draw(message);
		win.display();
		sf::Event event;
		while (win.pollEvent(event))
			if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
				win.close();
			if (event.type == sf::Event::MouseButtonPressed) {
			
				sf::Vector2f mouseClick((float)sf::Mouse::getPosition(win).x,(float)sf::Mouse::getPosition(win).y);
				// Buttons were clicked:
				if(_gameButtons[2]->getRect().getGlobalBounds().contains(mouseClick)){
					_gameButtons[2]->drawPressButton(win);
					win.close();
				}
				else if(_gameButtons[3]->getRect().getGlobalBounds().contains(mouseClick)){
					_gameButtons[3]->drawPressButton(win);
					// flag for understand when player want to finish a game
					_keepPlaying = false; 
					win.close();
				}
		}
	}
}

void GameController::handleCardSelection (int index) {
	switch (_gameBoard.getCard(index)->getIsSelected()) {
		case true:	// card was already selected, we need to undo selectionS
			_gameBoard.getCard(index)->setIsSelected(false);
			toggleSelectionOutline(_gameBoard.getCard(index)->getIsSelected(),
								   _gameBoard.getCard(index));
			_selectionCounter--;
			break;
		case false:	// card was not selected, we need to select it
			// checking if max num of cards is selected:
			if (_selectionCounter < 3) {
				_gameBoard.getCard(index)->setIsSelected(true);
				toggleSelectionOutline(_gameBoard.getCard(index)->getIsSelected(),
									   _gameBoard.getCard(index));
				_selectionCounter++;
			}
			break;
	}	// end of switch
}

void GameController::toggleSelectionOutline (bool selected, Card* card) {
	if (selected) {
		card->setIsSelected(true);
		card->getBlankCard()->setOutlineThickness(3.f);
		card->getBlankCard()->setOutlineColor(sf::Color::Yellow);
	}
	else {
		card->setIsSelected(false);
		card->getBlankCard()->setOutlineThickness(3.f);
		card->getBlankCard()->setOutlineColor(sf::Color::Transparent);
	}

}

void GameController::clearSelections() {
	for (unsigned int i = 0; i < NUM_OF_CARDS; i++) {
		if (_gameBoard.getCard(i)->getIsSelected())
			toggleSelectionOutline(false, _gameBoard.getCard(i));
	}
	
	_selectionCounter = 0;
}

void GameController::runSpecialCardTraits () {
	// special cards traits:
	for (unsigned int i = 0; i < NUM_OF_CARDS; i++) {
		_gameBoard.getCard(i)->changeColor();
		_gameBoard.getCard(i)->changeShape();
	}

	// making one card disappear
	vector <unsigned int> index;
	srand((unsigned int)time (NULL));
	index.push_back((unsigned int)rand % NUM_OF_CARDS);
	replaceCards(index);
}

void GameController::reduceLives(sf::RectangleShape &lives) {
	if (lives.getTexture() == &_threeLives)
		lives.setTexture(&_twoLives);
	else if (lives.getTexture() == &_twoLives)
		lives.setTexture(&_oneLife);
	else if (lives.getTexture() == &_oneLife)
		_keepPlaying = false;
}
