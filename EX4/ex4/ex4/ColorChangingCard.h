#include "Card.h"

class ColorChangingCard: public Card {
public:
protected:
	// FUNCTIONS:

	// sets the color and outline of shape on card
	void setShapeColors (sf::Color &cardColor, sf::Color &outlineColor);
	
	// changes the color of the card:
	void changeColor ();
};
