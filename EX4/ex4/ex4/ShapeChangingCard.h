#include "Card.h"

class ShapeChangingCard: public Card {
public:
protected:
	// FUNCTIONS:

	// sets the shape to be drawn on the card
	virtual sf::CircleShape setCardShape ();

	// changes the shape of the card:
	void changeShape ();
};
