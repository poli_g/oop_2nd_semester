#include "Board.h"

Board::Board () {
	randomCardsCreation();
	setCardLocations();
	_line_on_top.setSize(sf::Vector2f((float)BOARD_WIDTH,(float)(BOARD_HEIGHT/7)));
	_line_on_top.setFillColor (TopColor);
}

Board::~Board() {
	while (_cardsOnBoard.size() > 0) {
		_cardsOnBoard.erase(_cardsOnBoard.begin());
	}
}

void Board::randomCardsCreation () {
	vector<Card*> cardsVector;		// will hold pointers to created cards
	srand((unsigned int)time(NULL));

	// we randomly select a number between 1-12, if num is above 10 we 
	// create a special card, otherwise a regular one:
	while (_cardsOnBoard.size() < NUM_OF_CARDS) {
		int num = rand() % 12 + 1;
		if (num == 10) {
			// later on here we create a special card! right now regular
			Card *newCard;
			newCard = new JockerCard();
			_cardsOnBoard.push_back(newCard);
		}
		else if (num == 11){
			Card *newCard;
			newCard = new ShapeChangingCard();
			_cardsOnBoard.push_back(newCard);
		}
		else if (num == 12){
			Card *newCard;
			newCard = new ColorChangingCard();
			_cardsOnBoard.push_back(newCard);
		}
		else {
			Card *newCard;
			newCard = new Card();
			_cardsOnBoard.push_back(newCard);
		}
	}	// end of while
}


void Board::setCardLocations () {
	// this function calculating place  for the cards by dividion of
	//width and a height of the window and inserts card locations into
	//vector of locations (data memeber of class board)
	int cardCounter = 0;
	for (int j = 1; j <= NUM_OF_CARDS / 4; j++)
		for (int i = 1; i <= NUM_OF_CARDS / 3; i++) {
			sf::Vector2f newLocation((float)(i) * (BOARD_WIDTH / 5), (j) * (float)(BOARD_HEIGHT /3.5));
			_coordinatesOfCards.push_back(newLocation);
			_cardsOnBoard.at(cardCounter)->setLocation(newLocation);
			cardCounter++;
		}
}

void Board::drawCardsOnBoard (sf::RenderWindow &winToDrawOn) {
	for (int i = 0; i < NUM_OF_CARDS; i++) {
		_cardsOnBoard.at(i)->drawCard(_coordinatesOfCards.at(i), winToDrawOn);
	}
}

void Board::shuffleCards (sf::RenderWindow &win) {
	_cardsOnBoard.clear();
	randomCardsCreation();
	drawCardsOnBoard(win);
}

/*
vector<Card*> Board::groupChosenCards () {
	vector<Card*> selectedCards;		// vector to save pointers to selected cards
	for (unsigned int i = 1; i <= _cardsOnBoard.size(); i++) {
		if (_cardsOnBoard.at(i)->getIsSelected())
			selectedCards.push_back(_cardsOnBoard.at(i));
	}
	return selectedCards;
}
*/
