#include "Card.h"

class JockerCard: public Card {
public:
	// constructor:
	JockerCard();
	// draws the jocker card
	void drawCard (const sf::Vector2f &cardLocation, sf::RenderWindow &window);
protected:
	// DATA MEMBERS:
	sf::Texture _jocker;

};
