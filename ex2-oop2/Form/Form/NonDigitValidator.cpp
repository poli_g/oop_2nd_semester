#include "NonDigitValidator.h"

bool NonDigitValidator::validate(string value, string& error) {
	
	for (int i = 0; i < value.size(); i++)
		if (!isalpha(value[i])) {
			if (value[i] != '-') {
				error = "Can't contain digits!";
				return false;
			}
		}

	error = "";
	return true;
}