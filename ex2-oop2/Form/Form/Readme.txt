******************************************************************************************
            
                              R E A D M E


*******************************************************************************************


1)	OOP2 : EX2-Templates

2)	By:	Polina Granot ID:	307696070
		Shur Svetlana ID:	318032745


3)	In this exersize we implemented a simple Console application used to fill forms and 
	collecting information from the user by using templates.


4)	Added files:

	Field.h					-	Template Class contain data member _field Val type T. 
	FormField.h/cpp         -	Abstract base class of Field contain data members as strings.
	Form.h/cpp				-	Contains two vectors :vector <FormValidator*> and vector <FormField*> 
	MonthValue.h/cpp		-   Class representing a month, contains data member as int _month.
	IDValidator.h/cpp		-	Derived class of FieldValidator.
	RangeValidator.h/cpp	-	Derived class of FieldValidator.
	IDValidator.h/cpp		-	Derived class of FieldValidator.
	NonDigitValidator.h/cpp	-	Derived class of FieldValidator.
	NonNegativeValidator.h/cpp-	Derived class of FieldValidator.
	AgeYearValidator.h/cpp	-	Derived class of FormValidator.
	FieldValidator.h/cpp	-	Template base class of different Validators.
	FormValidator.h/cpp		-	Base class of AgeYearValidator.
	Main.cpp				-	contains main function.

	
5) Data Structures:			-	mostly we used vector from standart library. Vector of <FormField*> and 
								vector of <FormValidator*> in Form class so it's allowing to use polymorthism 
								for different actions on different forms and validators.

6) Algorithms:				-	no dificult algorithms where used in this exersize. All callculations of 
								validation of ID number tooken from Vikipedia.
								   
7) Known Bugs:				-	no bugs were detected


8) Additional Comments:		-	Age field can be iniialized using: poli-granot
							