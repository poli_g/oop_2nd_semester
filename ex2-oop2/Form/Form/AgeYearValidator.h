#pragma once
#include "FormValidator.h"
#include "Field.h"
#include <ctime>

template <class T1, class T2>
class AgeYearValidator : public FormValidator {
public:
	AgeYearValidator(T1 &age, T2 &year): _age(age), _year(year) {
	}

	~AgeYearValidator() {
	}

	bool validate(string&);

private:
	T1 &_age;
	T2 &_year;
};

template <class T1, class T2> 
bool AgeYearValidator<T1, T2>::validate(string &error) {
	/*	THIS DIDN'T WORK!!!
	time_t curTime = time(NULL);
	tm *tmData = localtime(curTime);
	int currYear = tmData->tm_year + 1900;
	*/

	int currYear = 2013;
	if ((currYear - _year.getFieldVal()) != _age.getFieldVal() && (currYear - _year.getFieldVal()) != _age.getFieldVal()+1) {
		error = "Age & year don't match!";

		// setting the display parameter of checked fields to true so they are displayed again for user to correct:
		_age.setDisplayField(true);
		_year.setDisplayField(true);

		return false;
	}
	error = "";
	return true;
}