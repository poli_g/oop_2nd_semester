#pragma once
#include "Configurations.h"

class MonthValue {
public:
	MonthValue(): _month(0) {};
	MonthValue(int month):_month(month) {};
	
	~MonthValue();

	// printing operator
	friend ostream  &operator << (ostream&, MonthValue &);
	// = operator with int
	void operator = (int val) {_month = val;};
	// cast operator to int from monthvalue
	operator int()const { return _month;};
	// cast operator from string to monthVal
	MonthValue(string month);
	// input operator for istream & monthvalue:
	friend istream &operator >> (istream&, MonthValue&);

private:
	int _month;
};



