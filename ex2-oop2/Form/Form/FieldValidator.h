#pragma once
#include "Configurations.h"

template <class T>
class FieldValidator {
public:
	FieldValidator() {};
	virtual ~FieldValidator() {};

	virtual bool validate(T, string&) = 0;
};

