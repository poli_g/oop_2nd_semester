#include "IDValidator.h"
#include <string>
#include "math.h"

bool IDValidator::validate(long id, string &error) {
	
	long int id_array[ID_LENGHT];

	if( id < MIN_ID || id > MAX_ID) {
		error = "The ID is not of right length";
		return false;
	}

	//converting id number into int array 
	for (int i = 0, j = ID_LENGHT-1; i < ID_LENGHT; i++, j--){
		id_array[i] = 0;
		id_array[i] =  id / pow(10.f,j);
		id = id - (id_array[i]*pow(10.f,j));
	}

	int sum = 0, incNum = 0, bikoret = id_array[ID_LENGHT-1];
	
	for (unsigned int i = 0; i < ID_LENGHT; i++){

                incNum = int(id_array[i]) * ((i % 2) + 1);
				//multiply digit by 1 or 2
                sum += (incNum > 9) ? incNum - 9 : incNum;
				//sum the digits up and add to counter
        
	}

		if(sum %10 ==0){
			error = "";
			return true;
		}
		else {
			error = "Wrong check digit!";
			return false;
		}
}