#pragma once
#include "FieldValidator.h"

template <class T>
class RangeValidator : public FieldValidator<T> {
public:
	RangeValidator(int low, int high): _lowerBound(low), _upperBound(high) {
	}

	~RangeValidator() {
	}

	virtual bool validate(T, string&);

private:
	int _lowerBound;
	int _upperBound;
};

template <class T>
bool RangeValidator<T>::validate(T value, string &error) {
	// implement cast to int operator
	if (value < _lowerBound || value > _upperBound) {
		error = "Out of range";
		return false;
	}
	error = "";
	return true;
}