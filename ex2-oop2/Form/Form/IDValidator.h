#pragma once
#include "FieldValidator.h"

const long MAX_ID = 999999999;
const long MIN_ID = 1111111;
const unsigned int ID_LENGHT = 9;


class IDValidator :	public FieldValidator<long> {
public:
	IDValidator() {};

	~IDValidator() {};

	bool validate(long, string&);
};

