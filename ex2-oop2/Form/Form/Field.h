#pragma once
#include "FormField.h"

template <class T>
class Field : public FormField {
public:
	Field (string req) {
		_requestFromUser = req;
	};
	~Field() {};

	// validates that the field is correct
	virtual bool validateField();
	// adds another validator to the field
	void addValidator(FieldValidator<T> *);

	void initField();
	// returns the field value
	T getFieldVal () {return _fieldVal;};

private:
	// value of field
	T _fieldVal;
	// contains validators of the field
	vector <FieldValidator<T>*> _fieldValidators;
};

template <class T>
void Field<T>::initField() {
	// printing request:
	cout << _requestFromUser << endl;

	// getting answer from user:
	cin >> _userInput;

	// converting userInput string to value:
	stringstream(_userInput) >> _fieldVal;
};

template <class T>
bool Field<T>::validateField () {
	for (unsigned int i = 0; i < _fieldValidators.size(); i++) {
		if (!_fieldValidators[i]->validate(_fieldVal, _errorMsg))
			return false;
	}
	return true;
};

template <class T>
void Field<T>::addValidator(FieldValidator<T> *validator) {
	_fieldValidators.push_back(validator);
};