#pragma once
#include "FormValidator.h"
#include "Field.h"

class Form{

public:
	Form();
	~Form();

	// adds another field to the form
	void addField(FormField* field) {_formFields.push_back(field);};
	// fills form getting input from user
	void fillForm();
	// validates all form's fields & form itself
	bool validateForm();
	// adds another validator to the form
	void addValidator(FormValidator *validator) {
		_formValidators.push_back(validator);
	};
	friend ostream  &operator << (ostream&, const Form &);
private:
	// contains validators of form
	vector <FormValidator*> _formValidators;
	// contains the fields of the form
	vector <FormField*> _formFields;
	// string containing error msg if validation returned false
	string _errorMsg;
};
