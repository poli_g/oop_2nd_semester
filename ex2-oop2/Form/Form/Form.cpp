#include "Form.h"

Form::Form() {
}

Form::~Form() {
}

ostream  &operator << (ostream& stream, const Form &toDisplay) {
	for (unsigned int i = 0; i < toDisplay._formFields.size(); i++) {
		stream << "-------------------------------------------------------------------------" << endl;
		stream << toDisplay._formFields[i]->getRequestString() << " = " << toDisplay._formFields[i]->getUserInput() << '\t' << toDisplay._formFields[i]->getErrorMsg() << endl;
		stream << "-------------------------------------------------------------------------" << endl;
	}
	stream << toDisplay._errorMsg << endl;
	return stream;
}

void Form::fillForm() {
	for (unsigned int i = 0; i < _formFields.size(); i++) {
		if (_formFields[i]->getDisplayField())
			_formFields[i]->initField();
	}
}

bool Form::validateForm() {
	bool retVal = true;
	// validating fields:
	for (unsigned int i = 0; i < _formFields.size(); i++) {
		if (!_formFields[i]->validateField())
			retVal = false;
		else 
			_formFields[i]->setDisplayField(false);
	}

	// validate the form itself:
	for (unsigned int i = 0; i < _formValidators.size(); i++)
		if (!_formValidators[i]->validate(_errorMsg)) {
			retVal = false;
		}

	return retVal;
}