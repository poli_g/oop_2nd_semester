#pragma once
#include "FieldValidator.h"
#include "Configurations.h"

class NonDigitValidator : public FieldValidator<string> {
public:

	NonDigitValidator() {};

	~NonDigitValidator() {};

	virtual bool validate(string, string&);
};

