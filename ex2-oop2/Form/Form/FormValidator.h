#pragma once
#include "Configurations.h"

class FormValidator {
public:
	FormValidator();
	~FormValidator();

	virtual bool validate(string&) = 0;
};

