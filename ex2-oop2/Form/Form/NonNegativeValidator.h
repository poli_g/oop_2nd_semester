#pragma once
#include "FieldValidator.h"

template <class T>
class NonNegativeValidator : public FieldValidator<T> {
public:
	NonNegativeValidator() {
	}

	~NonNegativeValidator() {
	}

	bool validate(T, string&);
};

template <class T>
bool NonNegativeValidator<T>::validate(T value, string &error) {
	if (value < 0) {
		error = "Must be positive";
		return false;
	}
	error = "";
	return true;
}