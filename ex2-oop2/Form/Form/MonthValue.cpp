#include "MonthValue.h"

MonthValue::~MonthValue() {
}

istream &operator >> (istream& stream, MonthValue& month) {
	stream >> month._month;
	return stream;
}
	
ostream  &operator << (ostream& stream, MonthValue &month) {
	stream << " = ";
	switch (month._month) {
	case 1:
		stream << "January";
		break;
	case 2:
		stream << "February";
		break;
	case 3:
		stream << "March";
		break;
	case 4:
		stream << "April";
		break;
	case 5:
		stream << "May";
		break;
	case 6:
		stream << "June";
		break;
	case 7:
		stream << "July";
		break;
	case 8:
		stream << "August";
		break;
	case 9:
		stream << "September";
		break;
	case 10:
		stream << "October";
		break;
	case 11:
		stream << "November";
		break;
	case 12:
		stream << "December";
		break;
	}
	stream << endl;
	cout << stream;
	return stream;
}

MonthValue::MonthValue(string intAsString){
     stringstream ss;
     ss << intAsString;
	 ss >> _month; //convert string into int and store it in "_month"
     ss.str("");   //clear the stringstream
     ss.clear();   //clear error flags
}
