#pragma once
#include "FieldValidator.h"
#include "Configurations.h"
#include "MonthValue.h"

class FormField {

public:

	FormField(): _display(true) {};
	virtual ~FormField();
	
	// returns the request string
	string getRequestString() {return _requestFromUser;};
	// returns the string of userInput
	string getUserInput(){return _userInput;};
	// validates field according to all validators field contains
	virtual bool validateField() = 0;
	// initializes field according to input from user
	virtual	void initField() = 0;
	// returns error msg
	string getErrorMsg() {return _errorMsg;};
	// returns bool _display
	bool getDisplayField () {return _display;};
	// sets bool _display
	void setDisplayField (bool val) {_display = val;};

protected:
	//request string
	string _requestFromUser;
	//user input
	string _userInput;
	// error string in case the validator returned false
	string _errorMsg;
	// boolean telling the form if to display field for user to fill/refill
	bool _display;
};

