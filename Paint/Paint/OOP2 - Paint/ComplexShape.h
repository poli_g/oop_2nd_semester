#pragma once
#include "shape.h"
#include "SimpleShape.h"

using std::vector;

class ComplexShape : public Shape {
public:
	ComplexShape();
	virtual ~ComplexShape();

	virtual void draw (sf::RenderWindow &) = 0;
	virtual void setSelection (bool) = 0;
};

