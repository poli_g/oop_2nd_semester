#pragma once
#include "SimpleShape.h"

using std::vector;

const float equalEdgeLen = 30;	// length of each edge in equal edge triangle
const float equalSideLen = 25;	// length of each side edge in equal sides triangle
const float baseLen = 10;		// length of base edge in equal sides triangle

class Triangle : public SimpleShape {
public:
	// C-TORS & D-TORS:
	Triangle(color color, float base = equalEdgeLen, float sideA = equalEdgeLen, float sideB = equalEdgeLen);
	virtual ~Triangle();

	// FUNCTIONS:
	virtual void draw(sf::RenderWindow &);
	virtual float getArea();
	virtual float getPerimeter();
	virtual void rotate ();

	//encreasing or making smaller the shape
	virtual void scale(float factor);
	// returns the first edge of the triangle
	float getEdge(){return _edges[0];};
	// returns height of triangle
	virtual float getHight(){return _triangleHeight;};	
	// returns the sf shape triangle:
	sf::ConvexShape &getTriangle () {return _triangle;};
	virtual void setSelection (bool);

protected:
	// DATA MEMBERS:
	vector <float> _edges;	// to save edges of the triangle
	sf::ConvexShape _triangle;
	float _triangleHeight;
	
};