#include "Button.h"

/******************* C-TORS *********************************************/

Button::Button(): _rec(sf::RectangleShape(sf::Vector2f(150, 80))),
				  _location (sf::Vector2f(100,50)){
					
	std::string str("Default");
	_nameOfButton = new sf::Text(str, _font, 30);
	_rec.setOrigin(_rec.getSize() / 2.f);
	_rec.setPosition(_location);
	setFont();

}

Button::Button(const sf::Vector2f location, const std::string text):
				_location (location) {

	_rec = sf::RectangleShape(sf::Vector2f(90, 40));
	_rec.setOrigin(_rec.getSize() / 2.f);
	_rec.setPosition(_location);

	_texture.loadFromFile ("button.png");
	
	//filling size of rectangle
	_texture.create(_rec.getLocalBounds().width,
	_rec.getLocalBounds().height);
	
	//loading font from file
	_font.loadFromFile("BRLNSDB.ttf");
	_nameOfButton = new sf::Text(text, _font, 30);
	setFont();

}

/*********************** FUNCTIONS ****************************************/
void Button::setFont(){

	//loading our font from the file
	
	_nameOfButton->setFont(_font);                  //setting font and size 
	_nameOfButton->setCharacterSize(SIZE_OF_FONT);
	
	//now we set the origin point of a text to be a middle
	//of a rectangle that boundet it
	_nameOfButton->setOrigin(_nameOfButton->getLocalBounds().width/2.f,
		_nameOfButton->getLocalBounds().height/2.f ); 
	//setting a location of a text to be a middle of a button
	_nameOfButton->setPosition(_location); 
	_nameOfButton->setColor(sf::Color::Black);

}

void Button::drawButton (sf::RenderWindow & window){

	//loadind texture of a button
	_texture.loadFromFile ("button.png");
	
	_rec.setTexture(&_texture);

	//draws a buton and a text on it
	window.draw (_rec);
	window.draw(*_nameOfButton);
	
}
