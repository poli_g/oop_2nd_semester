#include "StarOfDavid.h"

StarOfDavid::StarOfDavid(color color): _upperTriangle(color), _lowerTriangle(color) {
	_upperTriangle.setLocation(_location);
	_lowerTriangle.setLocation(_location);
	_lowerTriangle.getTriangle().setRotation(ANGLE*2);

}

StarOfDavid::~StarOfDavid() {
}

void StarOfDavid::scale(float factor) {
	_upperTriangle.scale(factor);
	_lowerTriangle.scale(factor);
}

void StarOfDavid::rotate () {
	_lowerTriangle.rotate();
	_upperTriangle.rotate();
	
}

void StarOfDavid::draw (sf::RenderWindow &win) {
	_upperTriangle.setLocation(_location);
	_lowerTriangle.setLocation(_location);
	_upperTriangle.draw(win);
	_lowerTriangle.draw(win);
}

float StarOfDavid::getArea() {return _upperTriangle.getArea()/9*12;};
float StarOfDavid::getPerimeter() {return _upperTriangle.getPerimeter()/9*12;};

void StarOfDavid::setSelection (bool selection) {
	// if we want to select the shape:
	if (selection) {
		_isSelected = selection;
		_upperTriangle.setSelection(true);
		_lowerTriangle.setSelection(true);
	}

	// if we want to unselect the shape:
	else {
		_isSelected = false;
		_upperTriangle.setSelection(false);
		_lowerTriangle.setSelection(false);
	}
}