#pragma once
#include <SFML/Graphics.hpp>
#include "Menu.h"
#include "Info.h"
#include "Canvas.h"

int main() {

	Canvas canvas;	// rectangle on which to draw all shapes
	Menu userMenu;	// user menu to select by which parameters to draw shapes
	Info shapesInfo(canvas);	// information of all shapes currently displayed on board

	//sf::RectangleShape canvas(sf::Vector2f(500, 500));
	sf::RenderWindow window(sf::VideoMode(700, 500), "Paint");
	sf::Vector2f mouseClick;	// to save mouse click location of user
	void (Menu::*arrowFunction) () = NULL;	// pointer to function executing arrow functionality
	void (Canvas::*buttonFunction)() = NULL;	// pointer to one of the menu buttons' functions
	const vector <Shape*> &shapesTemp = canvas.getShapes();	
	window.setVerticalSyncEnabled(true);
	
	while (window.isOpen()){

		window.clear();
		// drawing shapes:
		for (unsigned int i = 0; i < shapesTemp.size(); i++) {
			shapesTemp[i]->draw(window);
		}
		// drawing menu:
		userMenu.draw(window);
		shapesInfo.setData(canvas);
		shapesInfo.draw(window);
		window.display();

		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::MouseButtonReleased) {
				mouseClick.x = event.mouseButton.x;
				mouseClick.y = event.mouseButton.y;

				if (mouseClick.x <= 500 && mouseClick.y <= 500)	{
					canvas.createShape(mouseClick, userMenu.getChosenColor(), userMenu.getChosenShape());
					shapesInfo.setData(canvas);
				}
				else if (mouseClick.x > 500 && mouseClick.y <= 130) {
					// checking if an arrow was pressed:
					userMenu.setArrowFunction(arrowFunction, mouseClick);
					
					if (arrowFunction != NULL) {
						(userMenu.*arrowFunction)();
						arrowFunction = NULL;
					}
				}
				else if (mouseClick.x > 500 && mouseClick.y > 130) {
					canvas.setButtonFunction(buttonFunction, mouseClick, userMenu);

					if (buttonFunction != NULL) {
						(canvas.*buttonFunction)();
						buttonFunction = NULL;
					}
				}
			}
			// close window:
			if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
				window.close();
		}
				
	window.display();
	}//end of while

	return 0;
}
