#pragma once
#include "SimpleShape.h"
#include <cmath>

const float CIRCLE_RADIUS = 15;
const float PI = (float) 3.14;

class Circle : public SimpleShape {

public:

	Circle (color color, float  radius = CIRCLE_RADIUS);
	virtual ~Circle();

	// FUNCTIONS:
	virtual void draw(sf::RenderWindow &win);
	virtual float getArea() {return PI*pow(_radius,2);};
	virtual float getPerimeter () {return 2*PI*_radius;};
	virtual void rotate (){};
	virtual float getHight (){return _radius;};
	virtual void scale(float factor);
	virtual void setSelection (bool);
	// moves the circle down by 10 pixels:
	void moveDown();

protected:
	// DATA MEMBERS:
	float _radius;
	sf::CircleShape _sf_circle;
};

