#include "Trianlge.h"

Triangle::Triangle(color color, float base, float sideA, float sideB): _triangle(3), _triangleHeight(sqrt(pow(sideA, 2) - pow(base/2, 2))) {
	setColor(color);
	_triangle.setFillColor(_color);
	_triangle.setOutlineThickness(0);

	// initializing _edges vector
	_edges.resize(3);
	_edges[0] = base;
	_edges[1] = sideA;
	_edges[2] = sideB;

	// setting points of triangle
	_triangle.setPoint(0, sf::Vector2f(base/2, 0));
	_triangle.setPoint(1, sf::Vector2f(0, _triangleHeight));
	_triangle.setPoint(2, sf::Vector2f(base, _triangleHeight));
	_triangle.setOrigin(sf::Vector2f(base/2, 2*(_triangleHeight)/3 ));
}

Triangle::~Triangle() {
}

float Triangle::getArea() {
	return (_edges[0] * _triangleHeight)/2;
}

float Triangle::getPerimeter () {
	return (_edges[0] + _edges[1] + _edges[2]);
}

void Triangle::draw (sf::RenderWindow &win) {
	//_triangle.setOrigin(_triangle.getPoint(0));
	_triangle.setPosition(_location);
	win.draw(_triangle);

}

void Triangle::rotate () { 
	_triangle.rotate(ANGLE);
}

void Triangle::scale(float factor){
	_triangle.scale(factor,factor);   //scaling a triangle
	for (unsigned int i=0; i<3; i++)     //scaling all edges
		_edges[i] = _edges[i]*factor;	
	_triangleHeight *= factor;           //scaling an triangle height also

}

void Triangle::setSelection (bool selection) {
	// if we want to select the shape:
	if (selection) {
		_isSelected = selection;
		_triangle.setOutlineThickness(2);
		_triangle.setOutlineColor(sf::Color::Cyan);
	}

	// if we want to unselect the shape:
	else {
		_isSelected = false;
		_triangle.setOutlineThickness(0);
	}
}