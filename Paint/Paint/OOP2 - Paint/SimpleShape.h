#pragma once
#include "Shape.h"

class SimpleShape :	public Shape {

public:
	SimpleShape();
	virtual ~SimpleShape();
	virtual float getHight () = 0;

	virtual void draw (sf::RenderWindow &) = 0;
	virtual void setSelection (bool) = 0;
};

