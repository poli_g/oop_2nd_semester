#pragma once
#include "Canvas.h"
#include <typeinfo>
#include "SimpleShape.h"
#include "ComplexShape.h"
#include <sstream>

const float INFO_SIZE = 100;
const float INFO_DEV = 4;
const unsigned int CHAR_SIZE = 15;
const sf::Color INFO_COLOR = sf::Color (250,250,250,70);
const sf::Vector2f INFO_SHIFT = sf::Vector2f(500, 400);

class Info {
public:
	Info(const Canvas & canvas);
	~Info();

	float increaseTotalPer (float newShapePer) {_totalPer += newShapePer;};
	float increaseTotalArea (float newShapeArea) {_totalArea += newShapeArea;};
	void draw(sf::RenderWindow &, sf::Vector2f shift = INFO_SHIFT);
	void setData(const Canvas & canvas);

private:
	int _numOfSimpleShapes;		// num of simple shapes currently drawn
	int _numOfComplexShapes;	// num of complex shapes currently drawn
	float _totalPer;			// total drawn shapes perimeter
	float _totalArea;			// total drawn shapes area

	sf::Font _font;				// font in which all data will be displayed
	vector <sf::Text> _text;	// text of data to be displayed
	sf::RectangleShape _background;

	void setStrings();
	void setPositions(sf::Vector2f shift);
	
};

