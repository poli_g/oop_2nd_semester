#pragma once
#include <SFML/Graphics.hpp>
#include <cmath>
#include "Menu.h"

const float ANGLE = 90;
const float SCALE_FACTOR = (float)1.25;
const float SMALL_FACTOR = (float)0.80;

class Shape {
public:
	// C-TORS & D-TORS:
	Shape();
	virtual ~Shape();

	// FUNCTIONS:
	// draws the current shape onto the board:
	virtual void draw(sf::RenderWindow &) = 0;
	// returns the total are of the shape:
	virtual float getArea() = 0;	
	// returns total perimeter of shape:
	virtual float getPerimeter () = 0;
	// rotating the shape:
	virtual void rotate () = 0;
	//encreasing or making smaller the shape
	virtual void scale(float factor) = 0;
	// sets _location:
	void setLocation (sf::Vector2f loc) {_location = loc;};
	// returns location of shape:
	sf::Vector2f &getLocation() {return _location;};
	// sets seletion of shape:
	virtual void setSelection (bool) = 0;
	// returns _isSelected:
	bool getSelection () {return _isSelected;};

protected:
	// DATA MEMBERS:
	sf::Color _color;		// color of the shape
	sf::Vector2f _location;	// location of the shape on the board (canvas)
	bool _isSelected;		// is the shape currently selected
	
	// FUNCTIONS:
	// sets color of shape:
	virtual void setColor (color);
};

