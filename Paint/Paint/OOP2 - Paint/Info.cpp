#include "Info.h"

Info::Info(const Canvas & canvas): _numOfComplexShapes(0), _numOfSimpleShapes(0),
		_totalArea(0), _totalPer(0), _background(sf::Vector2f(200, INFO_SIZE)){
	
	// initializing font & texts
	_font.loadFromFile("BRLNSDB.ttf");
	_text.resize(4);		// we have four parameters to display

	for (unsigned int i = 0; i < _text.size(); i++) {
		_text[i].setFont(_font);	// setting font
		_text[i].setCharacterSize(CHAR_SIZE);
		_text[i].setOrigin(_text[i].getGlobalBounds().width/2, _text[i].getGlobalBounds().height/2);		// setting origin of each sentance
	}

	setStrings();

	//setting a color and position of the background menu
	_background.setFillColor(INFO_COLOR);
	_background.setOutlineThickness(2);
	
}
void Info::setData(const Canvas & canvas){
	
	// initializing all mathematical information
	const vector <Shape*> &shapesTemp = canvas.getShapes();
	// zeroing all info:
	_totalArea = 0;
	_totalPer = 0;
	_numOfComplexShapes = 0;
	_numOfSimpleShapes = 0;
	
	for (unsigned int i = 0; i < shapesTemp.size(); i++) {
		// calculating total area and perimeter of all shapes:
		_totalArea += shapesTemp[i]->getArea();
		_totalPer += shapesTemp[i]->getPerimeter();

		// counting how many of each shape type are currently drawn:
		if (typeid(*shapesTemp[i]).name() == typeid(House).name() ||
			typeid(*shapesTemp[i]).name() == typeid(StarOfDavid).name())
			_numOfComplexShapes++;
		else
			_numOfSimpleShapes++;
	}

}

Info::~Info() {
}
void Info::setStrings(){

	//inserting a string and data
	std::stringstream sstr, sstr1, sstr2, sstr3;
	sstr << "Simple Shapes:    " << _numOfSimpleShapes ;
	_text[0].setString(sstr.str());
	sstr1 << "Complex Shapes:  " << _numOfComplexShapes ;
	_text[1].setString(sstr1.str());
	sstr2 << "Total Perimeter:  " << _totalPer ;
	_text[2].setString(sstr2.str());
	sstr3 << "Total Area:        " << _totalArea ;
	_text[3].setString(sstr3.str());
}

void Info::setPositions(sf::Vector2f shift){
	
	//setting position of a background and texts
	_background.setPosition(INFO_SHIFT);
	for (unsigned int i = 0; i < _text.size(); i++) {
		_text[i].setPosition((sf::Vector2f(10, INFO_SIZE/INFO_DEV*i) + shift));
	}
}

void Info::draw(sf::RenderWindow &win, sf::Vector2f shift) {
	
	//fist of all setting all data and draw after

	setPositions(shift);
	setStrings();
	win.draw(_background);

	for (unsigned int i = 0; i < _text.size(); i++) {
		win.draw(_text[i]);
	}
	win.display();
}