#pragma once
#include "SimpleShape.h"

using std::vector;

const float REC_WIDTH = 30;		// default width of rect
const float REC_HIGHT = 10;		// default height of rect

class Rect : public SimpleShape {

public:

	// C-TORS & D-TORS:
	Rect(color color, float lenght = REC_WIDTH, float hight = REC_HIGHT);
	~Rect();

	// FUNCTIONS:
	// draws the current shape onto the board:
	virtual void draw(sf::RenderWindow &win);		
	// returns the total area of the shape:
	virtual float getArea();	
	// returns total perimeter of shape:
	virtual float getPerimeter ();	
	// rotates the shape 90 degrees clockwise
	virtual void rotate ();
	// retuens height of the shape:
	virtual float getHight (){return _edges[0];};
	// encreasing or making smaller the shape
	virtual void scale(float factor);
	// returns the sfml shape rect:
	sf::RectangleShape &getRect () {return _rect;};
	virtual void setSelection (bool);
	
protected:
	// DATA MEMBERS:
	vector <float> _edges;
	sf::RectangleShape _rect;
};

