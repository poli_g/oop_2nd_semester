#pragma once
#include "complexshape.h"
#include "Square.h"
#include "Trianlge.h"

enum direction {UP_DIR=0, RIGHT_DIR=1, DOWN_DIR=2, LEFT_DIR=3};

class House : public ComplexShape {

public:
	
	// C-TORS & D-TORS:
	House(color color);
	virtual ~House();

	// FUNCTIONS:
	virtual void rotate ();
	//returns area of a square and of a triangle 
	virtual float getArea();
	//returns perimeter of a shape by calculating it from perimeter of shapes it includes
	virtual float getPerimeter();

	virtual void draw(sf::RenderWindow &);
	//encreasing or making smaller the shape
	virtual void scale(float factor);
	virtual void setSelection (bool);

protected:
	Triangle _roof;    //contains two simple shapes: square and triangle
	Square _base;
	direction _dir;      //contanes direction of a roof
	unsigned int _count; //counts how many times shape was rotated
};

