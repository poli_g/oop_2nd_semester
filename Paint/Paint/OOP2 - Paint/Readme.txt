******************************************************************************************
            
                              R E A D M E


*******************************************************************************************


1)	OOP2 : PAINT

2)	By:	Polina Granot ID:	307696070
		Shur Svetlana ID:	318032745


3)	In this exersize we implemented a simple paint program, using grafic library SFML.


4)	Added files:


	Button.h/cpp			-	Class contain data members and functions responsible of the buttons
								on the board.
	Shape.h/cpp             -	Abstract base class of SimpleShape and ComplexShape classes. Contains 
								information that common to all shapes: color, location, ifSelected.
								Contains Pure virtual functions of actions. 
	SimpleShape.h/cpp		-	Abstract base class of clases: Circle, Rect, Triangle 
	ComplexShape.h/cpp		-   Abstract base class of clases: StarOfDavid and House
	Circle.h/cpp            -	Base class of Point. Include a radius and an implamentation of all functions. 
	Point.h/cpp				-	Derived class of Circle with radius 3 (it's visible enough) 							 
	Rect.h/cpp				-   Base class of Square. Contains vector size 2 of edges of a rectangle.
	Square.h/cpp			-	Derived class of Rect.          
	Triangle.h/cpp			-	Base class of EqualSidedTriangle and of EqualEdgedTriangle.
	EqualEdgedTriangle.h/cpp-	Derived class of Triangle.
	EqualSidedTriangle.h/cpp-	Derived class of Triangle.
	StarOfDavid.h/cpp		-	Derived class of ComplexShape. Contains two EqualSidedTriangles.
	House.h/cpp				-	Derived class of ComplexShape. Contains EqualSidedTriangle=roof and Square=base.
	Menu.h/cpp				-	Menu size it's 200X500 and it's includes all buttons and arrows for choosing
								a shape and a color. 
	Info.h/cpp				-	It's a small window drawn on the bottom of a menu contains all information about 
								shapes drawn on canvas: Number of Simple and Complex Shapes, Total Area, Total Perimeter.
	Canvas.h/cpp			-	It's actually a board we a drawing on it shapes. Size of canvas 500X500 and it
								contains shapes and functions of actions on that shapes. 
	Board.cpp				-	contains main function.
	BRLNSDB.ttf             -	font we desided to use
	*.png		            -	texture of all kinds of objects and arrows we used.
	button_up.png           -	texture we using for show pressed button
	*.wav					-	different sounds we used in the game.

	
5) Data Structures:			-	mostly we used vector from standart library. Vector of <button*> and vector of <Shape*>
								so it's allowing to use polymorthism for different actions on different shapes.

6) Algorithms:				-	no dificult algorithms where used in this exersize. All callculations of Area and 
								Perimeter calculates by known geometrycal formulas. Area and Perimeter of StarOfDavid
								where calculates from area of a triangles it'a include. A detailed explanation 
								you can find here:http://highmath.haifa.ac.il/data/Month_problems/hodesh-5.pdf
								   
7) Known Bugs:				-	Undo button doesn't work for rotation and scale operations.


8) Additional Comments:		
							