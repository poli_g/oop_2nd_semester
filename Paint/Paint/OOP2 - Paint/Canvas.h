#pragma once
#include <vector>
#include "Shape.h"
#include "Menu.h"
#include "StarOfDavid.h"
#include "House.h"
#include "Point.h"
#include "Square.h"
#include "EqualSidesTrianlge.h"
#include "EqualEdgeTrianlge.h"
#include <SFML/Audio.hpp>

using std::vector;

enum sounds {PRESS_SD, ACTION_SD, CLEAR_ALL_SD};

class Canvas {
public:
	Canvas();
	~Canvas();

	const vector <Shape*> &getShapes () const {return _shapes;};		// returns the shapes vector
	void createShape(sf::Vector2f location, color color, shape shape);
	// returns the white canvas shape:
	sf::RectangleShape getCanvas () {return _canvas;};

	// these are the actions a user can make of shapes:
	// deletes all currently drawn shapes:
	void clearCanvas();
	// selects next shape (in the order they were drawn in), if none is selected selects the first shape, if last shape is selected undoes all selections
	void selectNextShape();
	// selects previous shape (in the order they were drawn in), if none is selected selects the last shape, if first shape is selected undoes all selections
	void selectPrevShape();
	// selects all shapes of same type as currently selected shape
	void selectAllAlike();
	// delets currently sselected shapes:
	void deleteShapes();
	// rotates all selected shapes by 90 degrees clockwise
	void rotateShapes();
	// enlarges all selected shapes by 25%
	void enlargeShapes();
	// makes all selected shapes smaller by 20%
	void makeShapesSmaller();
	// moves the shape 10 pixels down if it's a circle
	void moveCircleDown();
	// sets appropriate function of button
	void setButtonFunction(void (Canvas::*&)(), sf::Vector2f, Menu&);
	// cancles last action performed on a single shape:
	void undoLastAction ();

private:
	vector <Shape*> _shapes;	// keeps all shapes currently drawn on canvas
	sf::RectangleShape _canvas;
	Shape* _preOperationShape;	// to give the option of undo last action
	std::vector<Shape*>::iterator _preOperationShapeLoc;	// saves location of last changed shape in vector
	bool _userDeleted;	// if user deleted a shape as last action
	bool _userCreated;	// if user created a shape as last action
	int _lastSelectedShapeLoc;	// saves the location in shapes vector of last selected shape
	// clears all selection (after selecting all alike) but the shape that was originally selected
	void clearSelectionButOriginal();
	// different music for the program:
	sf::Music _pressSound;
	sf::Music _actionSound;
	sf::Music _cleanSound;
	// makes desired sound:
	void makeSound(sounds);
	
};

