#include "Rect.h"

/************    C-TORS & D-TORS: *************************/

Rect::Rect(color color, float lenght, float hight) :_rect(sf::Vector2f(lenght, hight)){
	setColor(color);
	_rect.setFillColor(_color);
	_rect.setOutlineThickness(0);

	_edges.resize(2);
	_edges[0] = lenght;
	_edges[1] = hight;
	_rect.setOrigin(_edges[0]/2,_edges[1]/2);
}

Rect::~Rect() {
}

/************* FUNCTIONS ********************************/

void Rect::draw(sf::RenderWindow &win){
	_rect.setPosition(_location);
	win.draw(_rect);
	//win.display();
}

float Rect::getArea(){	
	// returns the total area of the rectangle or square
	return _edges[0]*_edges[1];
}

float Rect:: getPerimeter (){	
	// returns total perimeter of the rectangle or square
	return 2*(_edges[0]+_edges[1]);
}

void Rect:: rotate (){ //rotates a shape in angle 90
	_rect.setRotation(_rect.getRotation() + ANGLE);
}

void Rect::scale(float factor){
	_rect.scale(factor,factor);
	//encreasing an adges of a rectangle
	for (unsigned int i=0; i<2; i++)
		_edges[i] = _edges[i]*factor;
}

void Rect::setSelection (bool selection) {
	// if we want to select the shape:
	if (selection) {
		_isSelected = selection;
		_rect.setOutlineThickness(2);
		_rect.setOutlineColor(sf::Color::Cyan);
	}

	// if we want to unselect the shape:
	else {
		_isSelected = false;
		_rect.setOutlineThickness(0);
	}
}