#pragma once
//#include <cstdlib>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

using std::string;

const unsigned int SIZE_OF_FONT = 13;         //default size of font

class Button {

	public:
		
	//    C-TORS
	
	Button ();
	Button (const sf::Vector2f location, const std::string text); 

	//    FUNCTIONS
	
	void drawButton (sf::RenderWindow&);
	//returns location of the button
	sf::Vector2f getLocation() const {return _location;};
	//this function returns bounds of a button 
	sf::RectangleShape getRect() const {return _rec;};
	
	private:
		
		//  data members
		
		sf::Vector2f _location;     //keeping location of the button
		sf::Text *_nameOfButton;    //the buttons name
		sf::Font _font;             //font we use (we using only one in this exersize)
		sf::Texture _texture;       //for the texture of green button
		sf::RectangleShape _rec;    //local bounds of button

        
		void setFont();             //function that seting font to be our font
	
};