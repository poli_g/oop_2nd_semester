#pragma once
#include "complexshape.h"
#include "EqualEdgeTrianlge.h"

using std::vector;

class StarOfDavid : public ComplexShape {

public:
	
	StarOfDavid(color color);
	virtual ~StarOfDavid();
	virtual void rotate ();        //rotating the shape
	virtual float getArea();       //counting the area of star of David
	virtual float getPerimeter();  //counting perimeter of star of David
	virtual void scale(float);     //scale the shape by using scale of triangle
	virtual void draw (sf::RenderWindow &);
	virtual void setSelection (bool);

private:
	EqualEdgeTriangle _upperTriangle; //containg two triangles
	EqualEdgeTriangle _lowerTriangle;
};

