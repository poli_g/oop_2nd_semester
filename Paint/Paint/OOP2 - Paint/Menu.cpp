#include "Menu.h"

Menu::Menu(): _chosenColor(RED_CL), _chosenShape(CIRCLE_SP),
	_button(sf::Vector2f(100, 50)), _shapeLeftArrow(sf::Vector2f(30, 30)), _shapeRightArrow(sf::Vector2f(30, 30)),
	_colorLeftArrow(sf::Vector2f(30, 30)), _colorRightArrow(sf::Vector2f(30, 30)),_background(sf::Vector2f(200, 500)),
	 _colorPosition(0), _shapePosition(0) {
	
	//setting a color and position of the background menu
	_background.setFillColor(MENU_COLOR);
	_background.setOutlineThickness(2);
	//_background.setOutlineColor(sf::Color::White);
	_background.setPosition(sf::Vector2f(500,0));

	// loading textures

	_smallRightArrowTex.loadFromFile("smallRightArrow.png");
	_smallLeftArrowTex.loadFromFile("smallLeftArrow.png");
	
	// setting textures to objects:
	//_button.setTexture(&_buttonTex);
	_shapeLeftArrow.setTexture(&_smallLeftArrowTex);
	_shapeRightArrow.setTexture(&_smallRightArrowTex);
	_colorLeftArrow.setTexture(&_smallLeftArrowTex);
	_colorRightArrow.setTexture(&_smallRightArrowTex);

	// setting origins:
	
	_shapeLeftArrow.setOrigin(_shapeLeftArrow.getSize().x/2, _shapeLeftArrow.getSize().y/2);
	_shapeRightArrow.setOrigin(_shapeRightArrow.getSize().x/2, _shapeRightArrow.getSize().y/2);
	_colorLeftArrow.setOrigin(_colorLeftArrow.getSize().x/2, _colorLeftArrow.getSize().y/2);
	_colorRightArrow.setOrigin(_colorRightArrow.getSize().x/2, _colorRightArrow.getSize().y/2);

	// initializing _shapes:
	_shapes.resize(8);
	_shapesTex.loadFromFile("shapes_transparent.png");
	for (unsigned int i = 0; i < _shapes.size(); i++) {
		_shapes[i].setTexture(_shapesTex);
		_shapes[i].setTextureRect(sf::IntRect(i*spriteCoeff, 0, spriteCoeff, spriteCoeff));
		_shapes[i].setOrigin((float)spriteCoeff/2, (float)spriteCoeff/2);
	}

	// initializing _colors:
	_colors.resize(4);

	for (unsigned int i = 0; i < _colors.size(); i++) {
		_colors[i].setOutlineThickness(0);
		_colors[i].setRadius(COLOR_RADIUS);
		_colors[i].setOrigin(COLOR_RADIUS,COLOR_RADIUS);

		switch (i) {
			case 0:
				_colors[i].setFillColor(sf::Color::Red);
				break;
			case 1:
				_colors[i].setFillColor(sf::Color::Yellow);
				break;
			case 2:
				_colors[i].setFillColor(sf::Color::Magenta);
				break;
			case 3:
				_colors[i].setFillColor(sf::Color::Green);
				break;
		}
	}
		insertButtons();
}

Menu::~Menu() {
}

void Menu::insertButtons(){
	//place a buttons on the menu rectangle simetric
	_menu_buttons.push_back(new Button(sf::Vector2f(MENU_LEN/MENU_LEN_DEV+18,3*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Clean"));
	_menu_buttons.push_back(new Button(sf::Vector2f(5*MENU_LEN/MENU_LEN_DEV-18,3*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Choose same"));
	_menu_buttons.push_back(new Button(sf::Vector2f(MENU_LEN/MENU_LEN_DEV+18,4*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Bigger"));
	_menu_buttons.push_back(new Button(sf::Vector2f(5*MENU_LEN/MENU_LEN_DEV-18,4*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Smaller"));
	_menu_buttons.push_back(new Button(sf::Vector2f(MENU_LEN/MENU_LEN_DEV+18,5*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Erase"));
	_menu_buttons.push_back(new Button(sf::Vector2f(5*MENU_LEN/MENU_LEN_DEV-18,5*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Rotate"));
	_menu_buttons.push_back(new Button(sf::Vector2f(MENU_LEN/MENU_LEN_DEV+18,6*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Move circle"));
	_menu_buttons.push_back(new Button(sf::Vector2f(5*MENU_LEN/MENU_LEN_DEV-18,6*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Undo"));
	_menu_buttons.push_back(new Button(sf::Vector2f(MENU_LEN/MENU_LEN_DEV+18,7*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Prev"));
	_menu_buttons.push_back(new Button(sf::Vector2f(5*MENU_LEN/MENU_LEN_DEV-18,7*MENU_HIGHT/MENU_HIGHT_DEV)+BUTTON_SHIFT, "Next"));


}
void Menu::draw(sf::RenderWindow &win, sf::Vector2f shift) {
	setPositions(shift);
	
	win.draw(_background);
	win.draw(_shapeLeftArrow);
	win.draw(_shapeRightArrow);
	win.draw(_shapes[_shapePosition]);
	win.draw(_colorLeftArrow);
	win.draw(_colorRightArrow);
	win.draw(_colors[_colorPosition]);
	//draw all buttons
	for (unsigned int i=0; i<NUM_OF_BUTTONS; i++)
		_menu_buttons[i]->drawButton(win);
	
}

void Menu::setPositions(sf::Vector2f shift){

//devides a window menu to 6 sections
	_shapeLeftArrow.setPosition(sf::Vector2f(MENU_LEN/MENU_LEN_DEV,MENU_HIGHT/MENU_HIGHT_DEV)+shift);
	_shapeRightArrow.setPosition(sf::Vector2f(5*MENU_LEN/MENU_LEN_DEV,MENU_HIGHT/MENU_HIGHT_DEV)+shift);
	
	_colorLeftArrow.setPosition(sf::Vector2f(MENU_LEN/MENU_LEN_DEV,2*MENU_HIGHT/MENU_HIGHT_DEV)+shift);
	_colorRightArrow.setPosition(sf::Vector2f(5*MENU_LEN/MENU_LEN_DEV,2*MENU_HIGHT/MENU_HIGHT_DEV)+shift);

	for (unsigned int i = 0; i < _shapes.size(); i++) {
		_shapes[i].setPosition(sf::Vector2f(3*MENU_LEN/MENU_LEN_DEV,MENU_HIGHT/MENU_HIGHT_DEV)+shift);
	}

	for (unsigned int i = 0; i < _colors.size(); i++) {
		_colors[i].setPosition(sf::Vector2f(3*MENU_LEN/MENU_LEN_DEV,2*MENU_HIGHT/MENU_HIGHT_DEV)+shift);
	}
}

void Menu::shiftColorRight() {
	_colorPosition++;
	_colorPosition %= _colors.size();
	_chosenColor = (color)_colorPosition;
}

void Menu::shiftShapeRight() {
	_shapePosition++;
	_shapePosition %= _shapes.size();
	_chosenShape = (shape)_shapePosition;
}

void Menu::shiftColorLeft() {
	if (_colorPosition != 0)
		_colorPosition--;
	else
		_colorPosition = _colors.size()-1;
	_chosenColor = (color)_colorPosition;
}

void Menu::shiftShapeLeft() {
	if (_shapePosition != 0)
		_shapePosition--;
	else
		_shapePosition = _shapes.size()-1;
	_chosenShape = (shape)_shapePosition;
}

void Menu::setArrowFunction(void (Menu::*&func)(), sf::Vector2f point) {
	if (_shapeRightArrow.getGlobalBounds().contains(point))
		func = &Menu::shiftShapeRight;
	else if (_shapeLeftArrow.getGlobalBounds().contains(point))
		func = &Menu::shiftShapeLeft;
	else if (_colorRightArrow.getGlobalBounds().contains(point))
		func = &Menu::shiftColorRight;
	else if (_colorLeftArrow.getGlobalBounds().contains(point))
		func = &Menu::shiftColorLeft;
}
