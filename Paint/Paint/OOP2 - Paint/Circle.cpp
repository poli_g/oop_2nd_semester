#include "Circle.h"

Circle::Circle(color color, float radius): _radius(radius), _sf_circle(radius){
	setColor(color);
	_sf_circle.setFillColor(_color);
	_sf_circle.setOutlineThickness(0);
	_sf_circle.setOrigin(sf::Vector2f(_radius,_radius));
}

Circle::~Circle() {
}

void Circle::draw(sf::RenderWindow &win) {
	
	_sf_circle.setPosition(_location);
	win.draw(_sf_circle);
}

void Circle::scale(float factor){
	_sf_circle.scale(factor, factor);
	_radius = _radius * factor;
}

void Circle::setSelection (bool selection) {
	// if we want to select the shape:
	if (selection) {
		_isSelected = selection;
		_sf_circle.setOutlineThickness(2);
		_sf_circle.setOutlineColor(sf::Color::Cyan);
	}

	// if we want to unselect the shape:
	else {
		_isSelected = false;
		_sf_circle.setOutlineThickness(0);
	}
}

void Circle::moveDown() {
	_location += sf::Vector2f(0, 10);
}