#include "House.h"

House::House(color color): _roof(color), _base(color),_dir (UP_DIR), _count(0){	
}

House::~House() {
}

//this function draws a square - body of a house and triangle with shift upper (to make a roof)
void House::draw (sf::RenderWindow &win) {

	_base.setLocation(_location);
	_base.draw(win);        //draws a square first
	                        //and draws a roof depends whith direction it
	switch (_dir) {
		case UP_DIR:
			_roof.setLocation(_base.getLocation()+sf::Vector2f(0,-_base.getHight()/2-_roof.getHight()/3));
			_roof.draw(win);
			break;
		case RIGHT_DIR:
			_roof.setLocation(_base.getLocation()+sf::Vector2f(_base.getHight()/2+_roof.getHight()/3, 0));
			_roof.draw(win);
			break;
		case DOWN_DIR:
			_roof.setLocation(_base.getLocation()+sf::Vector2f(0,_base.getHight()/2+_roof.getHight()/3));
			_roof.draw(win);
			break;
		case LEFT_DIR:
			_roof.setLocation(_base.getLocation()+sf::Vector2f(-_base.getHight()/2-_roof.getHight()/3, 0));
			_roof.draw(win);
			break;
	} 
	
}

//rotates a house around sentre of a square
void House::rotate (){

	_count++; //counts number of rotations
	if ( _count % 4 == 0)
		_dir = UP_DIR;
	else if ( _count % 4 == 1)
		_dir = RIGHT_DIR;
	else if ( _count % 4 == 2)
		_dir = DOWN_DIR;
	else if ( _count % 4 == 3)
		_dir = LEFT_DIR;

	_roof.rotate();
	
	
}

void House::scale(float factor){
	_roof.scale(factor);
	_base.scale(factor);
}

float House::getArea() {return _base.getArea()+_roof.getArea();};

float House::getPerimeter() {return _base.getPerimeter()+_roof.getPerimeter();};

void House::setSelection (bool selection) {
	
	// if we want to select the shape:
	if (selection) {
		_isSelected = selection;
		_roof.setSelection(true);
		_base.setSelection(true);
	}

	// if we want to unselect the shape:
	else {
		_isSelected = false;
		_roof.setSelection(false);
		_base.setSelection(false);
	}
	
}