#include "Canvas.h"

Canvas::Canvas(): _canvas(sf::Vector2f(500, 500)), _preOperationShapeLoc(_shapes.begin()), _preOperationShape(NULL), _lastSelectedShapeLoc(-1), _userCreated(false), _userDeleted(false) {
	_canvas.setFillColor(sf::Color::White);
	
	// loading sounds:
	_pressSound.openFromFile("press.wav");
	_actionSound.openFromFile("action.wav");
	_cleanSound.openFromFile("clearAll.wav");
}

Canvas::~Canvas() {
}

void Canvas::makeSound(sounds sound) {
	switch (sound) {
		case PRESS_SD:
			_pressSound.play();
			break;
		case ACTION_SD:
			_actionSound.play();
			break;
		case CLEAR_ALL_SD:
			_cleanSound.play();
			break;
	}
}

void Canvas::createShape(sf::Vector2f location, color color, shape shape) {
	makeSound(PRESS_SD);

	// creating new shape:
	Shape *newShape;
	switch (shape) {
		case CIRCLE_SP:
			newShape = new Circle(color);
			break;
		case POINT_SP:
			newShape = new Point(color);
			break;
		case RECT_SP:
			newShape = new Rect(color);
			break;
		case SQUARE_SP:
			newShape = new Square(color);
			break;
		case EQUAL_EDGE_TR_SP:
			newShape = new EqualEdgeTriangle(color);
			break;
		case EQUAL_SIDE_TR_SP:
			newShape = new EqualSidesTriangle(color);
			break;
		case HOUSE_SP:
			newShape = new House(color);
			break;
		case STAR_SP:
			newShape = new StarOfDavid(color);
			break;
	}
	newShape->setLocation(location);

	// inserting new shape into shapes vector:
	_shapes.push_back(newShape);

	// supporting undo action:
	_preOperationShapeLoc = _shapes.begin() + _shapes.size() -1;
	_preOperationShape = newShape;
	_userDeleted = false;
	_userCreated = true;
}

void Canvas::clearCanvas() {
	makeSound(CLEAR_ALL_SD);

	// deleting all shapes:
	while (_shapes.size() != 0)
		_shapes.pop_back();

	// deleting preOperationShape:
	if (_preOperationShape != NULL) {
		delete _preOperationShape;
		_preOperationShape = NULL;
		_preOperationShapeLoc = _shapes.begin();
	}
}

void Canvas::clearSelectionButOriginal() {
	// checking if there are multiple selected shapes:
	if (_lastSelectedShapeLoc > 0) {  // this means we've selected all alike
		// unslecting all shapes but the originally selected:
		for (unsigned int i = 0; i < _shapes.size(); i++) {
			_shapes[i]->setSelection(false);
			if (i == _lastSelectedShapeLoc)
				_shapes[i]->setSelection(true);
		}
		_lastSelectedShapeLoc = -1;
	}
}

void Canvas::selectNextShape() {
	makeSound(PRESS_SD);

	bool noneSelected = true;	// states if no shape in vector is selected

	clearSelectionButOriginal();

	for (unsigned int i = 0; i < _shapes.size(); i++) {
		if (_shapes[i]->getSelection()) {
			noneSelected = false;	// a selected shape has been found
			// checking if it isn't the last shape:
			if (i != _shapes.size() - 1) 
				_shapes[i+1]->setSelection(true);	// selecting next shape

			_shapes[i]->setSelection(false);	// unselecting current shape
			break;
		}
	}

	// if there weren't any selected shapes we select the first one:
	if (noneSelected && _shapes.size() != 0)
		_shapes[0]->setSelection(true);
}

void Canvas::selectPrevShape() {
	makeSound(PRESS_SD);

	bool noneSelected = true;	// states if no shape in vector is selected

	clearSelectionButOriginal();

	for (unsigned int i = 0; i < _shapes.size(); i++) {
		if (_shapes[i]->getSelection()) {
			noneSelected = false;	// a selected shape has been found
			// checking if it isn't the first shape:
			if (i != 0) 
				_shapes[i-1]->setSelection(true);	// selecting next shape

			_shapes[i]->setSelection(false);	// unselecting current shape
			break;
		}
	}

	// if there weren't any selected shapes we select the first one:
	if (noneSelected && _shapes.size() != 0)
		_shapes[_shapes.size()-1]->setSelection(true);
}

void Canvas::selectAllAlike() {
	makeSound(PRESS_SD);

	std::string selectedShapeName;	// to save name of selected shape

	// finding selected shape:
	for (unsigned int i = 0; i < _shapes.size(); i++)
		if (_shapes[i]->getSelection()) {
			selectedShapeName = typeid(*_shapes[i]).name();
			_lastSelectedShapeLoc = i;	// saving last selected shape
		}

	// selecting all shapes like it:
	for (unsigned int i = 0; i < _shapes.size(); i++) {
		if (typeid(*_shapes[i]).name() == selectedShapeName)
			_shapes[i]->setSelection(true);
	}
}

void Canvas::deleteShapes() {
	makeSound(PRESS_SD);

	std::vector<Shape*>::iterator it = _shapes.begin();
	while (it != _shapes.end()) {
		if ((*it)-> getSelection()) {
			_userCreated = false;
			_userDeleted = true;

			// saving deleted shape & it's location for undo button:
			_preOperationShapeLoc = it;
			_preOperationShape = (*it);

			//delete (*it);	// deleting the shape object
			_shapes.erase(it);	// deleting entry in vector
			it = _shapes.begin();
		}
		else 
			it++;
	}
}

void Canvas::undoLastAction () {
	makeSound(PRESS_SD);

	std::vector<Shape*>::iterator it = _shapes.begin();

	if (_preOperationShape != NULL) {
		if (_userDeleted) {
			_preOperationShapeLoc = _shapes.insert(_preOperationShapeLoc, _preOperationShape);
			_preOperationShape = *_preOperationShapeLoc;
		
			_userDeleted = false;
			_userCreated = true;
		}
		else if (_userCreated) {
			_preOperationShape = *_preOperationShapeLoc;
			_preOperationShapeLoc = _shapes.erase(_preOperationShapeLoc);
			// checking if the last element was erased:
			if (_preOperationShapeLoc == _shapes.end())
				if (_shapes.size() == 0)
					_preOperationShapeLoc = _shapes.begin();
				else
					_preOperationShapeLoc = _shapes.begin() + _shapes.size() -1;
		
			_userCreated = false;
			_userDeleted = true;
		}
		else {
			_preOperationShapeLoc =  _shapes.erase(_preOperationShapeLoc);
			if (_shapes.size() == 0)
				_preOperationShapeLoc = _shapes.begin();
			
			_preOperationShapeLoc = _shapes.insert(_preOperationShapeLoc, _preOperationShape);
			
		}
	}
}

void Canvas::rotateShapes() {
	makeSound(ACTION_SD);

	std::vector<Shape*>::iterator it = _shapes.begin();
	for (; it != _shapes.end(); it++) {
		if ((*it)->getSelection()) {
			// to support undo button:
			_preOperationShapeLoc = it;
			_preOperationShape = (*it);
			_userCreated = false;
			_userDeleted = false;

			(*it)->rotate();
		}
	}
}

void Canvas::enlargeShapes() {
	makeSound(ACTION_SD);

	std::vector<Shape*>::iterator it = _shapes.begin();
	for (; it != _shapes.end(); it++) {
		if ((*it)->getSelection()) {
			// to support undo button:
			_preOperationShapeLoc = it;
			_preOperationShape = (*it);

			_userCreated = false;
			_userDeleted = false;

			(*it)->scale(SCALE_FACTOR);
		}
	}
}

void Canvas::makeShapesSmaller() {
	makeSound(ACTION_SD);

	std::vector<Shape*>::iterator it = _shapes.begin();
	for (; it != _shapes.end(); it++) {
		if ((*it)->getSelection()) {
			// to support undo button:
			_preOperationShapeLoc = it;
			_preOperationShape = (*it);
			_userCreated = false;
			_userDeleted = false;

			(*it)->scale(SMALL_FACTOR);
		}
	}
}

void Canvas::moveCircleDown() {
	std::vector<Shape*>::iterator it = _shapes.begin();

	for (; it != _shapes.end(); it++) {
		if ((*it)->getSelection()) {
			Circle *CirclePtr = dynamic_cast<Circle*>(*it);
			if (CirclePtr) {
			// to support undo button:
			_preOperationShapeLoc = it;
			_preOperationShape = (*it);
			_userCreated = false;
			_userDeleted = false;

			CirclePtr->moveDown();
			}
		}
	}
}

void Canvas::setButtonFunction(void (Canvas::*&func)(), sf::Vector2f point, Menu &menu) {
	for (unsigned int i = 0; i < menu.getButtonNum(); i++) {
		if (menu.getButtonRect(i).getGlobalBounds().contains(point)) {
			
			switch (i) {
			case 0:
				func = &Canvas::clearCanvas;
				break;
			case 1:
				func = &Canvas::selectAllAlike;
				break;
			case 2:
				func = &Canvas::enlargeShapes;
				break;
			case 3:
				func = &Canvas::makeShapesSmaller;
				break;
			case 4:
				func = &Canvas::deleteShapes;
				break;
			case 5:
				func = &Canvas::rotateShapes;
				break;
			case 6:
				func = &Canvas::moveCircleDown;
				break;
			case 7:
				func = &Canvas::undoLastAction;
				break;
			case 8:
				func = &Canvas::selectPrevShape;
				break;
			case 9:
				func = &Canvas::selectNextShape;
				break;
			}
		}
	}
}