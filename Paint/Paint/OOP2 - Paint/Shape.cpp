#pragma once
#include "Shape.h"

Shape::Shape(): _color(sf::Color::Red), _isSelected(false), _location(sf::Vector2f(0,0)) {
}

Shape::~Shape() {
}

void Shape::setColor (color newColor) {
	switch (newColor) {
		case RED_CL:
			_color = sf::Color::Red;
			break;
		case YELLOW_CL:
			_color = sf::Color::Yellow;
			break;
		case PURPLE_CL:
			_color = sf::Color::Magenta;
			break;
		case GREEN_CL:
			_color = sf::Color::Green;
			break;
	}
}