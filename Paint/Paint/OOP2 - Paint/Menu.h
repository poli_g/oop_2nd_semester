#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "Button.h"

enum color {RED_CL, YELLOW_CL, PURPLE_CL, GREEN_CL};
enum shape {CIRCLE_SP, EQUAL_EDGE_TR_SP, HOUSE_SP, POINT_SP, RECT_SP, SQUARE_SP, STAR_SP, EQUAL_SIDE_TR_SP};

const int spriteCoeff = 30;

const float MENU_LEN = 200;
const float MENU_HIGHT = 500;
const float MENU_HIGHT_DEV = 12;
const float MENU_LEN_DEV = 6;
const float COLOR_RADIUS = 15;
const unsigned int NUM_OF_BUTTONS = 10;
const sf::Vector2f BUTTON_SHIFT = sf::Vector2f(500,30);
const sf::Color MENU_COLOR = sf::Color(sf::Color(0,44,60,250));

using std::vector;

class Menu {
public:
	Menu();
	~Menu();

	// draws the menu to win, "shift" is where the reference point from which to start drawing
	void draw(sf::RenderWindow &, sf::Vector2f shift = sf::Vector2f(500, 0));
	
	color getChosenColor () {return _chosenColor;};
	shape getChosenShape () {return _chosenShape;};
	void setChosenColor (sf::Color color);
	void setChosenColor (shape shape);
	// returns rect of button:
	const sf::RectangleShape &getButtonRect(unsigned int i) const {return _menu_buttons[i]->getRect();}
	// returns size of buttons vector:
	unsigned int getButtonNum () {return _menu_buttons.size();};

	// moves selection color/shape right/left:
	void shiftColorRight();
	void shiftShapeRight();
	void shiftColorLeft();
	void shiftShapeLeft();

	// sets arrows functionality
	void setArrowFunction(void (Menu::*&)(), sf::Vector2f);

private:

	color _chosenColor;	// so save selected shape color
	shape _chosenShape;	// so save chosen shape to draw

	sf::Texture _smallRightArrowTex;
	sf::Texture _smallLeftArrowTex;
	sf::Texture _buttonTex;

	sf::Texture _shapesTex;
	vector <sf::Sprite> _shapes;
	vector <sf::CircleShape> _colors;
	vector <Button*> _menu_buttons;
	
	sf::RectangleShape _button;
	sf::RectangleShape _shapeRightArrow;
	sf::RectangleShape _shapeLeftArrow;
	sf::RectangleShape _colorRightArrow;
	sf::RectangleShape _colorLeftArrow;
	

	sf::RectangleShape _background;
	void setPositions(sf::Vector2f shift);
	void insertButtons();

	int _colorPosition;
	int _shapePosition;
};

