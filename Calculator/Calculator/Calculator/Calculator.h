#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cctype>
#include "Exercise.h"
#include <sstream>

using std::vector;
using std::string;
using std::stringstream;
using std::cout;
using std::cin;
using std::endl;

const char SPACE = ' ';
const int ASCII_DELIM = 48;

class Calculator{

public:
	
	Calculator() {};
	~Calculator() {};

	void enterExNumbers();			// gets numbers of exercises to solve from user
	bool readFromFile();			// reads exercises from file given by user
	void calculateChoosen();		// calculates the selected exercises and prints them
	void printHelloMsg() {cout << "Hello!!! Welcome to Callculator!" << endl;} 
	
private:

	vector <Exercise> _exercises;	// vector containing all exercises
	std::ifstream _inData;				// file to read exercises from
	vector <int> _exNumbers;			// numbers of exercises to solve

	bool getFileName ();			// gets file name to read exercises from user
	void calculate(Exercise &);// calculates a specific exercise
	void initExNums(string);	// inits the numbers of ex's to solve vector
	double stringToDouble (const string &);	// casts string into double
	void printResult(double num1, double num2, char operand);	// prints the answer of the exercise
};

