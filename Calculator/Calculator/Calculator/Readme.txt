******************************************************************************************
            
                              R E A D M E


*******************************************************************************************


1)	OOP2 : CLCULATOR 

2)	By:	Polina Granot ID:	307696070
		Shur Svetlana ID:	318032745


3)	In this exersize we implemented a calculator in console application using exceptions


4)	Added files:

	Exercise.h/cpp			-	This class contains data memebers that represents an exercise with two operands (like strings)
								and one operator (like char).
	
	Calculator.h/cpp		-	This class contains vector of exercises and it's actually controlls all game. It's responsible of:
								reading from file, reading number of exercises from user, converting from strings to double and more..
	
	Exception.h				-	This class is a base class of all exceptions.

	FileException.h			-	This class is a derived class of Exception, trow exeption if can't open a file.

	MathException.h			-	This class is a derived class of Exception, trow exeption if trying devide by zero.
	
	ExerciseException.h		-	This class is a derived class of Exception, trow exeption if exercise is not exist.

	NotADigitException.h	-	This class is a derived class of Exception, trow exeption if not number operand recognized.

	OperatorException.h		-	This class is a derived class of Exception, trow exeption if operator is not valid.

	main.cpp				-	contains main function.

	math.txt				-   file of exersises we read from. 


5) Data Structures:			-	mostly we used vector from standart library. Vector of <Exercises> ,vector <int>
								for the numbers of exercises we want to solve.

6) Algorithms:				-	no dificult algorithms was used in this exercise

7) Known Bugs:				-	no bugs were detected

8) Additional Comments:		-	we got one day late permition from Michal
							