#pragma once
#include "exception.h"

class ExerciseException : public Exception{
	
public:

	ExerciseException(){}
	virtual ~ExerciseException(){}
	
	virtual void show() {	
		Exception::show();
		cout << "Exercise doesn't exist" << endl;
	}
};

