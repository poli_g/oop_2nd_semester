#pragma once
#include "InputException.h"

class NotADigitException : public InputException{
	
public:

	NotADigitException(){}
	virtual ~NotADigitException(){}
	
	virtual void show() {
		Exception::show();
		cout << "Not a number operand" << endl; }
	
};

