#pragma once
#include "exception.h"

class FileException : public Exception {

public:

	FileException(){}
	virtual ~FileException(){}

	virtual void show() {	
		Exception::show();
		cout << "File doesn't exist" << endl; }
};

