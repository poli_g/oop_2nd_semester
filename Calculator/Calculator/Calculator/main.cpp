#pragma once
#include <iostream>
#include "Exception.h"
#include "Calculator.h"
#include "Exercise.h"

int main(){

	Calculator calculator;
	calculator.printHelloMsg();
	
	calculator.readFromFile();
	calculator.enterExNumbers();
	calculator.calculateChoosen();

	return 0;
}