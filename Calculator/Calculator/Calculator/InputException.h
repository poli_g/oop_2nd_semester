#pragma once
#include "exception.h"
class InputException : public Exception
{
public:
	InputException();
	virtual ~InputException();
};

