#pragma once
#include "InputException.h"

class OperatorException : public InputException{

public:

	OperatorException(){}
	virtual ~OperatorException(){}
	
	virtual void show() {	
		Exception::show();
		cout << "Unrecognized operator" << endl;
	}
};

