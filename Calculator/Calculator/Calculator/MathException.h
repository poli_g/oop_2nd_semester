#pragma once
#include "exception.h"

class MathException:public Exception {

public:
	
	MathException(){};
	virtual ~MathException(){};
	
	virtual void show() {
		Exception::show();
		cout << "Division by zero" << endl;
	}
};

