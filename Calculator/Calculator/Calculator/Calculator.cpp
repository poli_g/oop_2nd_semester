#include "Calculator.h"


bool Calculator::readFromFile() {
	while (!getFileName());	// getting valid file name from user

	//reading from file, creating the exercises as we go:
	while (_inData.good()) {
		string num1, num2;
		char operand;

		// reading first number:
		_inData >> num1;
		// discarding whitespaces:
		while (_inData.peek() == ' ')
			_inData.get();
		// reading operand:
		_inData >> operand;
		// discarding whitespaces:
		while (_inData.peek() == ' ')
			_inData.get();
		// reading second number:
		_inData >> num2;
		// discarding whitespaces:
		while (_inData.peek() == ' ' || _inData.peek() == '\n')
			_inData.get();

		// creating the exercise and inserting it into vector:
		Exercise newEx(num1, num2, operand);
		_exercises.push_back(newEx);
	}

	_inData.close();
	return true;
}

bool Calculator::getFileName () {
	std::string filename;     //filename of user  
	
	cout << "Please Enter a filename to read exercises from: \n";
	cin >> filename;

	// getting rid of \n char
	cin.get();
	
	try {
		_inData.open(filename);
		if(!_inData)
			throw FileException();
	}
	catch(FileException & fex){
		fex.show();
		return false;
	}
	return true;
}

void Calculator::enterExNumbers(){
	string exNums;
	
	cout << "Please enter the numbers of the exercises you wish to solve\n";
	std::getline(cin, exNums);

	initExNums(exNums);
}

void Calculator::initExNums(string exNums) {
	stringstream convert(exNums);

	if (exNums.find("-") != std::string::npos) {	// the user inputted range
		int index, last;
		convert >> index;

		// getting rid of characters in the middle
		while (convert.peek() == ' ' || convert.peek() == '-')
			convert.get();

		convert >> last;
		for (; index <= last; index++)
			_exNumbers.push_back(index);
	}
	else
		while (convert.good()) {
			int num;
			convert >> num;
			_exNumbers.push_back(num);
		}
}

void Calculator::calculateChoosen() {
	for (unsigned int i = 0; i < _exNumbers.size(); i++) {
		try {
			cout << _exNumbers[i] << ") ";
			if (_exNumbers[i] <= _exercises.size() && _exNumbers[i] > 0)
				calculate(_exercises[_exNumbers[i]-1]);
			else {
				throw ExerciseException();
			}
		}
		catch (ExerciseException &exExcep) {
			exExcep.show();
		}
	}
}

void Calculator::calculate(Exercise &ex) { 
	cout << ex;	// printing the exercise as was read from file
	
	char temp1 = ex.getFirst()[0];
	char temp2 = ex.getSecond()[0];
	try {
		if (!isdigit(temp1) || !isdigit(temp2)) {
			ex.setSolvable(false);
			throw new NotADigitException();
		}
		else if (ex.getOperand() != '+' && ex.getOperand() != '-' && ex.getOperand() != '*' && ex.getOperand() != '/') {
			ex.setSolvable(false);
			throw new OperatorException();
		}
	}
	catch (InputException* inEx) {
		inEx->show();
	}

	if (ex.GetSolvable()) {
		double num1 = stringToDouble(ex.getFirst()), 
			num2 = stringToDouble(ex.getSecond());
		
		printResult(num1, num2, ex.getOperand());
	}
}

void Calculator::printResult(double num1, double num2, char operand) {
	switch (operand) {
		case '+':
			cout << "= " << num1+num2 << endl;
			break;
		case '-':
			cout << "= " << num1-num2 << endl;
			break;
		case '/':
			try {
				if (num2 == 0)
					throw MathException();
				else
					cout << "= " << num1/num2 << endl;
			}
			catch (MathException &math) {
				math.show();
			}
			break;
		case '*':
			cout << "= " << num1*num2 << endl;
			break;
	}	
}

double Calculator::stringToDouble (const string &str) {
	double num;
	stringstream temp(str);
	temp >> num;
	return num;
}