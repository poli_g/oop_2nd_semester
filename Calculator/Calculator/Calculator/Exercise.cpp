#include "Exercise.h"

std::ostream  &operator << (std::ostream &stream, const Exercise &ex) {
	stream << ex._firstNum << ' ' << ex._operand << ' ' << ex._secondNum << ' ';
	return stream;
}