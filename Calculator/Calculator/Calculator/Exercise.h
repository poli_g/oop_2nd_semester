#pragma once
#include <math.h>
#include <string>
#include "MathException.h"
#include "FileException.h"
#include "NotADigitException.h"
#include "OperatorException.h"
#include "ExerciseException.h"

class Exception;

class Exercise {
public:
	
	Exercise (string num1, string num2, char operand): _solvable(true), _firstNum(num1), _secondNum(num2), _operand(operand) {};
	~Exercise (){};

	friend std::ostream  &operator << (std::ostream&, const Exercise &);
	// get/set functions:
	string getFirst () {return _firstNum;};
	string getSecond () {return _secondNum;};
	char getOperand () {return _operand;};
	bool GetSolvable () {return _solvable;};
	void setSolvable (bool val) {_solvable = val;};

private:
	string _firstNum;		// first number (can be char if a non-digit is read from file)
	string _secondNum;		// second number (can be char if a non-digit is read from file)
	char _operand;			// operand (+ / - *)
	bool _solvable;			// if the ex is valid, it is solvable
};