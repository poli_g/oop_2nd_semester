#pragma once
#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::cin;
using std::endl;

class Exception{

public:
	
	Exception(){};
	virtual ~Exception(){};

	//virtual function that prints ERROR before each message of exception 
	virtual void show() { std::cout << "ERROR: ";}
};

