#include "Controller.h"
#include <sstream>	// REMOVE

Controller::Controller(unsigned int lvlNum): _gameGraph(setLvlFile(lvlNum)), _level(lvlNum), _center(sf::Vector2f(WINDOW_WIDTH/2,WINDOW_HIGHT/2)){
	_corners.resize(POSSIBLE_NUM_OF_NEIGHBORS);
	_pressSound.openFromFile("press.wav");
	_connectedSound.openFromFile ("connected.wav");
	_winSound.openFromFile("win.wav");
	buildLightsGraph();

}

Controller::~Controller() {
}

void Controller::buildLightsGraph () {
	setVerticesLocations();
	updateVerticesAccordingToEdges();
}

void Controller::setVerticesLocations () {

	//setting the coodinates of a centeral vertex
	_gameGraph.setVeretexLocation (0, _center);
	
	unsigned int index = 1; //already set center

	setCorners(0);
	for (unsigned int j = 0; index < _gameGraph.graphSize() && j < POSSIBLE_NUM_OF_NEIGHBORS; j++, index++)
		_gameGraph.setVeretexLocation(index, _corners[j]);

	for (unsigned int lev = 1; lev <= _level; lev++)
		buildLevel(lev,index);				
}

void Controller::setCorners(unsigned int coeff){
	
	for (unsigned i = 0; i < POSSIBLE_NUM_OF_NEIGHBORS; i++){
		//calculating different edges
		float _cos = cos((float)(i*FULL_CIRCLE/POSSIBLE_NUM_OF_NEIGHBORS)*PI/180);
		float _sin = sin((float)(i*FULL_CIRCLE/POSSIBLE_NUM_OF_NEIGHBORS)*PI/180);
		//calculating a coordinate of vertex by multiplaing by cos & sin
		_corners[i] = _center+sf::Vector2f ((EDGE_LEN*(coeff+1))*_cos, (EDGE_LEN*(coeff+1))*_sin);			
	}
}

void Controller::buildLevel(unsigned int lev, unsigned int &index){
	
	//setting vector of corners for the current level
	setCorners(lev);

	for (unsigned int j = 0; index < _gameGraph.graphSize() && j < POSSIBLE_NUM_OF_NEIGHBORS; j++){
		_gameGraph.setVeretexLocation(index++, _corners[j]);
				
		for (unsigned int p = 1; (p <= lev) && (index < _gameGraph.graphSize()) ; p++){ 
				
			//calculating of lambda for current point
			float lambda = ((float)p/((float)lev+1-p));
					
			_gameGraph.setVeretexLocation(index++ , sf::Vector2f(
			(_corners[j].x+ lambda*_corners[(j+1)%POSSIBLE_NUM_OF_NEIGHBORS].x)/(lambda+1),
			(lambda*_corners[(j+1)%POSSIBLE_NUM_OF_NEIGHBORS].y+_corners[j].y)/(lambda+1)
			));

		} //end of for p (number of point from corner)						
	}  //end of for j (index run over _corner vector)
}

int Controller::countNumOfVericesCurrLevel(){
	int sum = 1;
	for( unsigned int i=0; i<=_level; i++)
		sum+=i*POSSIBLE_NUM_OF_NEIGHBORS;
	return sum;
}

void Controller::updateVerticesAccordingToEdges() {
	// finding surrounding vertices in drawing
	findSurroundingVertices();
	// adding 1/0 to appropriate angle of each vertexs' vertexData:
	initHalfEdges();
}

void Controller::findSurroundingVertices () {
	int circle = 1;	// the drawing is divided into circles of vertices, this is the circle we are currently referring to
	// each vertex (except the first) has three neighbors from the upper circle, this is used to calculate their serial nums
	int additive1 = 0, 
		additive2 = additive1+1, 
		additive3 = additive2+1;
	int verticesInCircle = 1;	// we start with one, after that it's drawingCoeff*circle
	int allPrevVertices = 0;	// num of vertices in all prev circles
	//VertexData &currVertexData = _gameGraph.getVertexData(0);	// data of current vertex who's surrounding vertices we're searching for
	int maxCircles = _level + 2;	// maximum circles for level

	// the first vertex is special, it has 6 surrounding vertices from circle above it, so we do it separatly:
	//currVertexData = _gameGraph.getVertexData(0);
	for (int i = 1; i <= circle * drawingCoeff; i++) {
		_gameGraph.getVertexData(0).addNeighbor(i);
		_gameGraph.getVertexData(i).addNeighbor(0);
	}
	allPrevVertices++;
	
	// updating all other vertices:
	while (allPrevVertices != _gameGraph.graphSize()) {
		verticesInCircle = circle * drawingCoeff;
		for (int j = allPrevVertices; j < verticesInCircle + allPrevVertices; j++) {
			// in case this is the first vertex of the circle:
			if (additive1 == 0)
				additive1 = ((circle+1) * drawingCoeff);

			// adding vertices next to him in same circle:
			_gameGraph.getVertexData(j).addNeighbor((j-1 == (circle-1) * drawingCoeff)? (verticesInCircle + allPrevVertices -1) : (j-1));
			_gameGraph.getVertexData(j).addNeighbor((j+1 == verticesInCircle + allPrevVertices)? (allPrevVertices) : (j+1));

			if (circle+1 < maxCircles) {
				// adding the vertices from next circle:
				_gameGraph.getVertexData(j).addNeighbor(allPrevVertices + verticesInCircle + additive1 - 1);
				_gameGraph.getVertexData(allPrevVertices + verticesInCircle + additive1 - 1).addNeighbor(j);
				_gameGraph.getVertexData(j).addNeighbor(allPrevVertices + verticesInCircle + additive2 - 1);
				_gameGraph.getVertexData(allPrevVertices + verticesInCircle + additive2 - 1).addNeighbor(j);

				// only if vertex is located on the corner of the hexagon
				if (j % circle == 1 || circle == 1) {
					_gameGraph.getVertexData(j).addNeighbor(allPrevVertices + verticesInCircle + additive3 - 1);
					_gameGraph.getVertexData(allPrevVertices + verticesInCircle + additive3 - 1).addNeighbor(j);

					//if (circle == 1)
						additive1 = additive3;
					//else
					//	additive1 = additive3;
				}
				else
					additive1 = additive2;

			}
			additive2 = additive1+1;
			additive3 = additive2+1;
		}
		allPrevVertices += verticesInCircle;
		circle++;
		additive1 = 0;
		additive2 = additive1+1;
		additive3 = additive2+1;
	}
	_gameGraph;
}

void Controller::makeSound(sounds sound) {

	switch (sound) {
		case PRESS_SD:
			_pressSound.play();
			break;
		case CONNECT_SD:
			_connectedSound.play();
			break;
		case WIN_SD:
			_winSound.play();
			break;
	}
}

void Controller::initHalfEdges () {
	for (unsigned int i = 0; i < _gameGraph.graphSize(); i++)
		checkAngleForNeighbors (i, _gameGraph.getNeighbors(i));

}
void Controller::checkAngleForNeighbors (unsigned int vertex, vector<int> neighbors){
	//getting location of a vertex
	sf::Vector2f point1 = _gameGraph.getVertexLocation(vertex);

	//checking for each neighbor of the vertex what is the angle of an adge
	for (unsigned int i = 0; i < neighbors.size(); i++){
		sf::Vector2f point2 = _gameGraph.getVertexLocation(neighbors[i]);
		float angle = this->angle(point1, point2);
		_gameGraph.setAngle (vertex, angle);
	}
}

float Controller::angle(sf::Vector2f point1, sf::Vector2f point2){
	//counting an angle between two points acording to X hinge
	float x1 = point1.x, y1 = point1.y;
	float x2 = point2.x, y2 = point2.y;
	float Ang = atan2(y2 - y1, x2 - x1) * (180 / PI);
	//have to be positive
	Ang = (Ang < 0) ? Ang + 360 : Ang;
	return Ang;
}

void Controller::drawGraph(sf::RenderWindow &win) {
	// drawing half edges
	for (unsigned int i = 0; i < _gameGraph.graphSize(); i++) {
		for (float angle = 0; angle < 360; angle += 60) {
			if (_gameGraph.getVertexData(i).checkHalfEdge(angle)) {
				// creating half edge:
				sf::Vertex halfEdge[2] =
					{
						sf::Vertex(sf::Vector2f(_gameGraph.getShape(i).getPosition()), sf::Color(82, 90, 200)),
						sf::Vertex(sf::Vector2f((cos(angle*PI/(FULL_CIRCLE/2)) * (EDGE_LEN/2)) + _gameGraph.getShape(i).getPosition().x, 
						(sin(angle*PI/(FULL_CIRCLE/2)) * (EDGE_LEN/2)) + _gameGraph.getShape(i).getPosition().y), sf::Color::White)
					};
				// drawing it
				win.draw(halfEdge, 2, sf::Lines);
			}
		}
	}

	// drawing vertices
	for(unsigned int i = 0; i < _gameGraph.graphSize(); i++){
		(_gameGraph.getConnected(i))? _gameGraph.getShape(i).setFillColor(sf::Color::Yellow) : _gameGraph.getShape(i).setFillColor(GRAY);
		win.draw(_gameGraph.getShape(i));

		/*
		// FOR DEBUGGIN REMOVE LATER!!!
		/*
		string Result;          // string which will contain the result

		std::ostringstream convert;   // stream used for the conversion

		convert << _gameGraph.getVertexNum(i);      // insert the textual representation of 'Number' in the characters in the stream

		Result = convert.str(); // set 'Result' to the contents of the stream


		sf::Text num(Result);
		num.setCharacterSize(20);
		num.setOrigin(num.getLocalBounds().width/2, num.getLocalBounds().height/2);
		num.setPosition(_gameGraph.getVertexLocation(i));
		num.setColor(sf::Color::Red);
		win.draw(num);
		*/
	}
}

string Controller::setLvlFile (unsigned int lvl) {
	switch (lvl) {
		case 1:
			return "lvl1.txt";
			break;
		case 2:
			return "lvl2.txt";
			break;
		case 3:
			return "lvl3.txt";
			break;
		default:
			return "lvl1.txt";
			break;
	}
}

bool Controller::runGame () {
	sf::RenderWindow win(sf::VideoMode(WINDOW_WIDTH, WINDOW_HIGHT), "Lights!");
	scrambleHalfEdges ();	// rotating vertices in random directions

	_gameGraph;

	while (win.isOpen()){
		win.clear();
		drawGraph(win);
		win.display();
		
		sf::Event event;
		while (win.pollEvent(event)) {
			if (!handleEvents(event, win))			
				return true;
		}

		// checking if player passed the level:
		if (checkLevel()) {
			makeSound(WIN_SD);
			win.close();
			return false;
	}
}

	return false;
}

bool Controller::handleEvents(const sf::Event& event, sf::RenderWindow &win) {
	// closing window & exiting game
	if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && 
		event.key.code == sf::Keyboard::Escape)) {
		return false;
	}
	// handling vertices' rotation:
	if (event.type == sf::Event::MouseButtonPressed) {
		sf::Vector2f mouseClick(event.mouseButton.x, event.mouseButton.y);	// saving mouse click
		// checking if a vertex was clicked:
		for (unsigned int i = 0; i < _gameGraph.graphSize(); i++) {
			if (_gameGraph.getShape(i).getGlobalBounds().contains(mouseClick)){
				rotateVertex(i, event);
				// update graph edges
				checkConnectedNeighbors(i);
			}
		}
	}

	return true;
}

void Controller::checkConnectedNeighbors(unsigned int index) {
	for (unsigned int index2 = 0; index2 < _gameGraph.getVertexData(index).sizeOfPosibleNeighbors(); index2++){
		if (connected(index,_gameGraph.getVertexData(index).getNeighbor(index2)))
			_gameGraph.addNeighborsToList(index, _gameGraph.getVertexData(index).getNeighbor(index2));
		else if(!connected(index, _gameGraph.getVertexData(index).getNeighbor(index2))){
			_gameGraph.removeNeighborsIfExist(index, _gameGraph.getVertexData(index).getNeighbor(index2));
		}
	}
}

bool Controller::connected(unsigned int index1, unsigned int index2){
	float angle = this->angle (_gameGraph.getVertexLocation(index1), _gameGraph.getVertexLocation(index2)); 
	float opposite_angle = (angle+(FULL_CIRCLE/2) < 360)? angle+(FULL_CIRCLE/2) : angle-(FULL_CIRCLE/2);
	//float opposite_angle = angle+FULL_CIRCLE/2 % FULL_CIRCLE;
	//checks if there an angle in both dirrections
	if(_gameGraph.getVertexData(index1).checkHalfEdge(angle) && 
		_gameGraph.getVertexData(index2).checkHalfEdge(opposite_angle))
		return true;
	else
		return false;
}

void Controller::reloadGame (unsigned int lvlNum) {
	// deleting old graph & creating a new one:
	bool levelChanged = (lvlNum != _level)? true : false;
	_gameGraph.deleteGraph(levelChanged);
	_gameGraph.readGraphFromFile(setLvlFile(lvlNum));

	_level = lvlNum;
	setVerticesLocations();
	updateVerticesAccordingToEdges();
}

void Controller::scrambleHalfEdges () {
	sf::Clock clock;	// for random seed
	int numOfRotations;	// how many times to rotate each vertex (0 - 6)
	bool dirOfRotation;	// in which direction to rotate the curr vertex

	// going over all vertices and rotating them randomly:
	for (unsigned int i = 0; i < _gameGraph.graphSize(); i++) {
		srand (clock.getElapsedTime().asMicroseconds());
		numOfRotations = rand () % POSSIBLE_NUM_OF_NEIGHBORS;
		dirOfRotation = (numOfRotations % 2 == 0)? true : false;

		for (int j = 1; j <= numOfRotations; j++)
			_gameGraph.getVertexData(i).rotate(dirOfRotation);

		checkConnectedNeighbors(i);
	}
}

bool Controller::checkLevel() {
	unsigned int counter = 0;
	
	//tuning off all lights
	for (unsigned int i = 0; i < _gameGraph.graphSize(); i++)
		_gameGraph.setConnected(i, false);
	
	BFS();

	//counting how many lights is turned on
	for (unsigned int j = 0 ; j < _gameGraph.graphSize(); j++)
		if(_gameGraph.getConnected(j))
			counter++;
	
	
	if(counter == _gameGraph.graphSize())
		return true;
	else
		return false;
	
}
void Controller::BFS(){
	// using a vector for implementation qeueu 
	vector <int> _qeueu;
	// inserting first vertex into a qeueu 
	_qeueu.push_back(0);
	_gameGraph.setConnected(0, true);

	while(!_qeueu.empty()){
		//taking last element in the qeueu
		int index = _qeueu.back();
		_qeueu.pop_back();
		_gameGraph.setConnected(index, true);
		
		//inserting all neighbors witch not turned on
		for (vector<int>::iterator it = _gameGraph.getNeighbors(index).begin(); it != _gameGraph.getNeighbors(index).end(); it++)
			if(!_gameGraph.getConnected(*it))
				_qeueu.push_back(*it);
	}

}

void Controller::rotateVertex (unsigned int i, const sf::Event& event) {
	
	if (event.mouseButton.button == sf::Mouse::Right)  // rotating anti-clockwise
	{
		_gameGraph.getVertexData(i).rotate(false);
	}
	else if (event.mouseButton.button == sf::Mouse::Left)  // rotating clockwise
	{
		_gameGraph.getVertexData(i).rotate(true);
	}
	makeSound(PRESS_SD);
}