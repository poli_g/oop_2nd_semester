#include "macros.h"

class Controller {
public:
	Controller(unsigned int);
	~Controller();
 
	void drawGraph(sf::RenderWindow &);		// draws graph to window
	bool runGame ();						// runs Lights game, returns true if window closed, false if game ended by winning
	void reloadGame (unsigned int);			// reloads data for new game, gets lvl num
private:
	Graph<VertexData> _gameGraph;	// graph drawn in lights game
	unsigned int _level;
	sf::Vector2f _center;
	vector<sf::Vector2f> _corners;
	// different music for the program:
	sf::Music _pressSound;
	sf::Music _connectedSound;
	sf::Music _winSound;
	// makes sound:
	void makeSound(sounds);

	void setVerticesLocations ();			// initializes the location of the sfml shapes representing vertices in graph
	void setCorners (unsigned int coeff);
	void buildLevel(unsigned int lev, unsigned int &index);
	int countNumOfVericesCurrLevel();		//counting number of vertices in current level
	void findSurroundingVertices ();		// finds the vertices that are next to current vertice in drawing for all vertices in graph
	void updateVerticesAccordingToEdges();	// updates at which angles to draw the half edges according to original graph edges
	void buildLightsGraph ();				// builds the graph with all it's drawing data
	void initHalfEdges ();					// adding 1/0 to appropriate angle of each vertexs' vertexData
	void checkAngleForNeighbors (unsigned int i, vector<int> neighbors);
	float angle(sf::Vector2f point1, sf::Vector2f point2); //calculating an angle between two points
	string setLvlFile (unsigned int);		// sets string of lvl file according to lvl number
	bool handleEvents(const sf::Event&, sf::RenderWindow &win);	// handles events in runGame function, returns false if win was closed
	void scrambleHalfEdges();				// rotates the vertices & the half edges randomly
	bool checkLevel();						// checks if all the graph vertices are connected by BFS, returns true if the are, false otherwise
	void rotateVertex (unsigned int i, const sf::Event& event);	// rotates vertex in the appropriate direction
	void checkConnectedNeighbors(unsigned int index); //checking if vertices still connected after rotating 
	bool connected(unsigned int index1, unsigned int index2); //checking if two vertices is really connected
	void BFS();
};

