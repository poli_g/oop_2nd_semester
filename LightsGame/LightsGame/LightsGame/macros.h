#pragma once
#include <SFML/Graphics.hpp>
#include "Graph.h"
#include "VertexData.h"
#include <math.h>
#include <SFML/Audio.hpp>

const unsigned int WINDOW_HIGHT = 600;	// diameters of window
const unsigned int WINDOW_WIDTH = 700;	// diameters of window
const unsigned int EDGE_LEN = 50;		// edge len of each mini triangle compiling the sub-hexagons
const float HIGHT = (float)43.3;		// hight of each mini triangle compiling the sub-hexagons
const float PI = 3.14159265;			// the value PI
const int drawingCoeff = 6;				// to calculate how many vertices are in each circle of drawing (6*circle)
const sf::Color GRAY(150, 150, 150);	// color of unconnected vertices

enum sounds {PRESS_SD, CONNECT_SD, WIN_SD};
