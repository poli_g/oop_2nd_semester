#pragma once
#include <vector>

using std::vector;
using std::pair;

const int POSSIBLE_NUM_OF_NEIGHBORS = 6;
const int FULL_CIRCLE = 360;

class VertexData {
public:
	VertexData();
	~VertexData() {};

	void addNeighbor(int i);	// add a surrounding neighbor to _neighborsInDrawing
	bool checkHalfEdge(float angle);
	void setHalfEdge(float angle, bool set);
	void rotate(bool clockwise);
	unsigned int sizeOfPosibleNeighbors() {return _neighborsInDrawing.size();}; //number of posibble neighbors
	int getNeighbor(unsigned int index) { return _neighborsInDrawing[index];};  //return next neighbor

private:
	vector<pair<int, bool>> _possibleHalfEdges;	// there are 6 half edges a vertex can have in a game, we represent them by the angle of the half-edge, where 0 means there's no edge at that angle of the vertex and 1 means there is.
	vector <int> _neighborsInDrawing;	// the serial numbers of the vertices that are this vertex's neighbors in drawing
};

