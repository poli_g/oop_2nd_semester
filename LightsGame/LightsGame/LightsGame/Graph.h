#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "VertexData.h"
#include <fstream>
#include <string>
#include <queue>

using std::streampos;
using std::pair;
using std::vector;
using std::ifstream;
using std::string;

const float VERTEX_RADUIS = 6;

template <class T>

class Graph {
	
public:
	Graph(const string&);	// creates an empty graph of X vertices
	~Graph() {};

	class IteratorBFS;
	
	bool checkEdge(unsigned int, unsigned int);
	unsigned int graphSize() {return _vertices.size();};
	void setAngle (unsigned int vertex, float angle);
	vector<int>& getNeighbors(unsigned int i) {return _neighbors[i];};
	void setVeretexLocation(int i, sf::Vector2f coordinates) {_vertices[i].setVertexLocation(coordinates);}
	sf::Vector2f getVertexLocation (unsigned int i) {return _vertices[i].getVertexLocation();}
	sf::CircleShape &getShape(int i) {return _vertices[i]._sfShape;}
	T &getVertexData(int i) {return _vertices[i]._data;};
	bool getConnected (int i) {return _vertices[i]._connected;};
	void setConnected (int i, bool val) {_vertices[i]._connected = val;};
	void readGraphFromFile (const string &);								// reads graph from file, returns num of vertices read
	void deleteGraph (bool zeroPosInFile);									// deletes all graph data
	void addNeighborsToList(unsigned int index1,unsigned int index2);		// both directions
	void addNeighborToList(unsigned int index1,unsigned int index2);		// adds naighbor into vector
	void removeNeighborsIfExist(unsigned int index1,unsigned int index2);	//both directions
	void removeNeighborIfExist(unsigned int index1,unsigned int index2);	//removes neighbor from vector
	
	unsigned int getVertexNum(int i) {return _vertices[i]._serialNum;};

private:
	class Vertex {
		public:		// class in nested and defined in private part of Graph so we do not separate to private/public and leave it all public
			Vertex ();						// default constructor
			T _data;						// data the vertex contains
			bool _connected;				// is the vertex connected to the graph or is it lonely
			unsigned int _serialNum;		// serial number of vertice, according to creating order
			static unsigned int _counter;	// number of vertices created
			sf::CircleShape _sfShape;		// sfml shape representing the vertex in drawing
			
			                                // get/set position of a vertex
			void setVertexLocation(sf::Vector2f coordinates) { _sfShape.setPosition(coordinates); };
			unsigned int operator()(Vertex ver) { return ver._serialNum; } //convertion operator po int
			sf::Vector2f getVertexLocation () { return _sfShape.getPosition();}
	
	};

	vector<Vertex> _vertices;				// vertices in graph
	vector<vector<int>> _neighbors;			// the vertices' neighbors
	streampos _posInGraphFile;				// position of cursor in levels file from which we read the graph
	ifstream _graphFile;					// file from which we read the different graphs

	bool readLine();	// reads one line (representing one vertices' neighbors list), returns true if successful, false if graph ended
	void createVertex(const vector<int> &);
};


//--------------- G R A P H  F U N C T I O N S ------------//

template <class T>
unsigned int Graph<T>::Vertex::_counter = 0;	// initializing the vertices count to zero

template <class T>
Graph<T>::Graph(const string &fileName): _graphFile() {
	readGraphFromFile(fileName);
}

template <class T>
void Graph<T>::addNeighborsToList(unsigned int index1,unsigned int index2){
	//add in both vertices
	addNeighborToList(index1, index2);
	addNeighborToList(index2, index1);
	
}

template <class T>
void Graph<T>::addNeighborToList(unsigned int index1,unsigned int index2){

	// if alredy exist do nothing
	for (unsigned int i = 0; i < _neighbors[index1].size(); i++)
		if(_neighbors[index1][i] == index2)
			return;
	// else inserting new naighbor into vector
	_neighbors[index1].push_back(index2);
}

template <class T>
void Graph<T>::removeNeighborsIfExist(unsigned int index1,unsigned int index2){
	//remove in both dirrections
	removeNeighborIfExist(index1, index2);
	removeNeighborIfExist(index2, index1);
}

template <class T>
void Graph<T>::removeNeighborIfExist(unsigned int index1,unsigned int index2){
	// if found it
	for (std::vector<int>::iterator it = _neighbors[index1].begin(); it != _neighbors[index1].end(); it++)
		if((*it) == index2){
			_neighbors[index1].erase(it);
			break;
		}
}

template <class T>
void Graph<T>::setAngle (unsigned int vertex, float angle) {
	_vertices[vertex]._data.setHalfEdge(angle, true);
}

template <class T>
void Graph<T>::readGraphFromFile(const string &fileName) {
	streampos fileLen;

	_graphFile.open(fileName);	// opening file
	_graphFile.seekg(0, _graphFile.end);	// getting length of file
	fileLen = _graphFile.tellg();

	// setting position to read from in file:
	if (_posInGraphFile == fileLen)
		_graphFile.seekg(0, _graphFile.beg);
	else
		_graphFile.seekg(_posInGraphFile);

	// reading from file
	while (_graphFile.good()) {
		if (!readLine())
			break;
	}

	// closing file & saving position:
	_posInGraphFile = _graphFile.tellg();
	_graphFile.close();
}

template <class T>
bool Graph<T>::readLine() {
	vector<int> vertices;	// to read vertices into
	int vertexNum;		// current vertex
	
	do {
		if (_graphFile.peek() == '-') {
			_graphFile.get();	// discarding the '-' char
			return false;
		}
		// reading vertex number, the first number is the vertex, all the rest are his neighbors
		_graphFile >> vertexNum;
		vertices.push_back(vertexNum);
		
	} while (_graphFile.peek() != '\n');

	// reading the '/n' char from file to discard it
	_graphFile.get();

	// creating the vertex & it's neighbors list
	createVertex(vertices);
	return true;
}

template <class T>
bool Graph<T>::checkEdge(unsigned int v1, unsigned int v2){

	for (int index = 0; i < _neighbors[v1].size(); i++)
		if(_neighbors[v1][i] == v2)
			return true;
	
	return false;
}

template <class T>
void Graph<T>::createVertex(const vector<int> &neighbors) {
	// creating vertex:
	Vertex newVertex;
	_vertices.push_back(newVertex);

	// updating neighbors list of created vertex
	_neighbors.push_back(neighbors);
}

template <class T>
void Graph<T>::deleteGraph (bool zeroPosInFile) {
	for (unsigned int i = 0; i < _vertices.size(); i++) {
		_vertices.clear();
		_neighbors.clear();
	}

	if (zeroPosInFile)
		_posInGraphFile = 0;
}

//--------------- V E R T E X   F U N C T I O N S ------------//

template <class T>
Graph<T>::Vertex::Vertex(): _serialNum(_counter++), _connected(false), _sfShape(VERTEX_RADUIS){
	// setting basic data for sfShape:
	_sfShape.setFillColor(sf::Color(150,150,150));
	_sfShape.setOrigin(VERTEX_RADUIS, VERTEX_RADUIS);
};

//--------------- BSF ITERATOR ------------//

template <class T>
class Graph<T>::IteratorBFS {
public:
	IteratorBFS (Graph<T> *graphP, int* nodeP = NULL):_graphPtr(graphP), _vertexPtr(nodeP) {};

	int operator*() {return (*_VertexPtr);};
	IteratorBFS operator ++ (int);
	IteratorBFS operator = (const IteratorBFS&);
	bool operator == (const IteratorBFS&) const {return (_vertexPtr == other._vertexPtr && _graphPtr == other._graphPtr);};
	bool operator != (const IteratorBFS&) const;
	IteratorBFS begin();
	IteratorBFS end() {return IteratorBFS(this,NULL);};

private:
	int* _vertexPtr;               // the vertex pointer
	Graph<VertexData> * _graphPtr; // the graph pointer
	std::queue<int> _BfsQeueu;		   // a queue for BFS
};


// ITERATOR FUNCTIONS:

template <class T>
typename Graph<T>::IteratorBFS Graph<T>::IteratorBFS::operator++ (int dummy) {
	//check if we got to the end of Bfs
	if (_graphPtr->_BfsQeueu.empty()){
		_vertexPtr = NULL;
		return *this;
	}
		
	//next node in the queue
	int index = _graphPtr->_BfsQeueu.front();
	_graphPtr->_BfsQeueu.pop();

	//inset to the queue the neighbors that are not colored yet
	std::vector<int>::iterator nIt;
	for(nIt = _graphPtr->_neighbors[*_vertexPtr].begin(); nIt != _graphPtr->_neighbors[*_vertexPtr].end(); nIt++) {
		if(!_graphPtr->getConnected(*nIt)) {
			_graphPtr->setConnected(*nIt);
			_graphPtr->_BfsQeueu.push(*nIt);
		}
	}
	//_vertexPtr = &(_graphPtr->_nodeVec.at(index));
	return *this;
};

template <class T>
typename Graph<T>::IteratorBFS Graph<T>::IteratorBFS::operator = (const IteratorBFS& other) {
	_vertexPtr = other._vertexPtr;
	return *this;
};

template <class T>
bool Graph<T>::IteratorBFS::operator != (const IteratorBFS& other) const {
	if(*this == other)
		return false;
	else
		return true;
}

template <class T>
typename Graph<T>::IteratorBFS Graph<T>::IteratorBFS::begin() {
	_vertices.setConnected((*_vertexPtr), true);
	vector<int>::iterator it;

	for(it = _neighbors[*_vertexPtr].begin(); it != _neighbors[*_vertexPtr].end(); it++){
		_vertices.setConnected ((*it), true);
		_BfsQeueu.push(*it);
	}
	return IteratorBFS(this,(*it));
}