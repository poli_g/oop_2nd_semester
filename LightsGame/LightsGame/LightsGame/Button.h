#pragma once
#include <cstdlib>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>



using std::cerr;

const unsigned int SIZE_OF_FONT = 22;         //default size of font

class Button {
public:
		
	//    C-TORS
	Button ();
	Button (const sf::Vector2f location, const std::string text); 

	//    FUNCTIONS
	//public function that draws a button, this function can recognize if 
	//it's pressed button or normal
	virtual void drawButton (sf::RenderWindow&);
	//change the status of the button to be pressed
	void pressButton() {_isPressed = true;};
	//change the status of the button to be unpressed
	void unpressButton() {_isPressed = false;};
	//returns font
	sf::Font getFont() const {return _font;};
	//returns location of the button
	sf::Vector2f getLocation() const {return _location;};
	void setLocation(sf::Vector2f new_location);
	//this function returns bounds of a button 
	sf::RectangleShape getRect() const {return _rec;};
	
	// returns isPressed data member
	bool getIsPressed () const { return _isPressed; };
	//drawing prosees of pressing a button 
	void drawPressButton(sf::RenderWindow & win);
	//bool changeMovmentStatus ();
			
protected:
		
	//  data members
		
	sf::Vector2f _location;     //keeping location of the button
	sf::Text *_nameOfButton;    //the buttons name
	sf::Font _font;             //font we use (we using only one in this exersize)
	sf::Texture _texture;       //for the texture of green button
	sf::RectangleShape _rec;    //local bounds of button
	bool _isPressed;            //knows if button pressed
	bool _pauseStatus;          //helpful data member for pause a game 

    //function that seting font to be our font
	virtual void setFont();
	
};