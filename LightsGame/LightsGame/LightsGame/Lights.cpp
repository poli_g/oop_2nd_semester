#pragma once
#include <SFML/Graphics.hpp>
#include "Controller.h"
#include "Button.h"

// FUNCTION DECLARATIONS:
bool runIntroScreen (unsigned int&);

int main () {
	//bool playing = true;
	unsigned int lvlNum;
	bool exit = runIntroScreen(lvlNum);
	
	if (!exit) {
		Controller controller(lvlNum);

		while (!exit) {
			exit = controller.runGame();
			if (!exit) {
				exit = runIntroScreen(lvlNum);
				controller.reloadGame(lvlNum);
			}
		}
	}
	return 0;
}

// FUNCTIONS:
bool runIntroScreen (unsigned int &lvlNum) {

	sf::RenderWindow intro(sf::VideoMode(WINDOW_WIDTH, WINDOW_HIGHT/2), "Please select level");
	Button easy(sf::Vector2f((WINDOW_WIDTH/4) * 1, WINDOW_HIGHT/4), "Easy");
	Button medium(sf::Vector2f((WINDOW_WIDTH/4) * 2, WINDOW_HIGHT/4), "Medium");
	Button hard(sf::Vector2f((WINDOW_WIDTH/4) * 3, WINDOW_HIGHT/4), "Hard");
	Button exit(sf::Vector2f((WINDOW_WIDTH/4) * 2, (WINDOW_HIGHT/4)*0.5), "Quit");

	while (intro.isOpen()){

		intro.clear();
		easy.drawButton(intro);
		medium.drawButton(intro);
		hard.drawButton(intro);
		exit.drawButton(intro);
		intro.display();
		
		sf::Event event;
		while (intro.pollEvent(event)) {
			if (event.type == event.MouseButtonPressed) {
				if (exit.getRect().getGlobalBounds().contains(sf::Vector2f(event.mouseButton.x, event.mouseButton.y))) {
					return true;
				}
				if (easy.getRect().getGlobalBounds().contains(sf::Vector2f(event.mouseButton.x, event.mouseButton.y))) {
					lvlNum = 1;
					return false;
				}
				if (medium.getRect().getGlobalBounds().contains(sf::Vector2f(event.mouseButton.x, event.mouseButton.y))) {
					lvlNum = 2;
					return false;
				}
				if (hard.getRect().getGlobalBounds().contains(sf::Vector2f(event.mouseButton.x, event.mouseButton.y))) {
					lvlNum = 3;
					return false;
				}
			}
		}
	}//end of while

	return false;
}