#include "VertexData.h"

VertexData::VertexData () {
	int angle = 0;
	for (int i = 0; i < POSSIBLE_NUM_OF_NEIGHBORS; i++) {
		// inserting possible angles and setting all possible half edges to 0, we update them later when graph is created
		pair<int, bool> newPair;
		newPair.first = angle;		// i represents current angle
		newPair.second = false;		// false because there currently aren't any neighbors, the graph hadn't been built yet
		_possibleHalfEdges.push_back(newPair);
		
		angle += (FULL_CIRCLE/POSSIBLE_NUM_OF_NEIGHBORS);	// increasing angle
	}
}
void  VertexData::setHalfEdge(float angle, bool set) {
// finding the relevant pair in the vector:
	std::vector<pair<int, bool>>::iterator it = _possibleHalfEdges.begin();
	for (; it != _possibleHalfEdges.end(); it++) {
		if ( abs((*it).first - angle) < 1){  //rounding an angle becouse it can be not integer 
			(*it).second = set;
			break;
		} 
	} 
}

void VertexData::addNeighbor(int i) {
	_neighborsInDrawing.push_back(i);
}

bool VertexData::checkHalfEdge(float angle) {
	// finding the relevant pair in the vector:
	std::vector<pair<int, bool>>::iterator it = _possibleHalfEdges.begin();
	for (; it != _possibleHalfEdges.end(); it++) {
		if ((*it).first == angle)
			// checking if theres a half edge at that angle
			return (*it).second;
	}
	return false;
}

void VertexData::rotate(bool clockwise) {
	// if we want to rotate clockwize so the rotating angle is -60
	int rotating_angle;
	if (clockwise)
		rotating_angle = -(FULL_CIRCLE/POSSIBLE_NUM_OF_NEIGHBORS);
	else
		rotating_angle = FULL_CIRCLE/POSSIBLE_NUM_OF_NEIGHBORS;

	// iterator runing over pairs of vector 
	std::vector<pair<int, bool>>::iterator it = _possibleHalfEdges.begin();
	for (; it != _possibleHalfEdges.end(); it++) {
		// adding rotating angle and 360 also if it's unpositive
		(*it).first = ((*it).first+rotating_angle+FULL_CIRCLE) % FULL_CIRCLE;
	}
}