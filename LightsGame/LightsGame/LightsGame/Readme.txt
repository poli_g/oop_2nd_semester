******************************************************************************************
            
                              R E A D M E


*******************************************************************************************


1)	OOP2 : LIGHTS

2)	By:	Polina Granot ID:	307696070
		Shur Svetlana ID:	318032745


3)	In this exersize we implemented a light game, using grafic library SFML.


4)	Added files:

	Graph.h					-	This class is implementation of a graph. Include vector of verticex, list of neighbors as int,
								and nested class Vertex, wich includes Vertex Data. This class contains aditional functions like: 
								check if edge exist between two vertices,   add/remove neighbor, read fron file and more..
								This class is template class, vertex data could be anything, so it' make this class very geneic
	
	VertexData.h/cpp		-	This class contains data memebers that permit us to play lights game. Vertex Data includes
								all information about a vertex neighbors in drawing (6 vertices) and also position of a
								halh edges that we see actually when we plaing.  
	
	Controller.h/cpp		-	This class contains the game graph and it's actually controlls all game. It's responsible of:
								rotating vertices, checking if vertices connected, loading levels add more.. 

	Lights.cpp				-	contains main function.
	
	Button.h/cpp			-	Class contain data members and functions responsible of the buttons
								on the board.
	macros.h				-   includes "#includes" and consts.
	FORTE.ttf			    -	font we desided to use
	lvl1/2/3.txt			-   text file contains data (neighbors list of a graph) of three different levels.
								Two graphs for each level. 
	button_up.png           -	texture we using for show pressed button
	*.wav					-	different sounds we used in the game.

	
5) Data Structures:			-	mostly we used vector from standart library. Vector of <Vertex> ,vector <vector <int>
								for the neighbors, vector<pair<int, bool>> for the half edges in Vertex Data. lso using a
								lot sf::Vector2f from SFML library for coordinates. 

6) Algorithms:				-	1)	for placing a vertices we used mathematic formulas using an angle, lenght of an adge
								and a start point in the miidle of a screan. First we placing vertex in the middle, it'll
								be vertex[0]. After we calculating a corners of hexahedron of a closest ring of vertices.
								For the next rings of vertices we also calculating a corners, saving tham in the vector a and 
								after calculating a points between corners. They are placed on straight line, so we can calculate 
								tham also. Function is generic can build any level of the game you wish!
								
								2)	locating the surrounding vertices of each vertex in drawing is done using calculations (according to how the vertices are placed).
									(there's a certain regularity).
								
								3)	IteratorBFS runs using known BFS algorithm.  

7) Known Bugs:				-	no bugs were detected

8) Additional Comments:		-	we got one day late permition from Michal
							