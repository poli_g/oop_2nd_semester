#include "SmartMonster.h" 


SmartMonster::SmartMonster(void)
{
}


SmartMonster::~SmartMonster(void)
{
}

void SmartMonster::sortDirectionsByPriority(sf::Vector2f monster_loc,sf::Vector2f pacmanLoc, std::vector<direction> &vec){

	float up_dis, down_dis, left_dis, right_dis;
	//this list is holding different distances and helping us to sort them from small to bigger
	std::list <float> dist_list;
	//counting distance from four points around a monster
	up_dis = distance(sf::Vector2f(monster_loc.x,monster_loc.y-spriteCoeff), pacmanLoc);
	dist_list.push_back(up_dis);
	right_dis = distance(sf::Vector2f(monster_loc.x+spriteCoeff,monster_loc.y), pacmanLoc);
	dist_list.push_back(right_dis);
	down_dis = distance(sf::Vector2f(monster_loc.x,monster_loc.y+spriteCoeff), pacmanLoc);
	dist_list.push_back(down_dis);
	left_dis = distance(sf::Vector2f(monster_loc.x-spriteCoeff,monster_loc.y), pacmanLoc);
	dist_list.push_back(left_dis);
	
	dist_list.sort(); //sorting the list of distances
	int size_of_list = dist_list.size();
	//inserting into vector of directions
	for(int i = 0; i < size_of_list; i++){
		if (dist_list.front() == up_dis)
			vec.push_back(UP_DT);
		else if(dist_list.front() == right_dis)
			vec.push_back(RIGHT_DT);
		else if(dist_list.front() == down_dis)
			vec.push_back(DOWN_DT);
		else if(dist_list.front() == left_dis)
			vec.push_back(LEFT_DT);
		dist_list.pop_front();

	}
}

bool SmartMonster::monsterMoveTospot (sf::Vector2f newLocation,const std::vector <std::vector<UnmovableObject*>> &board, const direction &dir) {
	// vector to store possible movement locations in:
	std::vector <sf::Vector2i> locsToCheck = setLocsTocheck(dir, newLocation, board);	// all locations we need to check
	
	// now we create a rectangle, the same size as the object we're moving, and place it at the new location the object will move to if it can. 
	//That is to check the	intersection between the bounding rectangles of that temp object and the walls around:
	sf::RectangleShape tempRect(sf::Vector2f(spriteCoeff, spriteCoeff));
	tempRect.setOrigin(spriteCoeff/2, spriteCoeff/2);
	tempRect.setPosition(newLocation);

	// checking collision with all points possible:
	for (unsigned int i = 0; i < locsToCheck.size(); i++) {
		if (board[locsToCheck.at(i).x][locsToCheck.at(i).y] != NULL)
			if (board[locsToCheck.at(i).x][locsToCheck.at(i).y]->isObstacle()) {
				if (tempRect.getGlobalBounds().intersects(board.at(locsToCheck.at(i).x).at(locsToCheck.at(i).y)->getLook().getGlobalBounds()))	// if they intersect
					return false;
			}
	}
	return true;

}

direction SmartMonster::setDirection (const std::vector <std::vector<UnmovableObject*>> & board, direction dir, sf::Vector2f pacmanLoc) {

	sf::Vector2f monster_loc = sf::Vector2f(getLook().getPosition().x, getLook().getPosition().y);
	std::vector <direction> vec_of_dir;
	//calls to function that sorts directions by value
	sortDirectionsByPriority (monster_loc, pacmanLoc, vec_of_dir);
	
	direction cur_dir = vec_of_dir[0];
	for (int i = 0; i < vec_of_dir.size(); i++){

		//checks if moving to this direction is possible, if not - choosing diferent direction 
		switch(vec_of_dir[i]){
				case UP_DT:
					if (monsterMoveTospot (sf::Vector2f(monster_loc.x,monster_loc.y-spriteCoeff), board, UP_DT))
						return UP_DT;
					break;
				case DOWN_DT:
						if (monsterMoveTospot (sf::Vector2f(monster_loc.x,monster_loc.y+spriteCoeff), board, DOWN_DT))
						return DOWN_DT;
					break;
				case LEFT_DT:
						if (monsterMoveTospot (sf::Vector2f(monster_loc.x-spriteCoeff,monster_loc.y), board, LEFT_DT))
						return LEFT_DT;
					break;
				case RIGHT_DT:
						if (monsterMoveTospot (sf::Vector2f(monster_loc.x+spriteCoeff,monster_loc.y), board, RIGHT_DT))
						return RIGHT_DT;
					break;
		}
	}//end of for
	//returns direction to move on
	return cur_dir;
}

