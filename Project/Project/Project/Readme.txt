******************************************************************************************
            
                              R E A D M E


*******************************************************************************************


1)	OOP PROJECT: PACMAN

2)	By:	Polina Granot ID:	307696070
		Shur Svetlana ID:	318032745


3)	In this exersize we implemented a pacman game, using grafic library SFML.


4)	Added files:


	Button.h/cpp			-  Class contain data members and functions responsible of the buttons
								on the board. 
	Pause.h/cpp			    -  Derived class of class Button responsible of the button "Pause"
								(pause the game). 
	Help.h/cpp				-  Derived class of class Button responsible of the button "Help"
								open help window and auomaticly pauses the game.
	Exit.h/cpp				-  Derived class of class Button responsible of the button "Exit"
	Object.h/cpp			-  Base class of MovebleObject and UnmovableObject holding data members
								that common to both of classes (like: texture, look).
								in constractor "sprite_sheet.png" is loaded (once in the beginning of the game)
								This Sprite sheet includes all images/looks of all objects of the game.
								We loading it only once and cuting needed rectangle from there when we need.    
	MovableObject.h/cpp		-  Base class of Player and Monster holding data members that common to both 
								of classes (like: speed, primery location(for placing in the corner)).
	UnMovableObject.h/cpp	-  Base class of Obsticle and Cookie.
	Obsticle.h/cpp			-  Obsticles of the board. There is two kinds of it (different look).
	Cookie.h/cpp			-  Cookies of the board. There is three kinds of it (different look and different effect on pacman).
	Monster.h/cpp			-  Base abstract class of DumbMonster and SmartMonster holding diferent functions, 
								that common to both of classes (like: change look (direction of eyes where moving).
								In constractor it randomly chooses look of the monster (one of five existing).
	DumbMonster.h/cpp		-  Class represents DumbMonster that choosing direction randomaly. When changes 
								direction of movement - change direction of eyes. 
	SmartMonster.h/cpp		-  Class represents SmartMonster that choosing direction by simple known algotithm. When changes 
								direction of movement - change direction of eyes.
	Player.h/cpp			-  Base abstract class of CompPlayer and UserPlayer holding diferent functions, 
								that common to both of classes (like: change look (direction of eyes where moving).
								In constractor it randomly chooses look of the monster (one of five existing).
	UserPlayer.h/cpp		-  Class represents UserPlayer - the pacman. Moving of UserPlayer is by using a keyboard.
								Game starts by pressing a Space. This is Pause also. 
	CompPlayer.h/cpp		-  Computer Player - PacWoman. Choosing	direction randomly. can eat a cookies also.				 										
	GameController.h/cpp	-  This class contain the vector of vectors of *UnmovebleObjects,  
								vector of *player, vector of *monster. This class is responsible of runing the game, 
								collision detectors and more. This class controls all game and all files in the program. 
	Pacman.cpp				 -  main
	forte.ttf                -  font we desided to use
	sprite_sheet.png		 -  texture of all kinds of objects and there conditions (loading only once when constructing
								a GameController)		
	yellow_button_up.png     -  texture we using for show pressed button
	yellow_button_down.png   -  texture we using for show unpressed button
	*.wav					 -  different sounds we used in the game.

	
5) Data Structures:		we used this project vector of vectors of UnmovebleObject*  vector<vector<UnmovebleObject*>>
						that holding all unmoveble objects of the game: obsticals and  a cookies. 
						vector of Players - vector of pacmans of the game, in first place it's allways UserPlayer,
						CompPlayers comes after him.
						vector of monsers - holds vector of all monsters of the game. and vector of buttons also.
						We choose to use this data structure becouse it's very easy to use and helps to contein a data.
						In one place we used <list> for the sort becouse <list> has an implamentation of the sort in
						standart library.

6) Algorithms:			We used simple algorithm for the chasing pacman. It checks four different points monster can move there,
						calculates distance from each point to the pacman, sorting this points by distance, and try to move to 
						the first(closest) point, if it's inpossible - try to move to the next point, etc. 
								   

7) Known Bugs:			no bugs were detected


8) Additional Comments: The game starts by pressing  "Space" button. Loading of each level can take some time depends on
						difficulty/size of a current level.     