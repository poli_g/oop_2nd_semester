#pragma once
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <cmath>
#include <vector>

const float spriteCoeff = 25;		// the size of the objects in the sprite sheet
const float playerSpriteCoeff = 23;

using std::vector;

class Object {
public:
	Object();
	virtual ~Object(void);

	// FUNCTIONS:
	virtual sf::Sprite &getLook () { return _look; };
	sf::Music _sound;
	
protected:
	sf::Texture _texture;		// pointer to sprite sheet
	sf::Sprite _look;			// how the object looks
	
	//function that calculates a distance between two points
	float distance(sf::Vector2f point1, sf::Vector2f point2);
};

