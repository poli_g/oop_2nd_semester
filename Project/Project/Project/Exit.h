#pragma once
#include "Button.h"

class Exit : public Button {

public:

	Exit(sf::Vector2f v2f);
	~Exit();

protected:

	virtual bool buttonFunctionality(sf::Clock &movementClock,  bool & movement) {return false;};
	
};

