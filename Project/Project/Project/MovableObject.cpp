#include "MovableObject.h"


MovableObject::MovableObject(): _speed(50.f), _primaryLocation(0, 0) {
}

bool MovableObject::isObjectThere(const sf::Vector2f& startPoint){

	if (getPrimaryLocation() == startPoint)
		return true;
	else
		return false;

}

MovableObject::~MovableObject(void)
{
}


std::vector <sf::Vector2i> MovableObject::setLocsTocheck (direction dir, sf::Vector2f newLocation, const vector <vector<UnmovableObject*>> &board) {
	std::vector <sf::Vector2i> locsToCheck;
	
	switch (dir) {
		case UP_DT:
			for (int j = -1; j <=1; j++) {
				sf::Vector2i point((newLocation.x / spriteCoeff) - j, (newLocation.y / spriteCoeff) - 1);
				if (point.x < board.size() && point.y < board[0].size())
					locsToCheck.push_back(point);
			}
			break;
		case DOWN_DT:
			for (int j = -1; j <=1; j++) {
				sf::Vector2i point((newLocation.x / spriteCoeff) - j, (newLocation.y / spriteCoeff) + 1);
				if (point.x < board.size() && point.y < board[0].size())
					locsToCheck.push_back(point);
			}
			break;
		case LEFT_DT:
			for (int j = -1; j <=1; j++) {
				sf::Vector2i point((newLocation.x / spriteCoeff) - 1, (newLocation.y / spriteCoeff) - j);
				if (point.x < board.size() && point.y < board[0].size())
					locsToCheck.push_back(point);
			}
			break;
		case RIGHT_DT:
			for (int j = -1; j <=1; j++) {
				sf::Vector2i point((newLocation.x / spriteCoeff) + 1, (newLocation.y / spriteCoeff) - j);
				if (point.x < board.size() && point.y < board[0].size())
					locsToCheck.push_back(point);
			}
			break;
	} 
	
	return locsToCheck;
}

bool MovableObject::moveTospot (sf::Vector2f newLocation,const std::vector <std::vector<UnmovableObject*>> &board, const direction &dir) {
	// vector to store possible movement locations in:
	std::vector <sf::Vector2i> locsToCheck = setLocsTocheck(dir, newLocation, board);	// all locations we need to check
	
	// now we create a rectangle, the same size as the object we're moving, and place it at the new location the object will move to if it can. 
	//That is to check the	intersection between the bounding rectangles of that temp object and the walls around:
	sf::RectangleShape tempRect(sf::Vector2f(playerSpriteCoeff, playerSpriteCoeff));
	tempRect.setOrigin(playerSpriteCoeff/2, playerSpriteCoeff/2);
	tempRect.setPosition(newLocation);

	// checking collision with all points possible:
	for (unsigned int i = 0; i < locsToCheck.size(); i++) {
		if (board[locsToCheck.at(i).x][locsToCheck.at(i).y] != NULL)
			if (board[locsToCheck.at(i).x][locsToCheck.at(i).y]->isObstacle()) {
				if (tempRect.getGlobalBounds().intersects(board.at(locsToCheck.at(i).x).at(locsToCheck.at(i).y)->getLook().getGlobalBounds()))	// if they intersect
					return false;
			}
	}
	return true;

}

void MovableObject::move (direction dir, float time, std::vector <std::vector<UnmovableObject*>> &board, sf::Vector2f pac_position) {
	// setting direction for monsters and computer players:
	if (isCompPlayer() || isMonster())
		dir = setDirection(board, dir, pac_position);

	// getting object's current location:
	sf::Vector2f objLocation (getLook().getPosition().x, getLook().getPosition().y);

	bool canMove = true;	// can the object perform the movement to the spot he's supposed to

 	switch (dir) { 
		case (UP_DT):
			canMove = moveTospot(sf::Vector2f(objLocation.x, objLocation.y - (_speed * time)), board, dir);

			// if we can move, we move:
			if (canMove)
				getLook().move(0.f, -(_speed * time));	// first, we move
			break; 

		case (DOWN_DT):
			canMove = moveTospot(sf::Vector2f(objLocation.x, objLocation.y + (_speed * time)), board, dir);

			// if we can move, we move:
			if (canMove)
				getLook().move(0.f, (_speed * time));	// first, we move
			break;
		case (LEFT_DT):
			canMove = moveTospot(sf::Vector2f(objLocation.x - (_speed * time), objLocation.y), board, dir);

			// if we can move, we move:
			if (canMove) {
				getLook().move(-(_speed * time), 0.f);
			}
			break;
		case (RIGHT_DT):
			canMove = moveTospot(sf::Vector2f(objLocation.x + (_speed * time), objLocation.y), board, dir);

			// if we can move, we move:
			if (canMove) {
				getLook().move((_speed * time), 0.f);
			}
			break;
	}

	// changing look of the object:
	changeLook(dir);
}
