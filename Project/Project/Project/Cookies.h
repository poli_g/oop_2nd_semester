#pragma once
#include "UnmovableObject.h"

const float cookieSpriteCoeff = 10;	// the size of the cookies in the sprite sheet

class Cookies :	public UnmovableObject {
public:
	Cookies(void);
	~Cookies(void);

	// FUNCTIONS:
	// returns _numOfCookies:
	static int getNumOfCookies () { return _numOfCookies; };
	// checks if this is a cookie:
	bool isCookie () { return true; };
	// checks if this is a wall:
	bool isObstacle () { return false; };
	// returns _speedpercent:
	float getSpeedPercent () { return _speedPercent; };
	// returns _speedTimer:
	float getSpeedTimer () { return _speedTimer; };

protected:
	static int _numOfCookies;
};

