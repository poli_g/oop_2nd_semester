#pragma once
#include "Player.h" 
#include "MovableObject.h"

class UserPlayer : public Player {
public:
	UserPlayer(void);
	~UserPlayer(void);
	// returns false in this class:
	virtual bool isCompPlayer() { return false; };
	// changes the appearance of the object according to it's direction
	void changeLook (direction dir);

protected:
	// checks with _board if we can move to the desired spot:
	bool move (sf::Vector2f &positionToCheck, std::vector <std::vector<UnmovableObject*>> &);
	// sets the direction of the userplayer, returns the direction set by user
	virtual direction setDirection (const std::vector <std::vector<UnmovableObject*>> &, direction dir, sf::Vector2f) { return dir; };
};

