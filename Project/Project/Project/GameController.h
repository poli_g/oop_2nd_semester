#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <fstream>				// for working with files
#include <sstream>				// for displaying score and lives more comfortably
#include <vector>
#include "Button.h"
#include "Exit.h"
#include "Help.h"
#include "Pause.h"
#include "MovableObject.h"
#include "UnmovableObject.h"
#include "Obstacle.h"
#include "Cookies.h"
#include "UserPlayer.h"
#include "CompPlayer.h"
#include "Player.h"
#include "Monster.h"
#include "Pause.h"
#include "Help.h"
#include "Exit.h"
#include "SmartMonster.h"
#include "DumbMonster.h"

const int SIZE_OF_SCORE = 27;		// size of score to display
const int numOfLevels = 5;			// number of levels we have

using std::vector;

class GameController {
public:
	GameController(sf::Font);
	~GameController(void);

	// FUNCTIONS:
	bool runGame ();
	//void stopPlaying() {_keepPlaying=false;};
	

private:
	vector <vector<UnmovableObject*>> _board;		// matrix storing the locations of all the static objects on the game board
	vector <Player*> _players;
	vector <Monster*> _monsters;
	sf::Text _textScore;
	sf::Music _music;
	
	sf::Font _font;
	std::ifstream _levelsFile;
	sf::Texture _spriteSheet;
	bool _keepPlaying;				// so we know if we need to keep playing
	vector <Button*> _gameButtons;	// buttons in game (pause, exit, etc)
	int _levelNum;
	int _game_score;
	bool _movement;
	bool _loadingComplete;
	
	// FUNCTIONS:
	void readObstaclesFromFile ();	// reads the levels file, creates walls where needed
	void initBoardSize ();			// initializes board width and heigh
	void initBoard ();			    // initializes _board data member
	void fillCookies ();			// fills cookies where there's empty spots on the board
	// creates the user player:
	void createUserPlayer ();
	// creates the computer players:
	void createComputerPlayers ();
	// creates all monsters of the game
	void createMonsters();
	// creates the buttons for the game (menu, help, etc)
	void createGameButtons ();
	//place buttons after loadind new level
	void placeButtons ();
	// draws the buttons of the game to the screen
	void drawButtons (sf::RenderWindow &window);      
	// handles all the events in the game loop
	void handleEvents (sf::RenderWindow &window, const sf::Event &event, sf::Clock &movementClock, enum direction &playerDir);
	//this function finds closest empty place to start point on the board 
	sf::Vector2f GameController::findClosestEmptyPlace(const sf::Vector2f& startPoint);
	// this function loads the next level:
	void loadNextLevel (sf::RenderWindow &window, sf::Clock &clock);
	//draws score on the board
	void drawScoreAndLivesOnBoard (sf::RenderWindow &winToDrawOn);
	//set origin of the text
	void setText(sf::Text& txt);
	// checks if the player wants to play a new game:
	bool playNewGame ();
	// checks players' collision with monsters:
	void checkColllisionWithMonsters (sf::Clock &movementClock);
	// places all monsters and pacmans to their primary locations:
	void resetPrimaryLocations();
	//cheks if it's some moveble object in that point
	bool isMovebleObjectThere(const sf::Vector2f& startPoint);
	// sets all settings for the player created
	void setPlayerSettings (Player *player);
	// this function checks if any of the players ate a cookie:
	void checkCollisionWithCookies(Player*, sf::Clock &, float &, float &);
	// loading thread function:
	void loadingScreen();
};

