#include "CompPlayer.h"


CompPlayer::CompPlayer() {
	_lives = 2;
	_look.setTextureRect(sf::IntRect(_colInSpriteSheet * spriteCoeff, RIGHT_WSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
}


CompPlayer::~CompPlayer(void)
{
}

direction CompPlayer::setDirection (const std::vector <std::vector<UnmovableObject*>> &, direction dir, sf::Vector2f loc) {
	srand (time (NULL));
	int val = rand() % 4 + 1;

	switch (val) {
		case 1:
			return UP_DT;
			break;
		case 2:
			return DOWN_DT;
			break;
		case 3:
			return LEFT_DT;
			break;
		case 4:
			return RIGHT_DT;
			break;
		default:
			UP_DT;
			break;
	}
}

void CompPlayer::changeLook (direction dir) {
	// opening mouth
	if (_colInSpriteSheet < 2)
		_colInSpriteSheet++;
	else
		_colInSpriteSheet = 0;
	
	switch (dir) {
		case UP_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, UP_WSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
		case DOWN_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, DOWN_WSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
		case LEFT_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, LEFT_WSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
		case RIGHT_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, RIGHT_WSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
	}

}
