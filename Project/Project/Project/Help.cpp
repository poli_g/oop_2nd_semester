#include "Help.h"


Help::Help(sf::Vector2f v2f)
{
	_location = v2f;
	_rec.setOrigin(_rec.getSize() / 2.f);
	_rec.setPosition(_location);
	
	_nameOfButton = new sf::Text(std::string ("Help"), _font, 30);
	setFont();

}

bool Help::buttonFunctionality(sf::Clock &movementClock,  bool & movement){
	
	movement = !movement;

	// size of window:
	unsigned int winWidth = 600;
	unsigned int winHeight = 400;

	// all the text we need to display:
	sf::Text msg ("#  PACMAN  #", _font, 40);
	msg.setColor (sf::Color(153,0,210));
	sf::Text msg1 ("Presentors: Svetlana Shur & Polina Granot", _font,30);
	msg1.setColor (sf::Color(153,0,210));
	sf::Text msg2 ("Start the game by pressing the Space Button.\nMove the pacman by pressing UP/DOWN/LEFT/RIGHT on keybord\nUser Player is represented by pacman, computer players - pacwoman.\nYelow cookie - regular cookie, Red cookie - is poison cookie\n(decreasing player speed),\nBlue - speedup cookie (encreasing a speed for 7 seconds).\nEat all cookies on the board and don't let a monser catch you!!!", _font, 19);
	sf::Text msg3 ("GOOD LUCK!",_font, 40);
	msg3.setColor (sf::Color(153,0,210));
	
	//setting origin point of a text
	msg.setOrigin(msg.getLocalBounds().width / 2, msg.getLocalBounds().height / 2);
	msg1.setOrigin(msg1.getLocalBounds().width / 2, msg1.getLocalBounds().height / 2);
	msg3.setOrigin(msg3.getLocalBounds().width / 2, msg3.getLocalBounds().height / 2);
	
	//seting position of the text
	msg.setPosition((float)winWidth / 2, ((float)winHeight / 15) * 1);
	msg1.setPosition((float)winWidth / 2, ((float)winHeight / 15) * 3);
	msg2.setPosition(30, ((float)winHeight / 15) * 5);
	msg3.setPosition((float)winWidth / 2, ((float)winHeight / 15) * 13);

	//creating help window 600x400
	sf::RenderWindow msgWin (sf::VideoMode(winWidth, winHeight), "# HELP #   press any key to continue...");
	msgWin.setVerticalSyncEnabled(true);

	while (msgWin.isOpen()) {
		msgWin.clear();
		msgWin.draw(msg);
		msgWin.draw(msg1);
		msgWin.draw(msg2);
		msgWin.draw(msg3);
		msgWin.display();
		
		sf::Event event;
		while (msgWin.pollEvent(event)) {
			if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed)){
			msgWin.close();
			}
		}
	}//while window is open
	
	movement = !movement;
	movementClock.restart();
	return true;
}

Help::~Help(void)
{
}
