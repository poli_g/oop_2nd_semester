#include "Object.h"

Object::Object() {
	// loading sprite sheet containing visual representation of all objects:
	_texture.loadFromFile("sprite_sheet.png");
	_look.setTexture(_texture);
}

Object::~Object(void){
}

float Object::distance(sf::Vector2f point1, sf::Vector2f point2){

	//calculates distance between two point by using known formula
	return sqrt (pow((point1.x - point2.x),2) + pow((point1.y - point2.y),2));
}