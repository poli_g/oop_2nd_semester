#include "Monster.h"
#include "GameController.h"

Monster::Monster() {

	sf::Clock clock;
	srand ((unsigned int)clock.getElapsedTime().asMicroseconds());
	int val = rand() % 5 + 1;
	switch (val) {
		case 1:
			_colInSpriteSheet = 3;
			_look.setTextureRect (sf::IntRect ((int)_colInSpriteSheet * spriteCoeff, 0, (int)spriteCoeff, (int)spriteCoeff));
			break;
		case 2:
			_colInSpriteSheet = 4;
			_look.setTextureRect (sf::IntRect ((int)_colInSpriteSheet * spriteCoeff, 0,(int) spriteCoeff, (int)spriteCoeff));
			break;
		case 3:
			_colInSpriteSheet = 5;
			_look.setTextureRect(sf::IntRect ((int)_colInSpriteSheet * spriteCoeff, 0,(int) spriteCoeff, (int)spriteCoeff));
			break;
		case 4:
			_colInSpriteSheet = 6;
			_look.setTextureRect (sf::IntRect ((int)_colInSpriteSheet * spriteCoeff, 0, (int)spriteCoeff,(int) spriteCoeff));
			break;
		case 5:
			_colInSpriteSheet = 7;
			_look.setTextureRect (sf::IntRect ((int)_colInSpriteSheet * spriteCoeff, 0,(int) spriteCoeff,(int) spriteCoeff));
			break;
	}
}

Monster::~Monster()
{
}

void Monster::changeLook (direction dir) {
	switch (dir) {
		case UP_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, UP_MR * spriteCoeff, (int) spriteCoeff,(int) spriteCoeff));
			break;
		case DOWN_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, DOWN_MR * spriteCoeff, (int) spriteCoeff,(int) spriteCoeff));
			break;
		case LEFT_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, LEFT_MR * spriteCoeff, (int) spriteCoeff,(int) spriteCoeff));
			break;
		case RIGHT_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, RIGHT_MR * spriteCoeff, (int) spriteCoeff,(int) spriteCoeff));
			break;
	}
}