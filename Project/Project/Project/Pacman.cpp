#include <SFML/Graphics.hpp>
#include "GameController.h"

int main () {
	sf::Font font;
	bool keepPlaying = true;
	bool loadingComplete = false;
	font.loadFromFile("forte.ttf");
	sf::Music introMusic;
	introMusic.openFromFile("pacman_open.wav");
	sf::Texture pacmanScreen;
	pacmanScreen.loadFromFile("pacman.png");
	sf::Sprite load(pacmanScreen);

	while (keepPlaying) {
		sf::RenderWindow loadingWindow(sf::VideoMode(pacmanScreen.getSize().x, pacmanScreen.getSize().y, 32), "Loading, Please wait :)");
		introMusic.openFromFile("pacman_open.wav");
		introMusic.setLoop(true);
		introMusic.play();

		loadingWindow.clear(sf::Color::White);
		loadingWindow.draw(load);
		loadingWindow.display();

		GameController controller(font);

		introMusic.stop();
		loadingWindow.close();

		loadingComplete = true;
		keepPlaying = controller.runGame();
	}
}
