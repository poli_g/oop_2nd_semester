#pragma once
#include "MovableObject.h"

// these help us draw the objects from the sprite sheet
enum pacmanSheetRow {UP_MSR, RIGHT_MSR, DOWN_MSR, LEFT_MSR};
enum pacwomanSheetRow {UP_WSR = 4, RIGHT_WSR = 5, DOWN_WSR = 6, LEFT_WSR = 7};

class Player : public MovableObject {
public:
	Player();
	virtual ~Player();

	// returns number of lives a player has:
	int getLives () { return _lives; };
	// sets the number of lives a player has:
	void setLives (int newLives) { _lives = newLives; };
	// returns if the current object is a computer player
	virtual bool isCompPlayer() = 0;
	// returns if current object is a monster:
	virtual bool isMonster() { return false; };
	// sets the movement direction randomly
	virtual direction setDirection (const vector <vector<UnmovableObject*>> &, direction, sf::Vector2f) = 0;
	// is the pacman at this location:
	bool isPacmanThere(const sf::Vector2f& startPoint);
	// changes the appearance of the object according to it's direction
	virtual void changeLook (direction dir) = 0;

protected:
	int _lives;
};

