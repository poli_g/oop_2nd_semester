#include "Pause.h"


Pause::Pause (sf::Vector2f v2f)
{
	_location = v2f;
	_rec.setOrigin(_rec.getSize() / 2.f);
	_rec.setPosition(_location);
	
	_nameOfButton = new sf::Text(std::string ("Pause"), _font, 30);
	setFont();

}
bool Pause::buttonFunctionality(sf::Clock &movementClock, bool & movement){
	
	//stop movment
	movement = !movement;
	//restart a clock
	movementClock.restart();
	return true;
}

Pause::~Pause(void)
{

}
