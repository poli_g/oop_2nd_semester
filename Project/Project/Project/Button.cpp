#include "Button.h"

/******************* C-TORS *********************************************/

Button::Button(): _rec(sf::RectangleShape(sf::Vector2f(150, 50))),
	_location (sf::Vector2f(100,50)), _isPressed (false), _pauseStatus(false) {
		
	 _texture.loadFromFile ("yelow_button_up.png");
	
	//filling size of rectangle
	_texture.create((unsigned int)_rec.getLocalBounds().width,
	(unsigned int)_rec.getLocalBounds().height);

	_font.loadFromFile("forte.ttf");
	
	
}

Button::Button(const sf::Vector2f location, const std::string text):
				_location (location), _isPressed (false), _pauseStatus(true) {

	_rec = sf::RectangleShape(sf::Vector2f(150, 50));
	_rec.setOrigin(_rec.getSize() / 2.f);
	_rec.setPosition(_location);

	_texture.loadFromFile ("yelow_button_up.png");
	
	//filling size of rectangle
	_texture.create((unsigned int)_rec.getLocalBounds().width,
	(unsigned int)_rec.getLocalBounds().height);
	
	//loading font from file
	_font.loadFromFile("forte.ttf");
	_nameOfButton = new sf::Text(text, _font, 30);
	setFont();

}

/*********************** FUNCTIONS ****************************************/
void Button::setFont(){

	//loading our font from the file
	
	_nameOfButton->setFont(_font);                  //setting font and size 
	_nameOfButton->setCharacterSize(SIZE_OF_FONT);
	_nameOfButton->setColor(sf::Color::Black);
	
	//now we set the origin point of a text to be a middle
	//of a rectangle that boundet it
	_nameOfButton->setOrigin(_nameOfButton->getLocalBounds().width/2.f,
		_nameOfButton->getLocalBounds().height/2.f+5 ); 
	//setting a location of a text to be a middle of a button
	_nameOfButton->setPosition(_location); 

}
//drawing a prosess of press and unpressing the button  
void Button::drawPressButton(sf::RenderWindow & win){

	pressButton();
	drawButton(win);
	win.display();
	unpressButton();
	drawButton(win);
	win.display();
}

void Button::drawButton (sf::RenderWindow & window){

	//loadind texture of a button
	if (_isPressed){
		_texture.loadFromFile ("yellow_button_down.png");
	}
	else
		_texture.loadFromFile ("yellow_button_up.png");
	
	_rec.setTexture(&_texture);

	//draws a buton and a text on it
	window.draw (_rec);
	window.draw(*_nameOfButton);
	
}

void Button::setLocation(sf::Vector2f new_location){
	
	_location = new_location;
	_rec.setPosition(_location);
	_nameOfButton->setPosition(_location);

}
