#include "GameController.h"

GameController::GameController(sf::Font font): _font(font), _levelsFile("levels.txt", std::ifstream::in),
	_keepPlaying (true), _levelNum(0), _movement(false), _game_score(0), _loadingComplete (false){
	// setting score:
	_textScore.setFont(_font);
	// loading sprite sheet containing visual representation of all objects:
	_spriteSheet.loadFromFile("sprite_sheet.png");
	// opening levels file, and initing all the stream positions:
	if (!_levelsFile.is_open())
		exit (EXIT_FAILURE);
	//load music from the file 
	_music.openFromFile("pacman_open.wav");
	// initializing _board's size and starting params:
	initBoard();
	// initializing _walls, and width and height of board:
	readObstaclesFromFile();
	// create all players:
	createUserPlayer();
	createComputerPlayers();
	// set monsters locations:
	createMonsters();
	// fill cookies where empty spaces remain:
	fillCookies();
	//creating a game buttons
	createGameButtons ();
}

GameController::~GameController(void) {
	while (_gameButtons.size() > 0) {
		delete _gameButtons[_gameButtons.size() - 1];
		_gameButtons.pop_back();
	}
	_levelsFile.close();
}

void GameController::createMonsters() {
	// clearing the vector first:
	for (unsigned int i = 0; i < _monsters.size(); i++)
		delete _monsters[i];
	_monsters.clear();

	Monster* monster;
	for (int i = 0; i < _levelNum; i++) { 

	//	creating new monster:
		int k = i % 2;
		if (k == 0)
			monster = new SmartMonster();
		else
			monster = new DumbMonster();

		// setting primary monster location:
		sf::Vector2f monsterLocation;
		int j = i % 4 ;
		switch (j) {
			case 0:
				monsterLocation = findClosestEmptyPlace(sf::Vector2f(1, 1));
				break;
			case 1:
				monsterLocation = findClosestEmptyPlace(sf::Vector2f((float)_board.size() - 2, 1));
				break;
			case 2:
				monsterLocation = findClosestEmptyPlace(sf::Vector2f((float)_board.size() - 2,(float) _board.at(0).size() - 2));
				break;
			case 3:
				monsterLocation = findClosestEmptyPlace(sf::Vector2f(1.f, (float) _board.at(0).size() - 2));
				break;
		}
		monster->setPrimaryLocation(monsterLocation);
		monster->getLook().setOrigin(spriteCoeff/2, spriteCoeff/2);
		monster->getLook().setPosition(monsterLocation.x * spriteCoeff + (spriteCoeff / 2), monsterLocation.y * spriteCoeff + (spriteCoeff / 2));
		_monsters.push_back(monster);
	}
}

void GameController::createUserPlayer () {
	Player* player = new UserPlayer();
	setPlayerSettings(player);
}

void GameController::createComputerPlayers () {
	srand (time(NULL));
	int numOfPacWomen = rand () % 3;

	for (unsigned int i = 0; i < numOfPacWomen; i++) {
		// creating players
		Player* player = new CompPlayer();

		setPlayerSettings(player);
	}
}

void GameController::setPlayerSettings (Player *player) {
	// finding position to place player at:
	sf::Vector2f playerLocation = findClosestEmptyPlace (sf::Vector2f(_board.size() / 2, _board.at(0).size() / 2));
	// saving the primary location of the player:
	player->setPrimaryLocation(playerLocation);
	// setting the reference point of the sprite representing the player
	player->getLook().setOrigin((spriteCoeff-2) / 2, (spriteCoeff-2) / 2);
	// setting the location of the player to the one we calculated:
	player->getLook().setPosition(playerLocation.x * spriteCoeff + ((playerSpriteCoeff) / 2), playerLocation.y * spriteCoeff + ((playerSpriteCoeff) / 2));
	// adding player to the vector of players:
	_players.push_back(player);
}

void GameController::initBoard () {
	// setting size:
	initBoardSize();

	// initializing _board so that all pointers are NULLs
	for (unsigned int i = 0; i < _board.size(); i++) {
		for (unsigned int j = 0; j < _board.at(i).size(); j++) { 
			_board.at(i).at(j) = NULL;
		}
	}
}

void GameController::initBoardSize () {
	// reading size from file
	// clearing any whitespaces:
	char c;
	if (_levelsFile.peek() == ' ' || _levelsFile.peek() == '\n')
		_levelsFile.get(c);
	char size[5];
	_levelsFile.getline(size, 3, 'X');
	int boardWidth = atoi(size);
	_levelsFile.getline(size, 3);
	int boardHeight = atoi(size);

	// resizing the board to the needed size
	_board.resize(boardHeight);
	for (unsigned int i = 0; i < _board.size(); i++)
		_board.at(i).resize(boardWidth);
}


void GameController::readObstaclesFromFile () {
	char c;		// to store current char read from file

	for (unsigned int j = 0; j < _board.at(0).size(); j ++) {
		for (unsigned int i = 0; i < _board.size(); i ++) {	
			_levelsFile.get(c);
			if (c == '\n')
				_levelsFile.get(c);
			if (c == '#') {
				_board.at(i).at(j) = new Obstacle();
				_board.at(i).at(j)->getLook().setOrigin(spriteCoeff / 2, spriteCoeff / 2);
				_board.at(i).at(j)->getLook().setPosition(sf::Vector2f(i * spriteCoeff + spriteCoeff / 2, j * spriteCoeff + spriteCoeff / 2));
			}
		}
	}

	_levelNum++;
}

void GameController::fillCookies () {
	for (unsigned int i = 0; i < _board.size(); i++) {
		for (unsigned int j = 0; j < _board.at(i).size(); j++) {
			if (_board.at(i).at(j) == NULL) {
				_board.at(i).at(j) = new Cookies();
				_board.at(i).at(j)->getLook().setOrigin(cookieSpriteCoeff / 2, cookieSpriteCoeff / 2);
				_board.at(i).at(j)->getLook().setPosition(sf::Vector2f(i * spriteCoeff + spriteCoeff / 2, j * spriteCoeff + spriteCoeff / 2));
			}
		}
	}	// end of first for
}

void GameController::loadNextLevel(sf::RenderWindow &window, sf::Clock &clock) {
	// initializing _board's size and starting params:
	for (unsigned int i = 0; i < _board.size(); i++) {
		for (unsigned int j = 0; j < _board[i].size(); j++) { 
			delete _board[i][j];
		}
	}
	_board.clear();
	_board.shrink_to_fit();
	initBoard();
	// initializing _walls
	readObstaclesFromFile();
	// recreate monsters:
	createMonsters();
	// fill cookies where empty spaces remain:
	fillCookies();
	// drawing buttons again:
	drawButtons(window);
	drawScoreAndLivesOnBoard (window);
	// deleting the computer players and creating new ones:
	while (_players.size() > 1) {
		delete _players[_players.size() - 1];
		_players.pop_back();
	}
	createComputerPlayers();
	// repositioning the user player:

	// finding position to place player at:
	sf::Vector2f playerLocation = findClosestEmptyPlace (sf::Vector2f(_board.size() / 2, _board.at(0).size() / 2));
	// saving the primary location of the player:
	_players[0]->setPrimaryLocation(playerLocation);
	// setting the location of the player to the one we calculated:
	_players[0]->getLook().setPosition(playerLocation.x * spriteCoeff + ((playerSpriteCoeff) / 2), playerLocation.y * spriteCoeff + ((playerSpriteCoeff) / 2));
	// resizing the window:
	window.create(sf::VideoMode((unsigned int) _board.size() *
		spriteCoeff + 200, (unsigned int) _board.at(0).size() * spriteCoeff), "Pacman");
	placeButtons();
	// restarting clock:
	clock.restart();
}
		
bool GameController::runGame () {

	sf::RenderWindow window(sf::VideoMode((unsigned) _board.size() * spriteCoeff + 200, (unsigned) _board.at(0).size() * spriteCoeff), "Pacman");
	window.setVerticalSyncEnabled(true);
	
	sf::Clock movementClock;
	sf::Clock cookieClock;
	float deltaTime = 0;			// to know how much to move
	float cookieTimer = 0;
	float speedAffectTimer = 0;			// how long does the speed affect last
	enum direction playerDir = RIGHT_DT;		// direction of user player movement

	while (window.isOpen()) {

		window.clear();

		// loding next level if needed:
		if (Cookies::getNumOfCookies() == 0) {
			if (_levelNum == numOfLevels)
				if (playNewGame())
					return true;
				else
					return false;
			else
				loadNextLevel(window, movementClock);	
			_movement = !_movement;
		}

		// drawing buttons, score & lives:
		drawButtons(window);		
		drawScoreAndLivesOnBoard (window);
		
		// drawing all the objects to the screen:
		for (unsigned int j = 0; j < _board.at(0).size(); j++) {
			for (unsigned int i = 0; i < _board.size(); i++) {
				if (_board.at(i).at(j) != NULL)
					window.draw(_board.at(i).at(j)->getLook());
			}
		}
		for (unsigned int i = 0; i < _players.size(); i++) {
			window.draw(_players.at(i)->getLook());
		}
		for (unsigned int i = 0; i < _monsters.size(); i++) {
			window.draw(_monsters.at(i)->getLook());
		}
	
		window.display();

		if (_movement) {
			// moving the players:
			deltaTime = movementClock.restart().asSeconds();
			for (unsigned int i = 0; i < _players.size(); i++) {
				_players[i]->move(playerDir, deltaTime, _board, _players[0]->getLook().getPosition());
				//_players[i]->_sound.play();
			}
		
			// moving the monsters:
			for (unsigned int i = 0; i < _monsters.size(); i++) {
				_monsters.at(i)->move(playerDir, deltaTime, _board, _players.at(0)->getLook().getPosition());
			}

			// checking collision with monsters:
			(checkColllisionWithMonsters(movementClock));

			// checking collision with cookies:
			for (unsigned int i = 0; i < _players.size(); i++) {
				checkCollisionWithCookies(_players[i], cookieClock, cookieTimer, speedAffectTimer);
			}
		}

		// checking if 
		float time  = cookieClock.getElapsedTime().asSeconds();
		if (cookieClock.getElapsedTime().asSeconds() > speedAffectTimer)
			_players[0]->setSpeed(70.f);

		float temp = _players[0]->getSpeed();
		
		// checking if players still have lives, if not we kill the player: (player at location 0 is user player!)
		for (unsigned int i = 0; i < _players.size(); i++) {
			if (_players[0]->getLives() == 0) {
				if (playNewGame())
					return true;
				else
					return false;
			}
			if (i > 0)
				if (_players[i]->getLives() == 0) {
					delete _players[i];
					_players.erase(_players.begin() + i);
				}
		}

		// handling events:
		sf::Event event;
        while (window.pollEvent(event)) {
			handleEvents(window, event, movementClock, playerDir);
		}
		if(!_keepPlaying)
			window.close();
	} 
}

void GameController::checkCollisionWithCookies(Player *player, sf::Clock &cookieClock, float &cookieTimer, float &speedAffectTimer) {
	_music.openFromFile("pacman_eatfruit.wav");
	for (unsigned int i = 0; i < _board.size(); i++)
		for (unsigned int j = 0; j < _board[i].size(); j++)
			if (_board[i][j] != NULL)			// NULLs represent empty places
				if (_board[i][j]->isCookie())   // if it's a cookie
					if (_board[i][j]->getLook().getGlobalBounds().contains(player->getLook().getPosition())) {
						// finding distance between centers of player & center of cookie:
						sf::Vector2f coordinateDifference;
						coordinateDifference.x = _board[i][j]->getLook().getOrigin().x - player->getLook().getOrigin().x;
						coordinateDifference.y = _board[i][j]->getLook().getOrigin().y - player->getLook().getOrigin().y;
						float distBtwCenters = sqrt(pow(coordinateDifference.x, 2) + pow(coordinateDifference.y, 2));
						
						if (distBtwCenters < playerSpriteCoeff/2) { // if distance is smaller than half the radius of the player
							// setting the time for the speed affect to last, if any:
							if (_board[i][j]->getSpeedTimer() != 0)
								speedAffectTimer = _board[i][j]->getSpeedTimer();
							// changing the player speed by speed coeff  % of the cookie eaten:
							player->setSpeed(((100 + _board[i][j]->getSpeedPercent()) * player->getSpeed()) / 100);
							if (player->getSpeed() > 80)
								player->setSpeed(80);
							if (player->getSpeed() < 10)
								player->setSpeed(10);
							delete _board[i][j];
							_game_score++;
							_board[i][j] = NULL;
							_music.play();
							cookieTimer = cookieClock.restart().asSeconds();
						}
					}
}

void GameController::checkColllisionWithMonsters (sf::Clock &movementClock) {
	// if a user player touches a monster the whole level is paused and all moving objects return to their initial positions.
	// if a computer playertouches a monster nothing happens, but that player's lives are deducted.
	//_music.openFromFile("pacman_die.wav");
	for (unsigned int i = 0; i < _players.size(); i++) {
		for (unsigned int j = 0; j < _monsters.size(); j++) {
			if (_players[i]->getLook().getGlobalBounds().intersects(_monsters[j]->getLook().getGlobalBounds())){
				_players[i]->_sound.play();
				if (_players[i]->getLives() > 0) {
					_players[i]->setLives(_players[i]->getLives() - 1);	// deducing one life
					_players[i]->getLook().setPosition((_players[i]->getPrimaryLocation().x * spriteCoeff) + (spriteCoeff-2)/2, (_players[i]->getPrimaryLocation().y * spriteCoeff) + (spriteCoeff-2)/2);
					if (i == 0) {					// resetting to original locations for this level
						_movement = !_movement;		// pausing
						resetPrimaryLocations();
					}
				}
			}
		}
	}
}

void GameController::resetPrimaryLocations() {
	for (unsigned int i = 0; i < _players.size(); i++) {
		_players[i]->getLook().setPosition((_players[i]->getPrimaryLocation().x * spriteCoeff) + (spriteCoeff-2)/2, (_players[i]->getPrimaryLocation().y * spriteCoeff) + (spriteCoeff-2)/2);
	}
	for (unsigned int j = 0; j < _monsters.size(); j++) { 		
		_monsters[j]->getLook().setPosition((_monsters[j]->getPrimaryLocation().x * spriteCoeff) + spriteCoeff/2, (_monsters[j]->getPrimaryLocation().y * spriteCoeff) + spriteCoeff/2);
	}
}

void GameController::createGameButtons () {
	//create a new menu button and insert into vector of buttons
	_gameButtons.push_back(new Pause (sf::Vector2f ((_board.size()*spriteCoeff + 100), (_board.at(0).size()*spriteCoeff /5))));
	//creating Help button using interihance
	_gameButtons.push_back(new Help (sf::Vector2f ((_board.size()*spriteCoeff + 100), (2*_board.at(0).size()*spriteCoeff /5))));
	//creating Exit button using interihance
	 _gameButtons.push_back(new Exit (sf::Vector2f ((_board.size()*spriteCoeff + 100), (3*_board.at(0).size()*spriteCoeff /5))));
}

void GameController::placeButtons () {	
	for (unsigned int i = 1; i <= _gameButtons.size(); i++)
		_gameButtons[i-1]->setLocation(sf::Vector2f ((_board.size()*spriteCoeff + 100), (i*_board.at(0).size()*spriteCoeff /5)));	
}

//drawing buttons of the game
void GameController::drawButtons (sf::RenderWindow &window) {  
	
	for (unsigned int i = 0; i < _gameButtons.size(); i++) 
	_gameButtons[i]->drawButton(window);	// drawing buttons on the board
}

void GameController::drawScoreAndLivesOnBoard (sf::RenderWindow &winToDrawOn) {

	//sting keeping the word "score" and a score of the game 
	std::stringstream sstr, sstr1;
	sstr << "Score: " << ( (_levelNum) * (_game_score)+ (_levelNum-1)*10) <<  std::endl;
	sstr1 << "Lives: " << _players.at(0)->getLives() <<  std::endl;
	sf::Text txt(sstr.str(), _font, SIZE_OF_SCORE);
	sf::Text txt1(sstr1.str(), _font, SIZE_OF_SCORE);

	setText(txt);
	setText(txt1);

	//place on the board score and lives
	txt.setPosition(sf::Vector2f(( _board.size()*spriteCoeff +100), 
				8*_board.at(0).size()*spriteCoeff /10)); 
	txt1.setPosition(sf::Vector2f(( _board.size()*spriteCoeff +100), 
				9*_board.at(0).size()*spriteCoeff /10)); 
	
	winToDrawOn.draw(txt);
	winToDrawOn.draw(txt1);	
}

void GameController::setText(sf::Text& txt){
		txt.setColor(sf::Color::White);
		//seting origin for score
		txt.setOrigin(txt.getLocalBounds().width/2.f,
		txt.getLocalBounds().height/2.f ); 
}

void GameController::handleEvents (sf::RenderWindow &window, const sf::Event &event, sf::Clock &movementClock, enum direction & playerDir) {
	// close window:
	if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && 
		event.key.code == sf::Keyboard::Escape))
		window.close();
	
	if (event.type == sf::Event::MouseButtonPressed) {  // user clicked mouse
		// we check this a lot so we defined it as a variable:
		sf::Vector2f mouseClick((float)sf::Mouse::getPosition(window).x,(float)sf::Mouse::getPosition(window).y);
		
		//if button is pressed
		// checking if buttons were clicked:
		for (unsigned int i=0; i<_gameButtons.size(); i++){
			if(_gameButtons[i]->getRect().getGlobalBounds().contains(mouseClick)){	
				_gameButtons[i]->drawPressButton(window);
				_keepPlaying = _gameButtons[i]->buttonFunctionality(movementClock, _movement);
			}
		}
		
	}//end of if(event.type == sf::Event::MouseButtonPressed)

	// changing movement direction:
	if (event.type == sf::Event::KeyPressed) {
		if 	(event.key.code == sf::Keyboard::Up)
			playerDir = UP_DT;
		if (event.key.code == sf::Keyboard::Down)
			playerDir = DOWN_DT;
		if (event.key.code == sf::Keyboard::Right)
			playerDir = RIGHT_DT;
		if (event.key.code == sf::Keyboard::Left)
			playerDir = LEFT_DT;
	}

	// changing if to move or not:
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space) {
		_movement = !_movement;
		movementClock.restart();
	}
}

bool GameController :: isMovebleObjectThere(const sf::Vector2f& startPoint){

	for (unsigned int i = 0; i < _monsters.size(); i++){
		if(_monsters[i]->isObjectThere(startPoint) == true)
			return true;
	}

	for (unsigned int j = 0; j < _players.size(); j++){
		if(_players[j]->isObjectThere(startPoint) == true)
			return true;
	}

	return false;
}

sf::Vector2f GameController::findClosestEmptyPlace(const sf::Vector2f& startPoint){ 
	for(unsigned int distance = 1; distance < _board.size()/2; distance++){

			if ( !(isMovebleObjectThere (startPoint)) &&
				((_board.at((unsigned int)startPoint.x).at((unsigned int)startPoint.y) == NULL) ||
				_board.at((unsigned int)startPoint.x).at((unsigned int)startPoint.y)->isCookie()))
				
			{
				//if current coner is empty put it there
				return startPoint;

			}
				else if (( startPoint.y+distance < _board.at(0).size() && 
					!(isMovebleObjectThere(sf::Vector2f(startPoint.x,startPoint.y+distance))) &&
				( _board.at((unsigned int)startPoint.x).at((unsigned int)startPoint.y+distance) == NULL ||
				_board.at((unsigned int)startPoint.x).at((unsigned int)startPoint.y+distance)->isCookie() == true)))
			{
				//checking up sell 
				//it's inside of the board and also a sell is empty and there is no moveble obect there
				return sf::Vector2f(startPoint.x,startPoint.y+distance);
			}

			else if (( startPoint.x+distance < _board.size() && 
					!(isMovebleObjectThere(sf::Vector2f(startPoint.x+distance,startPoint.y))) &&
				( _board.at((unsigned int)startPoint.x+distance).at((unsigned int)startPoint.y) == NULL ||
				_board.at((unsigned int)startPoint.x+distance).at((unsigned int)startPoint.y)->isCookie() == true)))
			{
				//checking sell on the right side 
				//it's inside of the board and also a sell is empty and there is no moveble obect there
				return sf::Vector2f(startPoint.x+distance,startPoint.y);	
			}

			else if (((startPoint.x-(float)distance) > 0) && 
				!(isMovebleObjectThere(sf::Vector2f(startPoint.x-distance,startPoint.y))) &&
				((_board.at((unsigned int)startPoint.x-distance).at((unsigned int)startPoint.y) == NULL)||
				(_board.at((unsigned int)startPoint.x-distance).at((unsigned int)startPoint.y)->isCookie() == true)))
			{
				//checking sell on the left side 
				//it's inside of the board and also a sell is empty and there is no moveble obect there
				return sf::Vector2f(startPoint.x-distance,startPoint.y);
			}
			
			else if ((startPoint.y-(float)distance) > 0 && 
				!(isMovebleObjectThere(sf::Vector2f(startPoint.x-distance,startPoint.y-(float)distance))) &&
				((_board.at((unsigned int)startPoint.x).at(startPoint.y-(float)distance) == NULL)||
				(_board.at((unsigned int)startPoint.x).at((unsigned int)startPoint.y-distance)->isCookie() == true)))
			{
				//checking down sell 
				//it's inside of the board and also a sell is empty and there is no moveble obect there
				return sf::Vector2f(startPoint.x,startPoint.y-distance);
			}
			
			else if (((startPoint.y-(float)distance) > 0) && 
				((unsigned int)startPoint.x+distance)<_board.size() &&
				!(isMovebleObjectThere(sf::Vector2f(startPoint.x+distance,startPoint.y-(float)distance))) &&
				((_board.at((unsigned int)startPoint.x+distance).at((unsigned int)startPoint.y-distance) == NULL)||
				(_board.at((unsigned int)startPoint.x+distance).at((unsigned int)startPoint.y-distance)->isCookie() == true)))
			{
				//checking sell on diagonal left up
				//it's inside of the board and also a sell is empty and there is no moveble obect there
				return sf::Vector2f(startPoint.x+distance,startPoint.y-distance);
			}

			else if ((((unsigned int)startPoint.y+distance) < _board.at(0).size()) && 
				((startPoint.x-(float)distance) > 0) &&
				!(isMovebleObjectThere(sf::Vector2f(startPoint.x-distance,startPoint.y+(float)distance))) &&
				((_board.at((unsigned int)startPoint.x-distance).at((unsigned int)startPoint.y+distance) == NULL)||
				(_board.at((unsigned int)startPoint.x-distance).at((unsigned int)startPoint.y+distance)->isCookie() == true)))
			{
				//checking sell on diagonal left down
				//it's inside of the board and also a sell is empty
				return sf::Vector2f(startPoint.x-distance,startPoint.y+distance);
			}
			//now we checking all cells around a point but not if board ends 
			else if (( (startPoint.y-(float)distance) > 0 )
				&& ( (startPoint.x-(float)distance) > 0) &&
				!(isMovebleObjectThere(sf::Vector2f(startPoint.x-distance,startPoint.y-(float)distance))) &&
				//place is empty or it is cookie there 
				((_board.at((unsigned int)startPoint.x-distance).at((unsigned int)startPoint.y-distance) == NULL)||
				(_board.at((unsigned int)startPoint.x-distance).at((unsigned int)startPoint.y-distance)->isCookie() == true)))
			{
				//checking sell on diagonal left up
				//it's inside of the board and also a sell is empty
				return sf::Vector2f(startPoint.x-distance,startPoint.y-distance);
				
			}
			
			else if ((((unsigned int)startPoint.y+distance) < _board.at(0).size()) && 
				(((unsigned int)startPoint.x+distance) < _board.size()) &&
				!(isMovebleObjectThere(sf::Vector2f(startPoint.x+distance,startPoint.y+(float)distance))) &&
				((_board.at((unsigned int)startPoint.x+distance).at((unsigned int)startPoint.y+distance) == NULL)||
				(_board.at((unsigned int)startPoint.x+distance).at((unsigned int)startPoint.y+distance)->isCookie() == true)))
			{
				//checking sell on diagonal right down
				//it's inside of the board and also a sell is empty
				return sf::Vector2f(startPoint.x+distance,startPoint.y+distance);	
			}		
	}
}

bool GameController::playNewGame () {
	// size of window:
	unsigned int winWidth = 400;
	unsigned int winHeight = 200;

	// all the text we need to display:
	sf::Text msg ("Do you wish to continue?", _font);
	//seting color to purple
	msg.setColor (sf::Color(153,0,210));
	sf::Text yes ("Y", _font);
	sf::Text no ("N", _font);
	sf::Text slash (" / ", _font);
	//seting color to purple
	slash.setColor (sf::Color(153,0,210));

	// setting for the text we're displaying:
	yes.setStyle(sf::Text::Underlined);
	no.setStyle(sf::Text::Underlined);

	msg.setOrigin(msg.getLocalBounds().width / 2, msg.getLocalBounds().height / 2);
	yes.setOrigin(yes.getLocalBounds().width / 2, yes.getLocalBounds().height / 2);
	no.setOrigin(no.getLocalBounds().width / 2, no.getLocalBounds().height / 2);
	slash.setOrigin(slash.getLocalBounds().width / 2, slash.getLocalBounds().height / 2);

	msg.setPosition((float)winWidth / 2, ((float)winHeight / 3) * 1);
	yes.setPosition(((float)winWidth / 2) - 10, ((float)winHeight / 3) * 2);
	slash.setPosition((float)winWidth / 2, ((float)winHeight / 3) * 2);
	no.setPosition(((float)winWidth / 2) + 30, ((float)winHeight / 3) * 2);

	sf::RenderWindow msgWin (sf::VideoMode(400, 200), "");
	msgWin.setVerticalSyncEnabled(true);

	while (msgWin.isOpen()) {
		msgWin.clear();
		msgWin.draw(msg);
		msgWin.draw(yes);
		msgWin.draw(slash);
		msgWin.draw(no);
		msgWin.display();
		
		sf::Event event;
		while (msgWin.pollEvent(event)) {
			if (event.type == sf::Event::KeyPressed) 
				if (event.key.code == sf::Keyboard::Y)
					return true;
				else if (event.key.code == sf::Keyboard::N)
					return false;
		}
	}
}

void GameController::loadingScreen () {
	sf::Texture pacmanScreen;
	pacmanScreen.loadFromFile("pacman.png");
	sf::Sprite load(pacmanScreen);
	sf::RenderWindow loadingWindow(sf::VideoMode(pacmanScreen.getSize().x, pacmanScreen.getSize().y, 32), "Loading, Please wait :)");
	_music.openFromFile("pacman_open.wav");
	_music.play();

	while (loadingWindow.isOpen()) {
		loadingWindow.clear(sf::Color::White);
		loadingWindow.draw(load);
		loadingWindow.display();

		if (_loadingComplete)
			loadingWindow.close();
	}

}