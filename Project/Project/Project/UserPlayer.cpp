#include "UserPlayer.h" 


UserPlayer::UserPlayer(){
	_lives = 3;
	
	_look.setTextureRect(sf::IntRect(_colInSpriteSheet * playerSpriteCoeff, RIGHT_MSR * playerSpriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
}

UserPlayer::~UserPlayer() {
}

void UserPlayer::changeLook (direction dir) {
	// opening mouth
	if (_colInSpriteSheet < 2)
		_colInSpriteSheet++;
	else
		_colInSpriteSheet = 0;
	
	switch (dir) {
		case UP_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, UP_MSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
		case DOWN_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, DOWN_MSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
		case LEFT_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, LEFT_MSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
		case RIGHT_DT:
			getLook().setTextureRect(sf::IntRect((int)_colInSpriteSheet * spriteCoeff, RIGHT_MSR * spriteCoeff, playerSpriteCoeff, playerSpriteCoeff));
			break;
	}

}