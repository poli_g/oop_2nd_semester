#pragma once
#include "Button.h"

class Help : public Button {

public:

	Help(sf::Vector2f v2f);
	~Help(void);

protected:

	//opens help window
	virtual bool buttonFunctionality(sf::Clock &movementClock,  bool & movement); 
};

