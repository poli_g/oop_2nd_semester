#pragma once 
#include "Object.h"

class UnmovableObject : public Object {
public:
	UnmovableObject();
	virtual ~UnmovableObject();

	virtual bool isCookie () = 0;
	virtual bool isObstacle () = 0;

	// returns _speedpercent:
	virtual float getSpeedPercent () { return _speedPercent; };
	// returns _speedTimer:
	virtual float getSpeedTimer () { return _speedTimer; };
protected:
	float _speedPercent;		// the percent by which the speed of the player changes when he eats this cookie
	float _speedTimer;			// how many seconds the speed affect lasts
};

