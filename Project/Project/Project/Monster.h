#pragma once
#include "MovableObject.h"
#include "UnmovableObject.h"

enum monsterSheetRow {UP_MR, RIGHT_MR, DOWN_MR, LEFT_MR};

class Monster : public MovableObject {
public:
	Monster();
	virtual ~Monster();

	// returns if the current object is a computer player
	virtual bool isCompPlayer() { return false; };
	// returns if current object is a monster:
	virtual bool isMonster() { return true; };
	// sets the movement direction randomly
	virtual direction setDirection (const vector <vector<UnmovableObject*>> &, direction, sf::Vector2f) = 0;
	// changes the appearance of the object according to it's direction
	virtual void changeLook (direction dir);

protected:
	
};

