#pragma once
#include "Button.h"

class Pause : public Button {

public:

	Pause(sf::Vector2f v2f);
	~Pause(void);

protected:

	virtual bool buttonFunctionality(sf::Clock &movementClock,  bool & movement);
};

