#pragma once
#include "UnmovableObject.h"
#include "GameController.h"

class Obstacle : public UnmovableObject {
public:
	Obstacle();
	~Obstacle(void);

	// checks if this is a cookie:
	bool isCookie () { return false; };
	// checks if this is a wall:
	bool isObstacle () { return true; };

protected:

};

