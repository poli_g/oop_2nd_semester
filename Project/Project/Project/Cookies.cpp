#include "Cookies.h"

int Cookies::_numOfCookies = 0;


Cookies::Cookies(void) {

	//_sound.openFromFile("pacman_eatfruit.wav");
	_numOfCookies++;
	sf::Clock clock;
	srand (clock.getElapsedTime().asMicroseconds());
	int val = rand () % 3 + 1;

	switch (val) {
		case 1:		// regular cookie
			_look.setTextureRect(sf::IntRect(((int)_look.getTextureRect().width / 10) * 8, 
				((int)_look.getTextureRect().height / 8)*1, (int)cookieSpriteCoeff,(int) cookieSpriteCoeff));
			_speedPercent = 0;
			_speedTimer = 0;
			break;
		case 2:		// speed up cookie
			_look.setTextureRect(sf::IntRect(((int)_look.getTextureRect().width / 10) * 8,
				((int)_look.getTextureRect().height / 8)*2, (int)cookieSpriteCoeff, (int)cookieSpriteCoeff));
			_speedPercent = 20;
			_speedTimer = 14;
			break;
		case 3:		// speed down cookie
			_look.setTextureRect(sf::IntRect(((int)_look.getTextureRect().width / 10) * 8, 
				((int)_look.getTextureRect().height / 8)*3, (int)cookieSpriteCoeff, (int)cookieSpriteCoeff));
			_speedPercent = -10;
			_speedTimer = 7;
			break;
		default:
			break;
	}
}

Cookies::~Cookies(void) {
	_numOfCookies--;
}
