#pragma once
#include "Monster.h"
#include "GameController.h"

class DumbMonster : public Monster {
public:
	DumbMonster(void);
	~DumbMonster(void);

	// sets the direction of the dumb monster:
	direction setDirection (const std::vector <std::vector<UnmovableObject*>> &, direction, sf::Vector2f);

protected:
};

