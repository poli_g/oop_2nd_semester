#pragma once
#include "object.h"
#include "UnmovableObject.h"

enum direction {UP_DT, DOWN_DT, LEFT_DT, RIGHT_DT};

class MovableObject : public Object {
public:
	MovableObject(void);
	virtual ~MovableObject(void);

	// movement of the object
	virtual void move (direction, float, vector <vector<UnmovableObject*>> &, sf::Vector2f pac_position);
	// checks if we can move to a specific spot
	bool moveTospot (sf::Vector2f newLocation,const vector <vector<UnmovableObject*>> &board, const direction &dir);
	// creates vector of possible locations to move to:
	vector <sf::Vector2i> setLocsTocheck (direction, sf::Vector2f, const vector <vector<UnmovableObject*>> &);
	// sets the primary location of the object 
	void setPrimaryLocation (sf::Vector2f loc) { _primaryLocation.x = loc.x; _primaryLocation.y = loc.y; };
	// gets the primary location of the object
	sf::Vector2f getPrimaryLocation () { return _primaryLocation; };
	// checks if an object i at given point:
	bool isObjectThere(const sf::Vector2f& startPoint);
	// returns if the current object is a computer player
	virtual bool isCompPlayer() = 0;
	// returns if current object is a monster:
	virtual bool isMonster() = 0;
	// sets the movement direction randomly
	virtual direction setDirection (const vector <vector<UnmovableObject*>> &, direction, sf::Vector2f) = 0;
	// changes the appearance of the object according to it's direction
	virtual void changeLook (direction dir) = 0;
	// returns _speed:
	float getSpeed () { return _speed; };
	// sets speed of player:
	void setSpeed (float speed) { _speed = speed; };

protected:
	sf::Vector2f _primaryLocation;
	float _speed;
	float _colInSpriteSheet;
};

