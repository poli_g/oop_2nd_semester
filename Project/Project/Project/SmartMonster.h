#pragma once 
#include "Monster.h"
#include <list>

class SmartMonster : public Monster {

public:
	SmartMonster(void);
	~SmartMonster(void);

	// sets the direction of the smatr monster:
	direction setDirection (const std::vector <std::vector<UnmovableObject*>> &, direction, sf::Vector2f);

protected:

	//sorts points/directions by distance to the pacman 
	void sortDirectionsByPriority(sf::Vector2f monster_loc,sf::Vector2f pacmanLoc, std::vector<direction> &vec);
	bool monsterMoveTospot (sf::Vector2f newLocation,const std::vector <std::vector<UnmovableObject*>> &board, const direction &dir);

};

