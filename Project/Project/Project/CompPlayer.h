#pragma once
#include "Player.h"

class CompPlayer :	public Player {

public:
	CompPlayer(void);
	~CompPlayer(void);

	// returns true:
	virtual bool isCompPlayer() { return true; };
	// sets direction of computer player:
	virtual direction setDirection (const std::vector <std::vector<UnmovableObject*>> &, direction, sf::Vector2f);
	// changes the appearance of the object according to it's direction
	void changeLook (direction dir);
};

